
## Get Started

### 1. Prerequisites

- [NodeJs](https://nodejs.org/en/)
- [NPM](https://npmjs.org/) - Node package manager

### 2. Installation

On the command prompt run the following commands:

``` 
 $ cd guruji
 $ cp .env.example .env (edit it with your API URL)
 $ npm install
 ```
### 3. Deploy the application 
Start the application on development environment:
 
 ```
 $ npm start
```

Run the application on production environment:
 
 ```
 $ npm run build
