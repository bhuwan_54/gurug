import React, { Suspense } from 'react';
import { render } from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

// 3rd
import 'styles/antd.less';
import 'styles/bootstrap/bootstrap.scss';
// custom
import 'styles/layout.scss';
import 'styles/theme.scss';
import 'styles/ui.scss';
import 'styles/vendors.scss';
import 'styles/themes/normalize.css';

import history from './utils/history';
import * as serviceWorker from './serviceWorker';
import configureStore from './store/configureStore';
import ScrollToTop from 'components/ScrollToTop';
import App from './containers/App';
import ErrorHandler from './components/Exception/Error';
import { AuthProvider } from './components/Auth/AuthContext';
const store = configureStore({}, history);

const mountNode = document.getElementById('root');

render(
  <Suspense fallback={<div className={'loader-container'}>Something went wrong. Please refresh the page</div>}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Router basename={process.env.PUBLIC_URL} history={history}>
          <ScrollToTop>
            <AuthProvider>
              <ErrorHandler>
                <App />
              </ErrorHandler>
            </AuthProvider>
          </ScrollToTop>
        </Router>
      </ConnectedRouter>
    </Provider>
  </Suspense>,
  mountNode
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
