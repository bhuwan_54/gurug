import * as firebase from 'firebase/app';
import {FIREBASE_CONFIG} from './constants/appConfig';

firebase.initializeApp(FIREBASE_CONFIG);
export default firebase;

