import React from 'react';
import { Form, Modal } from 'antd';
import moment from 'moment';

const FormItem = Form.Item;

export const toUpper = (v, prev) => {
  if (v === prev) {
    return v;
  }
  return v && v.charAt(0).toUpperCase() + v.slice(1);
};

export const urlToList = url => {
  if (url) {
    const urlList = url.split('/').filter(i => i);
    return urlList.map((urlItem, index) => `/${urlList.slice(0, index + 1).join('/')}`);
  }
};

export const isEmpty = obj => {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};

export const stringExplode = (str, delimiter) => {
  return str.split(delimiter);
};

export const isDuplicate = (array, title) => {
  return array.some(el => el.title === title);
};

export const arrayCompare = (arr1, arr2) => {
  if (!arr1 || !arr2) return;
  let result;
  arr1.forEach((e1, i) =>
    arr2.forEach(e2 => {
      if (e1.length > 1 && e2.length) {
        result = arrayCompare(e1, e2);
      } else if (e1 !== e2) {
        result = false;
      } else {
        result = true;
      }
    })
  );
  return result;
};

export const stringCompare = (str1, str2) => {
  const string1 = !isEmpty(str1) ? str1.toString() : '';
  const string2 = !isEmpty(str2) ? str2.toString() : '';
  return string1 === string2;
};

export const objectCompare = (obj1, obj2) => {
  return JSON.stringify(obj1) === JSON.stringify(obj2);
};

export const exactMatchByKey = (matchVal, myArray, matchKey = 'key') => {
  if (myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i][matchKey] === matchVal) {
        return myArray[i];
      }
    }
  }
  return;
};

export const matchBySearchKey = (matchKey, arrayItems) => {
  if (arrayItems) {
    const currentList = arrayItems.filter(item => {
      return item.searchKey.toLowerCase().search(matchKey.toLowerCase()) !== -1;
    });
    return currentList;
  }
  return;
};

export const matchByDynamicKey = (matchKey, myArray) => {
  if (myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i][matchKey]) {
        return myArray[i][matchKey];
      }
    }
  }
  return;
};

export const convertStingToPascalCase = str => {
  return str
    .toLowerCase()
    .split(' ')
    .map(word => {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(' ');
};

export const setColorStyle = (str1, str2, oldData, colorCode = 'red') => {
  const isEqual = stringCompare(str1, str2);
  const styleCode = !isEmpty(oldData)
    ? isEqual
      ? { color: '' }
      : { color: colorCode }
    : { color: '' };
  return styleCode;
};

export const hasPermission = (permissionCode, permissions) => {
  return permissions && permissions.includes(permissionCode);
};

export const getDate = () => {
  let date = new Date().toLocaleDateString();
  let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};

export const getTime = () => {
  let time = new Date().toLocaleTimeString('en-US', {
    hour12: false,
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  });
  time = time.split(':');
  time = time.join('');
  return time;
};

export const getStatusLabel = value => {
  let label = '';
  const statusValue = JSON.stringify(value);
  statusValue === 'true' ? (label = 'Yes') : (label = 'NO');
  return label;
};

export const getFieldPropertyValue = (data, compareKey, compareCode, returnKey) => {
  let result = '';
  const obj =
    Array.isArray(data) &&
    data.filter(d => {
      return compareKey === d[`${compareCode}`];
    });
  if (obj && obj[0] !== undefined && obj[0] !== null) {
    result = obj[0][`${returnKey}`];
  }
  return result;
};

export const getParentGroup = parentReference => {
  const lastIndex = parentReference.lastIndexOf('/');
  return parentReference ? parentReference.substr(0, lastIndex) : '';
};

export const getSubMenu = path => {
  const menuPath = path.split('/', 4);
  return menuPath[menuPath.length - 1];
};

export const formatLongNumberWithSuffix = value => {
  if (value === 0) {
    return 0;
  } else {
    // hundreds
    if (value <= 999) {
      return value;
    }
    // thousands
    else if (value >= 1000 && value <= 999999) {
      return value / 1000 + 'K';
    }
    // millions
    else if (value >= 1000000 && value <= 999999999) {
      return value / 1000000 + 'M';
    }
    // billions
    else if (value >= 1000000000 && value <= 999999999999) {
      return value / 1000000000 + 'B';
    } else return value;
  }
};

export const hasTouched = isFieldsTouched => {
  return isFieldsTouched();
};

export const hasErrors = fieldsError => {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
};

export const searchItemLayout = {
  xl: { span: 12, offset: 6 },
  lg: { span: 12, offset: 6 },
  md: { span: 18, offset: 6 },
  sm: { span: 24 },
  xs: { span: 24 },
};

export const formItemLayout = {
  labelCol: {
    xl: { span: 6 },
    lg: { span: 6 },
    md: { span: 8 },
    sm: { span: 6 },
    xs: { span: 24 },
  },
  wrapperCol: {
    xl: { span: 12 },
    lg: { span: 12 },
    md: { span: 16 },
    sm: { span: 18 },
    xs: { span: 24 },
  },
  labelAlign: 'left',
};

export const getSortingOrder = sorterOrdered => {
  let orderType;
  if (sorterOrdered) {
    if (sorterOrdered === 'descend') {
      orderType = 'DESCENDING';
    } else {
      orderType = 'ASCENDING';
    }
  }
  return orderType;
};

export const disabledPastDate = (current) =>{
  return current && current < moment().startOf('day');
};

export const disabledFutureDate = (current) =>{
  return current && current > moment().endOf('day');
};
