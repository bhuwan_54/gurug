import axios from 'axios';

import { API_URL, JWT_TOKEN, USER_FULL_NAME } from '../constants/appConfig';
import { http404Error, http500Error } from '../actions/httpErrorAction';
import configureStore from '../store/configureStore';
import { getLocalStorage, setLocalStorage, clearLocalStorage } from './storageUtil';
import history from './history';

const store = configureStore();

export const httpBase = () => {
  const api = axios.create({
    baseURL: `${API_URL}`,
    headers: {
      Authorization: 'Bearer ' + getLocalStorage(JWT_TOKEN),
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json',
  });

  api.interceptors.response.use(
    response => {
      // if (response.headers && response.headers['x-xsrf-token']) {
      //   setLocalStorage(JWT_TOKEN, response.headers['x-xsrf-token']);
      // }
      return response;
    },
    error => {
      if (401 === error.response.status) {
        clearLocalStorage(JWT_TOKEN);
        clearLocalStorage(USER_FULL_NAME);
        // store.dispatch(push('/'));
      }
      if (404 === error.response.status) {
        store.dispatch(http404Error());
       // store.dispatch(history.push('/404'));
      }
      if (500 === error.response.status) {
        store.dispatch(http500Error());
         store.dispatch(history.push('/500'));
      }
      // if (!error.status) {
      //   store.dispatch(http500Error());
      //   store.dispatch(push('/500'));
      // }
      return Promise.reject(error);
    }
  );

  return api;
};
