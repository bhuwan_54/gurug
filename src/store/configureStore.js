import { applyMiddleware, compose, createStore } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
// 'routerMiddleware': the new way of storing route changes with redux middleware since rrV4.
import { routerMiddleware } from 'connected-react-router';

import createRootReducer from '../reducers';

const configureStoreProd = (initialState = {}, history) => {
  // Add other middleware on this line...

  // thunk middleware can also accept an extra argument to be passed to each thunk action
  // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument

  const middlewares = [thunk, routerMiddleware(history)];
  return createStore(createRootReducer, initialState, compose(applyMiddleware(...middlewares)));
};

const configureStoreDev = (initialState = {}, history) => {
  //const reactRouterMiddleware = routerMiddleware(history);
  // Add other middleware on this line...

  // Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches.

  // thunk middleware can also accept an extra argument to be passed to each thunk action
  // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument

  const middlewares = [reduxImmutableStateInvariant(), thunk, routerMiddleware(history)];

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
  return createStore(
    createRootReducer,
    initialState,
    composeEnhancers(applyMiddleware(...middlewares))
  );
};

const configureStore = (initialState = {}, history) => {
  return process.env.NODE_ENV === 'production'
    ? configureStoreProd(initialState, history)
    : configureStoreDev(initialState, history);
};

export default configureStore;
