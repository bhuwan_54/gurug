import {
  REVIEW_FETCH_REQUEST,
  REVIEW_FETCH_REQUEST_SUCCESS,
  REVIEW_FETCH_REQUEST_FAILURE,
  REVIEW_CLEAN_REQUEST,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const reviewsReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case REVIEW_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case REVIEW_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalRecord,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case REVIEW_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case REVIEW_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default reviewsReducer;
