import {
  DASHBOARD_FETCH_REQUEST,
  DASHBOARD_FETCH_REQUEST_SUCCESS,
  DASHBOARD_FETCH_REQUEST_FAILURE,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const dashboardReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case DASHBOARD_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case DASHBOARD_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
      });

    case DASHBOARD_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    default:
      return state;
  }
};

export default dashboardReducer;
