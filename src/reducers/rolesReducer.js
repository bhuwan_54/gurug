import {
  ROLE_ADD_REQUEST,
  ROLE_ADD_REQUEST_SUCCESS,
  ROLE_ADD_REQUEST_FAILURE,
  ROLE_FETCH_REQUEST,
  ROLE_FETCH_REQUEST_SUCCESS,
  ROLE_FETCH_REQUEST_FAILURE,
  ROLE_UPDATE_REQUEST,
  ROLE_UPDATE_REQUEST_SUCCESS,
  ROLE_UPDATE_REQUEST_FAILURE,
  ROLE_CLEAN_REQUEST,
  ROLE_USER_TYPE_FETCH_REQUEST,
  ROLE_USER_TYPE_FETCH_REQUEST_SUCCESS,
  ROLE_USER_TYPE_FETCH_REQUEST_FAILURE,
  PERMISSION_FETCH_REQUEST,
  PERMISSION_FETCH_REQUEST_SUCCESS,
  PERMISSION_FETCH_REQUEST_FAILURE
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  userTypes: [],
  permissions:[],
  userTypesError: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const rolesReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case ROLE_ADD_REQUEST:
    case ROLE_FETCH_REQUEST:
    case ROLE_UPDATE_REQUEST:
    case PERMISSION_FETCH_REQUEST:
    case ROLE_USER_TYPE_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case ROLE_ADD_REQUEST_SUCCESS:
    case ROLE_UPDATE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case PERMISSION_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        permissions: action.data,
        errors: {},
      });
    case ROLE_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalRecord,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case ROLE_USER_TYPE_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        userTypes: action.data,
        userTypesError: {},
      });

    case ROLE_ADD_REQUEST_FAILURE:
    case ROLE_FETCH_REQUEST_FAILURE:
    case ROLE_UPDATE_REQUEST_FAILURE:
    case PERMISSION_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case ROLE_USER_TYPE_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        userTypesError: action.error,
      });

    case ROLE_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default rolesReducer;
