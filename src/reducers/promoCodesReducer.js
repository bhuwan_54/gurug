import {
  PROMO_CODE_ADD_REQUEST,
  PROMO_CODE_ADD_REQUEST_SUCCESS,
  PROMO_CODE_ADD_REQUEST_FAILURE,
  PROMO_CODE_FETCH_REQUEST,
  PROMO_CODE_FETCH_REQUEST_SUCCESS,
  PROMO_CODE_FETCH_REQUEST_FAILURE,
  PROMO_CODE_UPDATE_REQUEST,
  PROMO_CODE_UPDATE_REQUEST_SUCCESS,
  PROMO_CODE_UPDATE_REQUEST_FAILURE,
  PROMO_CODE_DISCOUNT_FETCH_REQUEST,
  PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS,
  PROMO_CODE_DISCOUNT_FETCH_REQUEST_FAILURE,
  PROMO_CODE_CLEAN_REQUEST,
  PROMO_CODE,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  discountType: [],
  discountTypeError: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const promoCodesReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case PROMO_CODE_ADD_REQUEST:
    case PROMO_CODE_FETCH_REQUEST:
    case PROMO_CODE_UPDATE_REQUEST:
    case PROMO_CODE_DISCOUNT_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case PROMO_CODE_ADD_REQUEST_SUCCESS:
    case PROMO_CODE_UPDATE_REQUEST_SUCCESS:
    case PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });
    //
    // case PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS:
    //   return Object.assign({}, state, {
    //     loading: false,
    //     discountType: action.data,
    //   });

    case PROMO_CODE_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalRecord,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case PROMO_CODE_ADD_REQUEST_FAILURE:
    case PROMO_CODE_FETCH_REQUEST_FAILURE:
    case PROMO_CODE_UPDATE_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });
    case PROMO_CODE_DISCOUNT_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        discountTypeError: action.error,
      });

    case PROMO_CODE_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        discountType: [],
        discountTypeError: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default promoCodesReducer;
