import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import settingsReducer from './settingsReducer';
import driversReducer from './driversReducer';
import customersReducer from './customersReducer';
import rolesReducer from './rolesReducer';
import geoFencingReducer from './geoFencingReducer';
import promoCodesReducer from './promoCodesReducer';
import livePreviewsReducer from './livePreviewsReducer';
import suportTicketsReducer from './supportTicketsReducer';
import reviewsReducer from './reviewsReducer';
import notificationsReducer from './notificationsReducer';
import driverCommissionsReducer from './driverCommissionsReducer';
import tripFeesReducer from './tripFeesReducer';
import singlePromoCodeReducer from './singlePromoCodeReducer';
import profileReducer from './profileReducer';
import dashboardReducer from './dashboardReducer';

import history from '../utils/history';

const appReducer = combineReducers({
  router: connectRouter(history),
  settings: settingsReducer,
  drivers: driversReducer,
  roles: rolesReducer,
  customers: customersReducer,
  geoFencings: geoFencingReducer,
  promoCodes: promoCodesReducer,
  livePreviews: livePreviewsReducer,
  supportTickets: suportTicketsReducer,
  reviews: reviewsReducer,
  driverCommissions: driverCommissionsReducer,
  notifications: notificationsReducer,
  tripFees: tripFeesReducer,
  promo: singlePromoCodeReducer,
  profiles: profileReducer,
  dashboards: dashboardReducer,
});

const rootReducer = (state, action) => {
  if (action && action.type === 'LOG_OUT_SUCCESS') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
