import {
  DRIVER_COMMISSION_ADD_REQUEST,
  DRIVER_COMMISSION_ADD_REQUEST_SUCCESS,
  DRIVER_COMMISSION_ADD_REQUEST_FAILURE,
  DRIVER_COMMISSION_FETCH_REQUEST,
  DRIVER_COMMISSION_FETCH_REQUEST_SUCCESS,
  DRIVER_COMMISSION_FETCH_REQUEST_FAILURE,
  DRIVER_COMMISSION_CLEAN_REQUEST,
  DRIVER_COMMISSION_TYPE_REQUEST,
  DRIVER_COMMISSION_TYPE_REQUEST_SUCCESS,
  DRIVER_COMMISSION_TYPE_REQUEST_FAILURE,
  COMMISSION_PROFILE_FETCH_REQUEST,
  COMMISSION_PROFILE_FETCH_REQUEST_SUCCESS,
  COMMISSION_PROFILE_FETCH_REQUEST_FAILURE,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  commissionType: [],
  loading: false,
  profiles: [],
  profileErrors: {},
  typeLoading: false,
  typeErrors: {},
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const driverCommissionsReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case DRIVER_COMMISSION_ADD_REQUEST:
    case DRIVER_COMMISSION_FETCH_REQUEST:
    case COMMISSION_PROFILE_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case DRIVER_COMMISSION_TYPE_REQUEST:
      return Object.assign({}, state, {
        typeLoading: true,
      });

    case DRIVER_COMMISSION_ADD_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case DRIVER_COMMISSION_TYPE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        typeLoading: false,
        commissionType: action.data,
        typeErrors: {},
      });
    case COMMISSION_PROFILE_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        profiles: action.data,
        profileErrors: {},
      });

    case DRIVER_COMMISSION_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalData,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case DRIVER_COMMISSION_ADD_REQUEST_FAILURE:
    case DRIVER_COMMISSION_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case DRIVER_COMMISSION_TYPE_REQUEST_FAILURE:
      return Object.assign({}, state, {
        typeLoading: false,
        typeErrors: action.error,
      });

    case COMMISSION_PROFILE_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loafing: false,
        profileErrors: action.error,
      });

    case DRIVER_COMMISSION_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default driverCommissionsReducer;
