import {
PROFILE_FETCH_REQUEST,
  PROFILE_FETCH_REQUEST_SUCCESS,
  PROFILE_FETCH_REQUEST_FAILURE,
  PROFILE_UPDATE_REQUEST,
  PROFILE_UPDATE_REQUEST_SUCCESS,
  PROFILE_UPDATE_REQUEST_FAILURE
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const profileReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case PROFILE_UPDATE_REQUEST:
    case PROFILE_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case PROFILE_UPDATE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case PROFILE_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalData,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case PROFILE_UPDATE_REQUEST_FAILURE:
    case PROFILE_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    default:
      return state;
  }
};

export default profileReducer;
