import {
  NOTIFICATION_ADD_REQUEST,
  NOTIFICATION_ADD_REQUEST_SUCCESS,
  NOTIFICATION_ADD_REQUEST_FAILURE,
  NOTIFICATION_FETCH_REQUEST,
  NOTIFICATION_FETCH_REQUEST_SUCCESS,
  NOTIFICATION_FETCH_REQUEST_FAILURE,
  NOTIFICATION_CLEAN_REQUEST,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const notificationsReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case NOTIFICATION_ADD_REQUEST:
    case NOTIFICATION_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case NOTIFICATION_ADD_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case NOTIFICATION_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalData,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case NOTIFICATION_ADD_REQUEST_FAILURE:
    case NOTIFICATION_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case NOTIFICATION_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default notificationsReducer;
