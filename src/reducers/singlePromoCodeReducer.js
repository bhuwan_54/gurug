import {
  PROMO_CODE
} from '../constants/actionTypes';

const INITIAL_STATE = {
promo:[]
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const singlePromoCodeReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case PROMO_CODE:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
      });
    default:
      return state;
  }
};

export default singlePromoCodeReducer;
