import {
  CUSTOMER_ADD_REQUEST,
  CUSTOMER_ADD_REQUEST_SUCCESS,
  CUSTOMER_ADD_REQUEST_FAILURE,
  CUSTOMER_FETCH_REQUEST,
  CUSTOMER_FETCH_REQUEST_SUCCESS,
  CUSTOMER_FETCH_REQUEST_FAILURE,
  CUSTOMER_UPDATE_REQUEST,
  CUSTOMER_UPDATE_REQUEST_SUCCESS,
  CUSTOMER_UPDATE_REQUEST_FAILURE,
  CUSTOMER_CLEAN_REQUEST,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const customersReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case CUSTOMER_ADD_REQUEST:
    case CUSTOMER_FETCH_REQUEST:
    case CUSTOMER_UPDATE_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case CUSTOMER_ADD_REQUEST_SUCCESS:
    case CUSTOMER_UPDATE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case CUSTOMER_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalRecord,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case CUSTOMER_ADD_REQUEST_FAILURE:
    case CUSTOMER_FETCH_REQUEST_FAILURE:
    case CUSTOMER_UPDATE_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case CUSTOMER_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default customersReducer;
