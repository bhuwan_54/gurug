import {
  SUPPORT_TICKET_ASSIGN_REQUEST,
  SUPPORT_TICKET_ASSIGN_REQUEST_SUCCESS,
  SUPPORT_TICKET_ASSIGN_REQUEST_FAILURE,
  SUPPORT_TICKET_FETCH_REQUEST,
  SUPPORT_TICKET_FETCH_REQUEST_SUCCESS,
  SUPPORT_TICKET_FETCH_REQUEST_FAILURE,
  EMPLOYEE_FETCH_REQUEST,
  EMPLOYEE_FETCH_REQUEST_SUCCESS,
  EMPLOYEE_FETCH_REQUEST_FAILURE,
  SUPPORT_TICKET_CLEAN_REQUEST
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  employee:[],
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const suportTicketsReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case SUPPORT_TICKET_ASSIGN_REQUEST:
    case SUPPORT_TICKET_FETCH_REQUEST:
    case EMPLOYEE_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case SUPPORT_TICKET_ASSIGN_REQUEST_SUCCESS:
    case EMPLOYEE_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        employee: action.data,
        errors: {},
      });

    case SUPPORT_TICKET_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalData,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case SUPPORT_TICKET_ASSIGN_REQUEST_FAILURE:
    case SUPPORT_TICKET_FETCH_REQUEST_FAILURE:
    case EMPLOYEE_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case SUPPORT_TICKET_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default suportTicketsReducer;
