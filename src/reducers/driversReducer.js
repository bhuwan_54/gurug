import {
  DRIVER_ADD_REQUEST,
  DRIVER_ADD_REQUEST_SUCCESS,
  DRIVER_ADD_REQUEST_FAILURE,
  DRIVER_FETCH_REQUEST,
  DRIVER_FETCH_REQUEST_SUCCESS,
  DRIVER_FETCH_REQUEST_FAILURE,
  DRIVER_UPDATE_REQUEST,
  DRIVER_UPDATE_REQUEST_SUCCESS,
  DRIVER_UPDATE_REQUEST_FAILURE,
  DRIVER_CLEAN_REQUEST,
  SECURITY_QUESTION_FETCH_REQUEST,
  SECURITY_QUESTION_FETCH_REQUEST_SUCCESS,
  SECURITY_QUESTION_FETCH_REQUEST_FAILURE
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  securityQuestion:[],
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const driversReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case DRIVER_ADD_REQUEST:
    case DRIVER_FETCH_REQUEST:
    case DRIVER_UPDATE_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case DRIVER_ADD_REQUEST_SUCCESS:
    case DRIVER_UPDATE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case SECURITY_QUESTION_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        securityQuestion: action.data,
        errors: {},
      });

    case DRIVER_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage || 0,
          pageSize: action.data.pageSize,
          total: action.data.totalData,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case DRIVER_ADD_REQUEST_FAILURE:
    case DRIVER_FETCH_REQUEST_FAILURE:
    case DRIVER_UPDATE_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case DRIVER_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default driversReducer;
