import {
  GEOFENCING_ADD_REQUEST,
  GEOFENCING_ADD_REQUEST_SUCCESS,
  GEOFENCING_ADD_REQUEST_FAILURE,
  GEOFENCING_FETCH_REQUEST,
  GEOFENCING_FETCH_REQUEST_SUCCESS,
  GEOFENCING_FETCH_REQUEST_FAILURE,
  GEOFENCING_UPDATE_REQUEST,
  GEOFENCING_UPDATE_REQUEST_SUCCESS,
  GEOFENCING_UPDATE_REQUEST_FAILURE,
  GEOFENCING_CLEAN_REQUEST,
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const geoFencingReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case GEOFENCING_ADD_REQUEST:
    case GEOFENCING_FETCH_REQUEST:
    case GEOFENCING_UPDATE_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case GEOFENCING_ADD_REQUEST_SUCCESS:
    case GEOFENCING_UPDATE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case GEOFENCING_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalRecord,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case GEOFENCING_ADD_REQUEST_FAILURE:
    case GEOFENCING_FETCH_REQUEST_FAILURE:
    case GEOFENCING_UPDATE_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case GEOFENCING_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default geoFencingReducer;
