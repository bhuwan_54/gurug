import {
  LIVE_PREVIEW_ADD_REQUEST,
  LIVE_PREVIEW_ADD_REQUEST_SUCCESS,
  LIVE_PREVIEW_ADD_REQUEST_FAILURE,
  LIVE_PREVIEW_FETCH_REQUEST,
  LIVE_PREVIEW_FETCH_REQUEST_SUCCESS,
  LIVE_PREVIEW_FETCH_REQUEST_FAILURE,
  LIVE_PREVIEW_UPDATE_REQUEST,
  LIVE_PREVIEW_UPDATE_REQUEST_SUCCESS,
  LIVE_PREVIEW_UPDATE_REQUEST_FAILURE,
  LIVE_PREVIEW_CLEAN_REQUEST,
  TOTAL_TRIP_FETCH_REQUEST,
  TOTAL_TRIP_FETCH_REQUEST_SUCCESS,
  TOTAL_TRIP_FETCH_REQUEST_FAILURE,
  TRIP_DETAIL_FETCH_REQUEST,
  TRIP_DETAIL_FETCH_REQUEST_SUCCESS,
  TRIP_DETAIL_FETCH_REQUEST_FAILURE
} from '../constants/actionTypes';

const INITIAL_STATE = {
  payload: [],
  loading: false,
  totalTrip:[],
  tripDetail:{},
  errors: {},
  pagination: {
    current: 0,
    pageSize: 0,
    total: 0,
    totalPage: 0,
  },
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
const livePreviewsReducer = (state, action) => {
  state = state || INITIAL_STATE;

  switch (action.type) {
    case LIVE_PREVIEW_ADD_REQUEST:
    case LIVE_PREVIEW_FETCH_REQUEST:
    case LIVE_PREVIEW_UPDATE_REQUEST:
    case TOTAL_TRIP_FETCH_REQUEST:
    case TRIP_DETAIL_FETCH_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });

    case LIVE_PREVIEW_ADD_REQUEST_SUCCESS:
    case LIVE_PREVIEW_UPDATE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        payload: action.data,
        errors: {},
      });

    case TOTAL_TRIP_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        totalTrip: action.data,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalData,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case TRIP_DETAIL_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        tripDetail: action.data,
        errors: {},
      });

    case LIVE_PREVIEW_FETCH_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        payload: action.data,
        loading: false,
        errors: {},
        pagination: {
          current: action.data.currentPage,
          pageSize: action.data.pageSize,
          total: action.data.totalRecord,
          totalPage: action.data.totalPage,
          showSizeChanger: true,
        },
      });

    case LIVE_PREVIEW_ADD_REQUEST_FAILURE:
    case LIVE_PREVIEW_FETCH_REQUEST_FAILURE:
    case LIVE_PREVIEW_UPDATE_REQUEST_FAILURE:
    case TOTAL_TRIP_FETCH_REQUEST_FAILURE:
    case TRIP_DETAIL_FETCH_REQUEST_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        errors: action.error,
      });

    case LIVE_PREVIEW_CLEAN_REQUEST:
      return Object.assign({}, state, {
        payload: [],
        loading: false,
        errors: {},
        pagination: {
          current: 0,
          pageSize: 0,
          total: 0,
          totalPage: 0,
        },
      });

    default:
      return state;
  }
};

export default livePreviewsReducer;
