import { message } from 'antd';
import {
  geoFencingFetchRequest,
  geoFencingFetchRequestSuccess,
  geoFencingFetchRequestFailure,
  geoFencingAddRequest,
  geoFencingAddRequestSuccess,
  geoFencingAddRequestFailure,
  geoFencingUpdateRequest,
  geoFencingUpdateRequestSuccess,
  geoFencingUpdateRequestFailure,
} from '../actions/geoFencingAction';
import { fetch, store, update, destroy } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';

export const fetchGeofencingWithCriteria = formData => {
  return dispatch => {
    dispatch(geoFencingFetchRequest());

    store('admin/geo-fencing/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(geoFencingFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          geoFencingFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const fetchGeoFencing = () => {
  return dispatch => {
    dispatch(geoFencingFetchRequest());

    store('admin/geo-fencing/list', { pageSize: 10, pageNumber: 1 })
      .then(response => {
        if (response.status === 200) {
          dispatch(geoFencingFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          geoFencingFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const fetchGeoFencingById = (formData) => {
  return dispatch => {
    dispatch(geoFencingFetchRequest());

    fetch(`admin/geo-fencing/${formData}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(geoFencingFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          geoFencingFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const addGeoFencing = formData => {
  return dispatch => {
    dispatch(geoFencingAddRequest());

    store('admin/geo-fencing', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(geoFencingAddRequestSuccess(response.data));
          message.success('Geofence Added.');
          history.push('/geofencings');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(geoFencingAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updateGeoFencing = formData => {
  return (dispatch) => {
    dispatch(geoFencingUpdateRequest());

    update(`admin/geo-fencing`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(geoFencingUpdateRequestSuccess(response.data));
          message.success('Geofence Updated.');
          history.push('/geoFencings');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          geoFencingUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const deleteGeofenceById = id => {
  return (dispatch) => {
    dispatch(geoFencingUpdateRequest());
    return destroy(`admin/geo-fencing`, id)
      .then(response => {
        if (response.status === 200) {
          //dispatch(geoFencingUpdateRequestSuccess(response));
          return response;
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          geoFencingUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};
