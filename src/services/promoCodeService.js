import {
  promoCodeFetchRequest,
  promoCodeFetchRequestSuccess,
  promoCodeFetchRequestFailure,
  promoCodeAddRequest,
  promoCodeAddRequestSuccess,
  promoCodeAddRequestFailure,
  promoCodeUpdateRequest,
  promoCodeUpdateRequestSuccess,
  promoCodeUpdateRequestFailure,
  promoCode
} from '../actions/promoCodeAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';
import { driverUpdateRequest, driverUpdateRequestFailure, driverUpdateRequestSuccess } from '../actions/driverAction';
import { message } from 'antd';

export const fetchPromoCodeWithCriteria = formData => {
  return dispatch => {
    dispatch(promoCodeFetchRequest());

    store('admin/promo-code/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(promoCodeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchPromoCodeByIdentifier = id => {
  return dispatch => {
    dispatch(promoCodeFetchRequest());

    fetch(`admin/promo-code/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(promoCodeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};


export const fetchDiscountType = () => {
  return function(dispatch) {
    dispatch(promoCodeFetchRequest());

    store('admin/promo-code/list-discount-type')
      .then(response => {
        if (response.status === 200) {
          dispatch(promoCodeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const addPromoCode = formData => {
  return (dispatch, getState) => {

    dispatch(promoCodeAddRequest());
    store('admin/promo-code', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(promoCodeAddRequestSuccess(response.data));
          history.push('/promoCodes');
          message.success('Promo codes added successfully.')
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(promoCodeAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updatePromoCodes = formData => {
  return (dispatch ) =>{
    dispatch(promoCodeUpdateRequest());

    update(`admin/promo-code`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(promoCodeUpdateRequestSuccess(response.data));
          history.push('/promoCodes');
          message.success('Promo codes updated successfully.')

        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const updatePromoCodeStatus = formData => {
  return (dispatch) => {
    dispatch(driverUpdateRequest());

    store(`admin/promo-code/${formData.apiUrlKey}`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverUpdateRequestSuccess(response.data));
          history.push('/promoCodes');
          message.success('Promo codes status updated successfully.')

        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchPendingPromoCode = formData => {
  return function(dispatch) {
    dispatch(promoCodeFetchRequest());

    store('promo-code/pending', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(promoCodeFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        console.error(error);
        dispatch(
          promoCodeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchPendingDetailByIdentifier = id => {
  return dispatch => {
    dispatch(promoCodeFetchRequest());

    fetch(`promo-code/pending/${id}`)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(promoCodeFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const approvePromoCode = formData => {
  return function(dispatch) {
    dispatch(promoCodeUpdateRequest());

    store('promo-code/approve', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(promoCodeUpdateRequestSuccess(response.data.data));
          history.push('/promoCodes/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const rejectPromoCode = formData => {
  return dispatch => {
    dispatch(promoCodeUpdateRequest());

    store('promo-code/reject', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(promoCodeUpdateRequestSuccess(response.data.data));
          history.push('/promoCodes/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          promoCodeUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};


export const promoCodeSet = values => {
  return dispatch => {
    dispatch(promoCode(values))
  }
}
