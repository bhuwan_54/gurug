import {
  tripFeeFetchRequest,
  tripFeeFetchRequestSuccess,
  tripFeeFetchRequestFailure,
  tripFeeAddRequest,
  tripFeeAddRequestSuccess,
  tripFeeAddRequestFailure,
} from '../actions/tripFeeAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';
import { message } from 'antd';

export const fetchTripFeeWithCriteria = (formData) => {
  return dispatch => {
    dispatch(tripFeeFetchRequest());
    store(`trip/fareDetails`,formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(tripFeeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(tripFeeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchTripFeeByIdentifier = id => {
  return dispatch => {
    dispatch(tripFeeFetchRequest());
    fetch(`tripFee/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(tripFeeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(tripFeeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const addTripFee = formData => {
  return dispatch => {
    dispatch(tripFeeAddRequest());

    store('tripFee', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(tripFeeAddRequestSuccess(response.data));
          history.push('/tripFees');
          message.success('Tip Fee added.')
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(tripFeeAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
