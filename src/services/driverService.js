import {
  driverFetchRequest,
  driverFetchRequestSuccess,
  driverFetchRequestFailure,
  driverAddRequest,
  driverAddRequestSuccess,
  driverAddRequestFailure,
  driverUpdateRequest,
  driverUpdateRequestSuccess,
  driverUpdateRequestFailure,
  securityFetchRequest,
  securityFetchRequestSuccess,
  securityFetchRequestFailure
} from '../actions/driverAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';
import { message } from 'antd';

export const fetchDriverWithCriteria = (formData = {}) => {
  return dispatch => {
    dispatch(driverFetchRequest());
    store('admin/driver/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchDriverByIdentifier = id => {
  return dispatch => {
    dispatch(driverFetchRequest());
    fetch(`admin/driver/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updateDriver = formData => {
  return dispatch => {
    dispatch(driverUpdateRequest());
    update(`admin/driver/update-admin`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverUpdateRequestSuccess(response.data));
          history.push('/drivers');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updateDriverStatus = formData => {
  return dispatch => {
    dispatch(driverUpdateRequest());

    fetch(`admin/driver/${formData.apiUrlKey}/${formData.id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverUpdateRequestSuccess(response.data));
          history.push('/drivers');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const approveDriver = formData => {
  return dispatch => {
    dispatch(driverUpdateRequest());

    store('driver/approve', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(driverUpdateRequestSuccess(response.data.data));
          history.push('/drivers/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const rejectDriver = formData => {
  return dispatch => {
    dispatch(driverUpdateRequest());

    store('driver/reject', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(driverUpdateRequestSuccess(response.data.data));
          history.push('/drivers/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
export const fetchSecurityQuestion = () => {
  return function(dispatch) {
    dispatch(securityFetchRequest());
    fetch('security-question/list')
      .then(response => {
        if (response.status === 200) {
          dispatch(securityFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(securityFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchPendingDriver = formData => {
  return function(dispatch) {
    dispatch(driverFetchRequest());
    store('driver/pending', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(driverFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchPendingDetailByIdentifier = id => {
  return dispatch => {
    dispatch(driverFetchRequest());

    fetch(`driver/pending/${id}`)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(driverFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const addDriver = formData => {
  return dispatch => {
    dispatch(driverAddRequest());

    store('admin/driver/register', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverAddRequestSuccess(response.data));
          history.push('/drivers');
          message.success('Driver Registered Successfully.')
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(driverAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
