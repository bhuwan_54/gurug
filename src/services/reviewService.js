import {
  reviewFetchRequest,
  reviewFetchRequestSuccess,
  reviewFetchRequestFailure,
} from '../actions/reviewAction';

import { fetch, store, update } from '../utils/httpUtil';
import { ERROR_MESSAGE } from '../constants/appConfig';

export const fetchReviewWithCriteria = formData => {
  return dispatch => {
    dispatch(reviewFetchRequest());

    store(`admin/review/list`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(reviewFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          reviewFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchReviewQuestionWithCriteria = formData => {
  return dispatch => {
    dispatch(reviewFetchRequest());

    fetch(`review/list/customer`)
      .then(response => {
        if (response.status === 200) {
          dispatch(reviewFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          reviewFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchReviewByIdentifier = id => {
  return dispatch => {
    dispatch(reviewFetchRequest());

    fetch(`admin/review/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(reviewFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          reviewFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};
