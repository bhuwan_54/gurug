import {
  notificationFetchRequest,
  notificationFetchRequestSuccess,
  notificationFetchRequestFailure,
  notificationAddRequest,
  notificationAddRequestSuccess,
  notificationAddRequestFailure,
} from '../actions/notificationAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';

export const fetchNotificationWithCriteria = (formData = {}) => {
  return dispatch => {
    dispatch(notificationFetchRequest());
    fetch('notification')
      .then(response => {
        if (response.status === 200) {
          dispatch(notificationFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(notificationFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchNotificationByIdentifier = id => {
  return dispatch => {
    dispatch(notificationFetchRequest());
    fetch(`admin/notification/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(notificationFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(notificationFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const addNotification = formData => {
  return dispatch => {
    dispatch(notificationAddRequest());

    store('notification', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(notificationAddRequestSuccess(response.data));
          history.push('/notifications');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(notificationAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
