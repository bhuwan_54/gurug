import {
  customerFetchRequest,
  customerFetchRequestSuccess,
  customerFetchRequestFailure,
  customerAddRequest,
  customerAddRequestSuccess,
  customerAddRequestFailure,
  customerUpdateRequest,
  customerUpdateRequestSuccess,
  customerUpdateRequestFailure,
} from '../actions/customerAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';

export const fetchCustomerWithCriteria = formData => {
  return dispatch => {
    dispatch(customerFetchRequest());

    store('admin/customer/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(customerFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchCustomerByIdentifier = id => {
  return dispatch => {
    dispatch(customerFetchRequest());

    fetch(`admin/customer/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(customerFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchPendingCustomer = formData => {
  return dispatch => {
    dispatch(customerFetchRequest());

    store('customer/pending', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(customerFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchPendingDetailByIdentifier = id => {
  return dispatch => {
    dispatch(customerFetchRequest());

    fetch(`customer/pending/${id}`)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(customerFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const addCustomer = formData => {
  return dispatch => {
    dispatch(customerAddRequest());
    store('customer', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(customerAddRequestSuccess(response.data));
          history.push('/customers/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updateCustomer = formData => {
  return dispatch => {
    dispatch(customerUpdateRequest());

    update(`admin/customer`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(customerUpdateRequestSuccess(response.data));
          history.push('/customers');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          customerUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const updateCustomerStatus = formData => {
  return dispatch => {
    dispatch(customerUpdateRequest());

    fetch(`admin/customer/${formData.apiUrlKey}/${formData.id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(customerUpdateRequestSuccess(response.data));
          history.push('/customers');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};


export const approveCustomer = formData => {
  return dispatch => {
    dispatch(customerUpdateRequest());

    store('customer/approve', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(customerUpdateRequestSuccess(response.data.data));
          history.push('/customers/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          customerUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const rejectCustomer = formData => {
  return dispatch => {
    dispatch(customerUpdateRequest());

    store('customer/reject', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(customerUpdateRequestSuccess(response.data.data));
          history.push('/customers/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          customerUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};


export const fetchProfileById = id => {
  return dispatch => {
    dispatch(customerFetchRequest());

    fetch(`profile/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(customerFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(customerFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
