import { fetch } from '../utils/httpUtil';
import { ERROR_MESSAGE } from '../constants/appConfig';
import {
  dashboardFetchRequest,
  dashboardFetchRequestSuccess,
  dashboardFetchRequestFailure,
} from '../actions/dashboardAction';

export const fetchDashboadStats = () => {
  return dispatch => {
    dispatch(dashboardFetchRequest());
    fetch(`admin/report-statistics`)
      .then(response => {
        if (response.status === 200) {
          dispatch(dashboardFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          dashboardFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};
