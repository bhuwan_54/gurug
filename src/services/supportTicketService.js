import {
 supportAssignRequest,
  supportAssignRequestSuccess,
  supportTicketAssignRequestFailure,
  supportTicketFetchRequest,
  supportTicketFetchRequestSuccess,
  supportTicketRequestFailure,
  employeeFetchRequest,
  employeeFetchRequestSuccess,
  employeeFetchRequestFailure,
} from '../actions/supportTicketAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';

import {
  ERROR_MESSAGE
} from '../constants/appConfig';

export const fetchSupportTicketWithCriteria = (formData) => {
  return dispatch => {
    dispatch(supportTicketFetchRequest());
    store('admin/support-ticket/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(supportTicketFetchRequestSuccess(response.data.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(supportTicketRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchSupportTicketByIdentifier = id => {
  return dispatch => {
    dispatch(supportTicketFetchRequest());
    fetch(`admin/support-ticket/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(supportTicketFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(supportTicketRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};


export const assignSupportTicket = formData => {
  return dispatch => {
    dispatch(supportAssignRequest());

    store('admin/support-ticket/assign', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(supportAssignRequestSuccess(response.data));
          history.push('/supports');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(supportTicketAssignRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};


export const closeSupportTicket = formData => {
  return dispatch => {
    dispatch(supportAssignRequest());

    store('admin/support-ticket/close', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(supportAssignRequestSuccess(response.data));
          history.push('/supports');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(supportTicketAssignRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};


export const fetchEmployeeDropdown = () => {
  return dispatch => {
    dispatch(employeeFetchRequest());
    fetch(`admin/dropdown`)
      .then(response => {
        if (response.status === 200) {
          dispatch(employeeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(employeeFetchRequestSuccess(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
