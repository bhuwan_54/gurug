import {
  roleFetchRequest,
  roleFetchRequestSuccess,
  roleFetchRequestFailure,
  roleUserTypeFetchRequest,
  roleUserTypeFetchRequestSuccess,
  roleUserTypeFetchRequestFailure,
  roleAddRequest,
  roleAddRequestSuccess,
  roleAddRequestFailure,
  roleUpdateRequest,
  roleUpdateRequestSuccess,
  roleUpdateRequestFailure,
  permissionFetchRequest,
  permissionFetchRequestSuccess,
  permissionFetchRequestFailure,
} from '../actions/roleAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';
import { message } from 'antd';

export const fetchRoleWithCriteria = formData => {
  return dispatch => {
    dispatch(roleFetchRequest());

    store('admin/role/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(roleFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(roleFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchRoleByIdentifier = id => {
  return dispatch => {
    dispatch(roleFetchRequest());

    fetch(`admin/roles-and-permissions/preview/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(roleFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(roleFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const fetchUserType = () => {
  return dispatch => {
    dispatch(roleUserTypeFetchRequest());

    fetch('admin/role/drop-down')
      .then(response => {
        if (response.status === 200) {
          dispatch(roleUserTypeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          roleUserTypeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchPermission = () => {
  return dispatch => {
    dispatch(permissionFetchRequest());

    fetch(`admin/permission/list`)
      .then(response => {
        if (response.status === 200) {
          dispatch(permissionFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          permissionFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const addRole = formData => {
  return dispatch => {
    dispatch(roleAddRequest());
    store('admin/roles-and-permissions/edit', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(roleAddRequestSuccess(response.data));
          message.success('Role has added successfully.');
          history.push('/roles');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(roleAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updateRole = formData => {
  return dispatch => {
    dispatch(roleUpdateRequest());
    store('admin/roles-and-permissions/edit', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(roleUpdateRequestSuccess(response.data));
          history.push('/roles');
          message.success('Role has updated successfully.');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(roleUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const approverole = formData => {
  return dispatch => {
    dispatch(roleUpdateRequest());

    store('roles/approve', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(roleUpdateRequestSuccess(response.data.data));
          history.push('/roles/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(roleUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const rejectRole = formData => {
  return function(dispatch) {
    dispatch(roleUpdateRequest());

    store('roles/reject', formData)
      .then(response => {
        if (response.data.message === 'SUCCESS') {
          dispatch(roleUpdateRequestSuccess(response.data.data));
          history.push('/roles/pending');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(roleUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
