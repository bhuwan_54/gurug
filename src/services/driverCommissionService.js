import {
  driverCommissionFetchRequest,
  driverCommissionFetchRequestSuccess,
  driverCommissionFetchRequestFailure,
  driverCommissionAddRequest,
  driverCommissionAddRequestSuccess,
  driverCommissionAddRequestFailure,
  commissionTypeFetchRequest,
  commissionTypeFetchRequestSuccess,
  commissionTypeFetchRequestFailure,
  commissionProfileFetchRequest,
  commissionProfileFetchRequestSuccess,
  commissionProfileFetchRequestFailure,
} from '../actions/driverCommissionAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';

export const fetchDriverCommissionWithCriteria = (formData = {}) => {
  return dispatch => {
    dispatch(driverCommissionFetchRequest());
    fetch('commission/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverCommissionFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          driverCommissionFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchDriverCommissionByIdentifier = id => {
  return dispatch => {
    dispatch(driverCommissionFetchRequest());
    fetch(`commissions/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverCommissionFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          driverCommissionFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchCommissionType = () => {
  return dispatch => {
    dispatch(commissionTypeFetchRequest());
    fetch(`commission/dropdown/type`)
      .then(response => {
        if (response.status === 200) {
          dispatch(commissionTypeFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          commissionTypeFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const fetchCommissionProfile = () => {
  return dispatch => {
    dispatch(commissionProfileFetchRequest());
    fetch(`commission/dropdown/profile`)
      .then(response => {
        if (response.status === 200) {
          dispatch(commissionProfileFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          commissionProfileFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};

export const addDriverCommission = formData => {
  return dispatch => {
    dispatch(driverCommissionAddRequest());

    store('commission', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(driverCommissionAddRequestSuccess(response.data));
          history.push('/dashboard');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          driverCommissionAddRequestFailure(error.response ? error.response.data : ERROR_MESSAGE)
        );
      });
  };
};
