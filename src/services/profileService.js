import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';
import {
  profileFetchRequest,
  profileFetchRequestSuccess,
  profileFetchRequestFailure,
  profileUpdateRequest,
  profileUpdateRequestSuccess,
  profileUpdateRequestFailure,
} from '../actions/profileAction';

export const fetchProfile = () => {
  return dispatch => {
    dispatch(profileFetchRequest());

    fetch(`admin/profile`)
      .then(response => {
        if (response.status === 200) {
          dispatch(profileFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(profileFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};

export const updateProfile = (formData) => {
  return dispatch => {
    dispatch(profileUpdateRequest());

    store(`admin/update-password`, formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(profileUpdateRequestSuccess(response.data));
          localStorage.clear();
          history.push('/');
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(profileUpdateRequestFailure(error.response ? error.response.data : ERROR_MESSAGE));
      });
  };
};
