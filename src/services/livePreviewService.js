import {
  livePreviewFetchRequest,
  livePreviewFetchRequestSuccess,
  livePreviewFetchRequestFailure,
  totalTripFetchRequest,
  totalTripFetchRequestSuccess,
  totalTripFetchRequestFailure,
  tripDetailFetchRequest,
  tripDetailFetchRequestSuccess,
  tripDetailFetchRequestFailure,
} from '../actions/livePreviewAction';
import { fetch, store, update } from '../utils/httpUtil';
import history from '../utils/history';
import { ERROR_MESSAGE } from '../constants/appConfig';

export const fetchLivePreviewWithCriteria = formData => {
  return dispatch => {
    dispatch(livePreviewFetchRequest());

    store('admin/livePreview/list', formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(livePreviewFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          livePreviewFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const fetchLivePreviewByIdentifier = id => {
  return dispatch => {
    dispatch(livePreviewFetchRequest());

    fetch(`livePreview/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(livePreviewFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          livePreviewFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const fetchTotalTripListWithCriteria = formData => {
  return dispatch => {
    dispatch(totalTripFetchRequest());

    store('admin/trip/list',formData)
      .then(response => {
        if (response.status === 200) {
          dispatch(totalTripFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          totalTripFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};

export const fetchTripDetailByIdentifier = id => {
  return dispatch => {
    dispatch(tripDetailFetchRequest());

    fetch(`trip/${id}`)
      .then(response => {
        if (response.status === 200) {
          dispatch(tripDetailFetchRequestSuccess(response.data));
        } else {
          // TODO
        }
      })
      .catch(error => {
        dispatch(
          tripDetailFetchRequestFailure(error.response ? error.response.data : ERROR_MESSAGE),
        );
      });
  };
};
