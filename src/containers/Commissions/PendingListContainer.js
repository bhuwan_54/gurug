import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingList from '../../components/Drivers/PendingList';
import * as driverService from '../../services/driverService';
import * as driverAction from '../../actions/driverAction';

class PendingListContainer extends Component {
  /**
   * Fetch pending drivers records.
   *
   * @param {object} formData
   */
  fetchPendingDriver = formData => {
    this.props.actions.fetchPendingDriver(formData);
  };

  /**
   * Clean drivers props.
   *
   */
  cleanDriverProps = () => {
    this.props.actions.driverCleanRequest();
  };

  render() {
    return (
      <PendingList
        fetchPendingDriver={this.fetchPendingDriver}
        cleanDriverProps={this.cleanDriverProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  drivers: state.drivers.payload,
  errors: state.drivers.errors,
  loading: state.drivers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, driverService, driverAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingListContainer);
