import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/Commissions/List';
import * as driverCommissionService from '../../services/driverCommissionService';
import * as driverCommissionAction from '../../actions/driverCommissionAction';

export class ListContainer extends Component {
  /**
   * fetch driver commissions record with criteria.
   * @param {object} formData
   */
  fetchDriverCommissionWithCriteria = formData => {
    this.props.actions.fetchDriverCommissionWithCriteria(formData);
  };

  /**
   * Clean drivers commissions props.
   *
   */
  cleanDriverCommissionsProps = () => {
    this.props.actions.driverCommissionCleanRequest();
  };

  render() {
    return (
      <List
        fetchDriverCommissionWithCriteria={this.fetchDriverCommissionWithCriteria}
        cleanDriverCommissionsProps={this.cleanDriverCommissionsProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  driverCommissions: state.driverCommissions.payload,
  errors: state.driverCommissions.errors,
  loading: state.driverCommissions.loading,
  pagination: state.driverCommissions.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, driverCommissionService, driverCommissionAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
