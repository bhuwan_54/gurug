import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/Commissions/AddForm';
import * as driverCommissionService from '../../services/driverCommissionService';
import * as driverCommissionAction from '../../actions/driverCommissionAction';

class AddContainer extends Component {
  /**
   * Add driver commissions record.
   *
   * @param {object} formData
   */
  addDriverCommission = formData => {
    this.props.actions.addDriverCommission(formData);
  };

  fetchCommissionType = () => {
    this.props.actions.fetchCommissionType();
  };

  fetchCommissionProfile = () => {
    this.props.actions.fetchCommissionProfile();
  };
  /**
   *
   * Fetch driver commissions record.
   *
   * @param {object} formData
   */
  fetchDriverCommissionWithCriteria = formData => {
    this.props.actions.fetchDriverCommissionWithCriteria(formData);
  };

  /**
   * Clean drivers commissions props.
   *
   */
  cleanDriverCommissionsProps = () => {
    this.props.actions.driverCommissionCleanRequest();
  };

  render() {
    return (
      <AddForm
        addDriverCommission={this.addDriverCommission}
        fetchCommissionType={this.fetchCommissionType}
        fetchCommissionProfile={this.fetchCommissionProfile}
        fetchDriverCommissionWithCriteria={this.fetchDriverCommissionWithCriteria}
        cleanDriverCommissionsProps={this.cleanDriverCommissionsProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  commissions: state.driverCommissions.payload,
  profiles: state.driverCommissions.profiles,
  commissionType: state.driverCommissions.commissionType,
  errors: state.driverCommissions.errors,
  loading: state.driverCommissions.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    Object.assign({}, driverCommissionService, driverCommissionAction),
    dispatch
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
