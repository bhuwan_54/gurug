import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import Authorization from '../../routes/Authorization';

// Import custom components
import List from './ListContainer';
import Detail from './DetailContainer';
import EditForm from './EditContainer';
import PendingList from './PendingListContainer';
import NotFound from '../Exception/NotFoundContainer';
import AddForm from './AddContainer';

const Driver = ({ match }) => (
  <Fragment>
    <Switch>
      {/*<Route exact path={`${match.url}`} component={List} />*/}
      <Route exact path={`${match.url}/new`} component={AddForm} />
      <Route component={NotFound} />
    </Switch>
  </Fragment>
);

export default Driver;
