import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingDetail from '../../components/Drivers/PendingDetail';
import * as driverService from '../../services/driverService';
import * as driverAction from '../../actions/driverAction';

class PendingDetailContainer extends Component {
  /**
   * Fetch pending driver detail records.
   *
   * @param {string} id
   */
  fetchPendingDetailByIdentifier = id => {
    this.props.actions.fetchPendingDetailByIdentifier(id);
  };

  /**
   * Clean drivers props.
   *
   */
  cleanDriverProps = () => {
    this.props.actions.driverCleanRequest();
  };

  render() {
    return (
      <PendingDetail
        fetchPendingDetailByIdentifier={this.fetchPendingDetailByIdentifier}
        cleanDriverProps={this.cleanDriverProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  drivers: state.drivers.payload,
  errors: state.drivers.errors,
  loading: state.drivers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, driverService, driverAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingDetailContainer);
