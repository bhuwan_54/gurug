import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm22 from '../../components/Drivers/AddForm22';
import * as driverService from '../../services/driverService';
import * as driverAction from '../../actions/driverAction';

class AddContainer extends Component {
  /**
   * Add customer record.
   *
   * @param {object} formData
   */
  addDriver = formData => {
    this.props.actions.addDriver(formData);
  };

  fetchSecurityQuestion = () => {
    this.props.actions.fetchSecurityQuestion();
  };

  /**
   * Clean drivers props.
   *
   */
  cleanDriverProps = () => {
    this.props.actions.driverCleanRequest();
  };

  render() {
    return (
      <AddForm22
        addDriver={this.addDriver}
        fetchSecurityQuestion={this.fetchSecurityQuestion}
        cleanDriverProps={this.cleanDriverProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  drivers: state.drivers.payload,
  securityQuestion: state.drivers.securityQuestion,
  errors: state.drivers.errors,
  loading: state.drivers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, driverService, driverAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
