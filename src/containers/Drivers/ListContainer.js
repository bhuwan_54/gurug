import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/Drivers/List';
import * as driverService from '../../services/driverService';
import * as driverAction from '../../actions/driverAction';

export class ListContainer extends Component {
  /**
   * fetch driver record with criteria.
   * @param {object} formData
   */
  fetchDriverWithCriteria = formData => {
    this.props.actions.fetchDriverWithCriteria(formData);
  };

  /**
   * Clean drivers props.
   *
   */
  cleanDriverProps = () => {
    this.props.actions.driverCleanRequest();
  };

  render() {
    return (
      <List
        fetchDriverWithCriteria={this.fetchDriverWithCriteria}
        cleanDriverProps={this.cleanDriverProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  drivers: state.drivers.payload.data,
  errors: state.drivers.errors,
  loading: state.drivers.loading,
  pagination: state.drivers.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, driverService, driverAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
