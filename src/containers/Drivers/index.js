import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import Authorization from '../../routes/Authorization';

// Import custom components
import List from './ListContainer';
import Detail from './DetailContainer';
import EditForm from './EditContainer';
import AddForm from './AddContainer';
import NotFound from '../Exception/NotFoundContainer';

const Driver = ({ match }) => (
  <Fragment>
    <Switch>
      <Route exact path={`${match.url}`} component={List} />
      <Route exact path={`${match.url}/:id/detail`} component={Detail} />
      <Route exact path={`${match.url}/:id/edit`} component={EditForm} />
      <Authorization exact path={`${match.url}/new`} component={AddForm} rights={['DRIVER_ADD']} />

      <Route component={NotFound} />
    </Switch>
  </Fragment>
);

export default Driver;
