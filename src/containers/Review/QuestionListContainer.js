import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import QuestionList from '../../components/Review/QuestionList';
import * as reviewService from '../../services/reviewService';
import * as reviewAction from '../../actions/reviewAction';

export class ListContainer extends Component {
  /**
   * fetch review record with criteria.
   * @param {object} formData
   */
  fetchReviewQuestionWithCriteria = () => {
    this.props.actions.fetchReviewQuestionWithCriteria();
  };

  /**
   * Clean reviews props.
   *
   */
  cleanReviewProps = () => {
    this.props.actions.reviewCleanRequest();
  };

  render() {
    return (
      <QuestionList
        fetchReviewQuestionWithCriteria={this.fetchReviewQuestionWithCriteria}
        cleanReviewProps={this.cleanReviewProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  reviews: state.reviews.payload.data,
  errors: state.reviews.errors,
  loading: state.reviews.loading,
  pagination: state.reviews.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, reviewService, reviewAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
