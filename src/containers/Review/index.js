import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import Authorization from '../../routes/Authorization';

// Import custom components
import List from './ListContainer';
import QuestionList from './QuestionListContainer';
import NotFound from '../Exception/NotFoundContainer';

const LivePreview = ({ match }) => (
  <Fragment>
    <Switch>
      <Route exact path={`${match.url}`} component={List} />
      <Route exact path={`${match.url}/questions`} component={QuestionList} />
      <Route component={NotFound} />
    </Switch>
  </Fragment>
);

export default LivePreview;
