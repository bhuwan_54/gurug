import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/Notifications/AddForm';
import * as notificationService from '../../services/notificationService';
import * as notificationAction from '../../actions/notificationAction';

class AddContainer extends Component {
  /**
   * Add notification record.
   *
   * @param {object} formData
   */
  addNotification = formData => {
    this.props.actions.addNotification(formData);
  };

  /**
   * Clean notification props.
   *
   */
  cleanNotificationProps = () => {
    this.props.actions.notificationCleanRequest();
  };

  render() {
    return (
      <AddForm
        addNotification={this.addNotification}
        cleanNotificationProps={this.cleanNotificationProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  notifications: state.notifications.payload,
  errors: state.notifications.errors,
  loading: state.notifications.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, notificationService, notificationAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
