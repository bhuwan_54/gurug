import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import Detail from '../../components/Drivers/Detail';
import * as driverService from '../../services/driverService';
import * as driverAction from '../../actions/driverAction';

class AddContainer extends Component {
  /**
   * Fetch driver record by id.
   *
   * @param {object} id
   */
  fetchDriverByIdentifier = id => {
    this.props.actions.fetchDriverByIdentifier(id);
  };

  /**
   * Clean drivers props.
   *
   */
  cleanDriverProps = () => {
    this.props.actions.driverCleanRequest();
  };

  render() {
    return (
      <Detail
        cleanDriverProps={this.cleanDriverProps}
        fetchDriverByIdentifier={this.fetchDriverByIdentifier}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  drivers: state.drivers.payload,
  errors: state.drivers.errors,
  loading: state.drivers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, driverService, driverAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
