import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/Notifications/List';
import * as notificationService from '../../services/notificationService';
import * as notificationAction from '../../actions/notificationAction';

export class ListContainer extends Component {
  /**
   * fetch driver record with criteria.
   * @param {object} formData
   */
  fetchNotificationWithCriteria = formData => {
    this.props.actions.fetchNotificationWithCriteria(formData);
  };

  /**
   * Clean drivers props.
   *
   */
  cleanNotificationProps = () => {
    this.props.actions.notificationCleanRequest();
  };

  render() {
    return (
      <List
        fetchNotificationWithCriteria={this.fetchNotificationWithCriteria}
        cleanNotificationProps={this.cleanNotificationProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  notifications: state.notifications.payload,
  errors: state.notifications.errors,
  loading: state.notifications.loading,
  pagination: state.notifications.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, notificationService, notificationAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
