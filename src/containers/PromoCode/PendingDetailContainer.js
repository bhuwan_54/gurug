import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingDetail from '../../components/PromoCode/PendingDetail';
import * as promoCodeService from '../../services/promoCodeService';
import * as promoCodeAction from '../../actions/promoCodeAction';

class PendingDetailContainer extends Component {
  /**
   * Fetch pending PromoCode detail records.
   *
   * @param {string} id
   */
  fetchPendingDetailByIdentifier = id => {
    this.props.actions.fetchPendingDetailByIdentifier(id);
  };

  /**
   * Clean promoCodes props.
   *
   */
  cleanPromoCodeProps = () => {
    this.props.actions.promoCodeCleanRequest();
  };

  render() {
    return (
      <PendingDetail
        fetchPendingDetailByIdentifier={this.fetchPendingDetailByIdentifier}
        cleanPromoCodeProps={this.cleanPromoCodeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  promoCodes: state.promoCodes.payload,
  errors: state.promoCodes.errors,
  loading: state.promoCodes.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, promoCodeService, promoCodeAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingDetailContainer);
