import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/PromoCode/List';
import * as promoCodeService from '../../services/promoCodeService';
import * as promoCodeAction from '../../actions/promoCodeAction';

export class ListContainer extends Component {
  /**
   * fetch promoCode record with criteria.
   * @param {object} formData
   */
  fetchPromoCodeWithCriteria = formData => {
    this.props.actions.fetchPromoCodeWithCriteria(formData);
  };

  updatePromoCodes = (formData) => {
    this.props.actions.updatePromoCodes(formData)
  };

  updatePromoCodeStatus = (formData) => {
    this.props.actions.updatePromoCodeStatus(formData)
  };

  promoCodeSet = (values) => {
    this.props.actions.promoCodeSet(values)
  };

  /**
   * Clean promoCodes props.
   *
   */
  cleanPromoCodeProps = () => {
    this.props.actions.promoCodeCleanRequest();
  };

  render() {
    return (
      <List
        updatePromoCodes={this.updatePromoCodes}
        promoCodeSet={this.promoCodeSet}
        fetchPromoCodeWithCriteria={this.fetchPromoCodeWithCriteria}
        updatePromoCodeStatus={this.updatePromoCodeStatus}
        cleanPromoCodeProps={this.cleanPromoCodeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  promoCodes: state.promoCodes.payload.data,
  errors: state.promoCodes.errors,
  loading: state.promoCodes.loading,
  pagination: state.promoCodes.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, promoCodeService, promoCodeAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
