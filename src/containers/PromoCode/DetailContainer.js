import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import Detail from '../../components/PromoCode/Detail';
import * as promoCodeService from '../../services/promoCodeService';
import * as promoCodeAction from '../../actions/promoCodeAction';

class AddContainer extends Component {
  /**
   * Fetch promoCode record by id.
   *
   * @param {object} id
   */
  fetchPromoCodeByIdentifier = id => {
    this.props.actions.fetchPromoCodeByIdentifier(id);
  };

  /**
   * Clean promoCodes props.
   *
   */
  cleanPromoCodeProps = () => {
    this.props.actions.promoCodeCleanRequest();
  };

  render() {
    return (
      <Detail
        addPromoCode={this.addPromoCode}
        cleanPromoCodeProps={this.cleanPromoCodeProps}
        fetchPromoCodeByIdentifier={this.fetchPromoCodeByIdentifier}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  promoCodes: state.promoCodes.payload,
  errors: state.promoCodes.errors,
  loading: state.promoCodes.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, promoCodeService, promoCodeAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
