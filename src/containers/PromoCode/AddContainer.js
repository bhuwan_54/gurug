import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/PromoCode/AddForm';
import * as promoCodeService from '../../services/promoCodeService';
import * as promoCodeAction from '../../actions/promoCodeAction';

class AddContainer extends Component {
  /**
   * Add promo code record.
   *
   * @param {object} formData
   */
  addPromoCode = formData => {
    this.props.actions.addPromoCode(formData);
  };

  /**
   * fetch discount type record.
   *
   */
  fetchDiscountType = () => {
    this.props.actions.fetchDiscountType();
  };

  /**
   * Clean promo codes props.
   *
   */
  cleanPromoCodeProps = () => {
    this.props.actions.promoCodeCleanRequest();
  };

  render() {
    return (
      <AddForm
        addPromoCode={this.addPromoCode}
        fetchDiscountType={this.fetchDiscountType}
        cleanPromoCodeProps={this.cleanPromoCodeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  promoCodes: state.promoCodes.payload,
  errors: state.promoCodes.errors,
  loading: state.promoCodes.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, promoCodeService, promoCodeAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
