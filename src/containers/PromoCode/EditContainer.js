import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import EditForm from '../../components/PromoCode/EditForm';
import * as promoCodeService from '../../services/promoCodeService';
import * as promoCodeAction from '../../actions/promoCodeAction';

class AddContainer extends Component {
  /**
   * Update promoCodes record.
   *
   * @param {object} formData
   */
  updatePromoCodes = formData => {
    this.props.actions.updatePromoCodes(formData);
  };
  /**
   * Update promo code status record.
   *
   * @param {object} formData
   */
  updatePromoCodeStatus = formData => {
    this.props.actions.updatePromoCodeStatus(formData);
  };
  /**
   * Fetch promoCodes detail by identifier.
   *
   * @param {string} id
   */

  fetchPromoCodeByIdentifier = id => {
    this.props.actions.fetchPromoCodeByIdentifier(id);
  };

  /**
   * Clean promo code props.
   *
   */
  cleanPromoCodeProps = () => {
    this.props.actions.promoCodeCleanRequest();
  };

  render() {
    return (
      <EditForm
        updatePromoCodes={this.updatePromoCodes}
        updatePromoCodeStatus={this.updatePromoCodeStatus}
        fetchPromoCodeByIdentifier={this.fetchPromoCodeByIdentifier}
        cleanPromoCodeProps={this.cleanPromoCodeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  promoCodes: state.promoCodes.payload,
  errors: state.promoCodes.errors,
  loading: state.promoCodes.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, promoCodeService, promoCodeAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
