import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingList from '../../components/PromoCode/PendingList';
import * as promoCodeService from '../../services/promoCodeService';
import * as promoCodeAction from '../../actions/promoCodeAction';

class PendingListContainer extends Component {
  /**
   * Fetch pending promoCodes records.
   *
   * @param {object} formData
   */
  fetchPendingPromoCode = formData => {
    this.props.actions.fetchPendingPromoCode(formData);
  };

  /**
   * Clean promoCodes props.
   *
   */
  cleanPromoCodeProps = () => {
    this.props.actions.promoCodeCleanRequest();
  };

  render() {
    return (
      <PendingList
        fetchPendingPromoCode={this.fetchPendingPromoCode}
        cleanPromoCodeProps={this.cleanPromoCodeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  promoCodes: state.promoCodes.payload,
  errors: state.promoCodes.errors,
  loading: state.promoCodes.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, promoCodeService, promoCodeAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingListContainer);
