import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/SupportTicket/AddForm';
import * as customerService from '../../services/customerService';
import * as customerAction from '../../actions/customerAction';

class AddContainer extends Component {
  /**
   * Add customer record.
   *
   * @param {object} formData
   */
  addCustomer = formData => {
    this.props.actions.addCustomer(formData);
  };

  /**
   * Clean customers props.
   *
   */
  cleanCustomerProps = () => {
    this.props.actions.customerCleanRequest();
  };

  render() {
    return (
      <AddForm
        addCustomer={this.addCustomer}
        cleanCustomerProps={this.cleanCustomerProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  customers: state.customers.payload,
  errors: state.customers.errors,
  loading: state.customers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, customerService, customerAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
