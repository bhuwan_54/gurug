import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import Authorization from '../../routes/Authorization';

// Import custom components
import List from './ListContainer';
import Detail from './DetailContainer';
import EditForm from './EditContainer';
import PendingList from './PendingListContainer';
import PendingDetail from './PendingDetailContainer';
import NotFound from '../Exception/NotFoundContainer';

const Customer = ({ match }) => (
  <Fragment>
    <Switch>
      <Route exact path={`${match.url}`} component={List} />
      <Route exact path={`${match.url}/:id/detail`} component={Detail} />
      {/*<Route exact path={`${match.url}/:id/edit`} component={EditForm} />*/}
      {/*<Route exact path={`${match.url}/pending`} component={PendingList} />*/}
      {/*<Route exact path={`${match.url}/pending/:id/detail`} component={PendingDetail} />*/}

      <Route component={NotFound} />
    </Switch>
  </Fragment>
);

export default Customer;
