import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/SupportTicket/List';
import * as supportTicketService from '../../services/supportTicketService';
import * as supportTicketAction from '../../actions/supportTicketAction';

export class ListContainer extends Component {
  /**
   * fetch Support Tickets record with criteria.
   * @param {object} formData
   */
  fetchSupportTicketWithCriteria = formData => {
    this.props.actions.fetchSupportTicketWithCriteria(formData);
  };

  /**
   * Clean support tickets props.
   *
   */
  cleanSupportTicketProps = () => {
    this.props.actions.supportTicketCleanRequest();
  };

  render() {
    return (
      <List
        fetchSupportTicketWithCriteria={this.fetchSupportTicketWithCriteria}
        cleanSupportTicketProps={this.cleanSupportTicketProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  supportTickets: state.supportTickets.payload,
  errors: state.supportTickets.errors,
  loading: state.supportTickets.loading,
  pagination: state.supportTickets.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, supportTicketService, supportTicketAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
