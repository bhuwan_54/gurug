import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import Detail from '../../components/SupportTicket/Detail';
import * as supportTicketService from '../../services/supportTicketService';
import * as supportTicketAction from '../../actions/supportTicketAction';

class AddContainer extends Component {
  /**
   * Fetch Customer record by id.
   *
   * @param {object} id
   */
  fetchSupportTicketByIdentifier = id => {
    this.props.actions.fetchSupportTicketByIdentifier(id);
  };

  /**
   * Assign Support Ticket.
   *
   * @param {object} formData
   */
  assignSupportTicket = (formData) => {
    this.props.actions.assignSupportTicket(formData);
  };



  /**
   * close Support Ticket.
   *
   * @param {object} formData
   */
  closeSupportTicket = (formData) => {
    this.props.actions.closeSupportTicket(formData);
  };

  /**
   * fetch employeee records.
   *
   */
  fetchEmployeeDropdown = () => {
    this.props.actions.fetchEmployeeDropdown()
  };
  /**
   * Clean support tickets props.
   *
   */
  cleanSupportTicketProps = () => {
    this.props.actions.supportTicketCleanRequest();
  };

  render() {
    return (
      <Detail
        assignSupportTicket={this.assignSupportTicket}
        fetchEmployeeDropdown={this.fetchEmployeeDropdown}
        closeSupportTicket={this.closeSupportTicket}
        cleanSupportTicketProps={this.cleanSupportTicketProps}
        fetchSupportTicketByIdentifier={this.fetchSupportTicketByIdentifier}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  supportTickets: state.supportTickets.payload,
  errors: state.supportTickets.errors,
  loading: state.supportTickets.loading,
  employee: state.supportTickets.employee
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, supportTicketService, supportTicketAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
