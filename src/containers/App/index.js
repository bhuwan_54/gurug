import React, { Fragment } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';

// Import custom components
import PrivateRoute from '../../routes/PrivateRoute';
import RestrictRoute from '../../routes/RestrictRoute';
import PublicRoute from '../../routes/PublicRoute';

import {
  AsyncProfile,
  AsyncAppLayout,
  AsyncCommission,
  AsyncCustomer,
  AsyncDashboard,
  AsyncDriver,
  AsyncForbidden,
  AsyncGeofencing,
  AsyncInternalServer,
  AsyncLivePreview,
  AsyncLogin,
  AsyncNotFound,
  AsyncNotifications,
  AsyncPromoCode,
  AsyncReset,
  AsyncReview,
  AsyncRoles,
  AsyncSupportTicket,
  AsyncTripFees

} from './AsyncComponent'
const App = (props) => {
  // const isRoot = location.pathname === '/' ? true : false;
  // if (isRoot) {
  //   return ( <Redirect to={'/'}/> );
  // }
  return (
    <Fragment>
      <Switch>
        <Route exact path="/" component={AsyncLogin} />
        <Route path="/reset" component={AsyncReset} />

        <PrivateRoute path="/dashboard" layout={AsyncAppLayout} component={AsyncDashboard} />
        <PrivateRoute path="/drivers" layout={AsyncAppLayout} component={AsyncDriver} />
        <PrivateRoute path="/roles" layout={AsyncAppLayout} component={AsyncRoles} />
        <PrivateRoute path="/customers" layout={AsyncAppLayout} component={AsyncCustomer} />
        <PrivateRoute path="/geofencings" layout={AsyncAppLayout} component={AsyncGeofencing} />
        <PrivateRoute path="/trips" layout={AsyncAppLayout} component={AsyncLivePreview} />
        <PrivateRoute path="/promoCodes" layout={AsyncAppLayout} component={AsyncPromoCode} />
        <PrivateRoute path="/profile" layout={AsyncAppLayout} component={AsyncProfile} />
        <PrivateRoute path="/commissions" layout={AsyncAppLayout} component={AsyncCommission} />
        <PrivateRoute path="/reviews" layout={AsyncAppLayout} component={AsyncReview} />
        <PrivateRoute path="/supports" layout={AsyncAppLayout} component={AsyncSupportTicket} />
        <PrivateRoute path="/notifications" layout={AsyncAppLayout} component={AsyncNotifications} />
        <PrivateRoute path="/tripFees" layout={AsyncAppLayout} component={AsyncTripFees} />

        {/*<PublicRoute path="/profile" layout={AsyncAppLayout} component={AsyncDashboard} /> *!/*/}
        <PublicRoute path="/403" layout={AsyncAppLayout} component={AsyncForbidden} />

        <Route path="/500" component={AsyncInternalServer} />
        <Route path="/404" component={AsyncNotFound} />
        <Route component={AsyncNotFound} />
      </Switch>
    </Fragment>
  );
};

export default withRouter(App);
