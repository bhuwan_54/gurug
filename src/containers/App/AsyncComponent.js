import React, { Fragment, Component } from 'react';
import {Spin} from 'antd';
import loadable from '@loadable/component';
import { LoadingOutlined } from '@ant-design/icons';

const antIcon = <LoadingOutlined style={{ fontSize: 50 }} spin />;

 export const AsyncNotFound = loadable(() => import('containers/Exception/NotFoundContainer'));
 export const AsyncForbidden = loadable(() => import('containers/Exception/ForbiddenContainer'));
 export const AsyncInternalServer = loadable(() => import('containers/Exception/InternalServerContainer'));

export const AsyncLogin = loadable(() => import('containers/Auth/LoginContainer'));
export const AsyncReset = loadable(() => import('containers/Auth/ResetContainer'));

export const AsyncAppLayout = loadable(() => import('components/Layout/AppLayout'));


export const AsyncDriver = loadable(() => import('containers/Drivers/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..." />
    </div>,
});
export const AsyncCustomer = loadable(() => import('containers/Customers/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon}  tip="Loading..."/>
  </div>,
});
export const AsyncGeofencing = loadable(() => import('containers/Geofencing/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});

export const AsyncLivePreview = loadable(() => import('containers/LivePreview/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});
export const AsyncPromoCode = loadable(() => import('containers/PromoCode/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..." />
  </div>,
});
export const AsyncRoles = loadable(() => import('containers/Role/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});
export const AsyncProfile = loadable(() => import('containers/Profile/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..." />
  </div>,
});
export const AsyncCommission = loadable(() => import('containers/Commissions/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});
export const AsyncReview = loadable(() => import('containers/Review/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});
export const AsyncSupportTicket = loadable(() => import('containers/SupportTicket/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..." />
  </div>,
});
export const AsyncNotifications = loadable(() => import('containers/Notifications/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..." />
  </div>,
});
export const AsyncTripFees = loadable(() => import('containers/TripFees/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});
export const AsyncDashboard = loadable(() => import('containers/Dashboard/'),{
  fallback: <div className={'loading-async'}><Spin indicator={antIcon} tip="Loading..."/>
  </div>,
});
