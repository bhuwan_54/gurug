import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import ResetPasswordForm from '../../components/Auth/ResetPasswordForm';

export class ResetContainer extends Component {
  /**
   * Submit the form.
   * @param {object} formProps
   *
   */
  submitForm = formProps => {
    this.props.actions.submitForm(formProps);
  };

  render() {
    return <ResetPasswordForm onSubmit={this.submitForm} />;
  }
}

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}), dispatch),
});

export default connect(
  null,
  mapDispatchToProps
)(ResetContainer);
