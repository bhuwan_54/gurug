import React, { Component } from 'react';
import { connect } from 'react-redux';

// Import custom components
import ProfileForm from '../../components/Profile/Profile';
import { bindActionCreators } from 'redux';
import * as profileService from '../../services/profileService';

class ProfileContainer extends Component {
  /**
   * fetch profile.
   */
  fetchProfile = () => {
    this.props.actions.fetchProfile();
  };

  updateProfile = (formData) => {
    this.props.actions.updateProfile(formData)
  }

  render() {
    return <ProfileForm updateProfile={this.updateProfile} fetchProfile={this.fetchProfile}{...this.props} />;
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  users: state.profiles.payload,
  errors: state.profiles.errors,
  loading: state.profiles.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, profileService), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
