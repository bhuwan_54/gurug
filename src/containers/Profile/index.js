import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import Authorization from '../../routes/Authorization';

// Import custom components
import ProfileForm from './ProfileContainer';
// import AddForm from './AddContainer';
import NotFound from '../Exception/NotFoundContainer';

const Profile = ({ match }) => (
  <Fragment>
    <Switch>
      <Route exact path={`${match.url}`} component={ProfileForm} />

      <Route component={NotFound} />
    </Switch>
  </Fragment>
);

export default Profile;
