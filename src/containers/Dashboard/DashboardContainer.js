import React, { Component } from 'react';

// Import custom components
import Dashboard from '../../components/Dashboard/Dashboard';
import { bindActionCreators } from 'redux';
import * as dashboardService from '../../services/dashboardService';
import { connect } from 'react-redux';
import AddForm22 from '../../components/Drivers/AddForm22';

class DashboardContainer extends Component {
  fetchDashboadStats = () => {
    this.props.actions.fetchDashboadStats();
  };

  render() {
    return <Dashboard fetchDashboadStats={this.fetchDashboadStats} {...this.props} />;
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  dashboards: state.dashboards.payload,
  errors: state.dashboards.errors,
  loading: state.dashboards.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, dashboardService), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);
