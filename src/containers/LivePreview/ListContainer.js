import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/LivePreview/List';
import * as livePreviewService from '../../services/livePreviewService';
import * as livePreviewAction from '../../actions/livePreviewAction';

export class ListContainer extends Component {
  /**
   * fetch livePreview record with criteria.
   * @param {object} formData
   */
  fetchLivePreviewWithCriteria = formData => {
    this.props.actions.fetchLivePreviewWithCriteria(formData);
  };

  /**
   * Clean livePreviews props.
   *
   */
  cleanLivePreviewProps = () => {
    this.props.actions.livePreviewCleanRequest();
  };

  render() {
    return (
      <List
        fetchLivePreviewWithCriteria={this.fetchLivePreviewWithCriteria}
        cleanLivePreviewProps={this.cleanLivePreviewProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  livePreviews: state.livePreviews.payload.data,
  errors: state.livePreviews.errors,
  loading: state.livePreviews.loading,
  pagination: state.livePreviews.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, livePreviewService, livePreviewAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
