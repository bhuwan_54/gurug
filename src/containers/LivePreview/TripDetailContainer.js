import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import TripDetail from '../../components/LivePreview/TripDetail';
import * as livePreviewService from '../../services/livePreviewService';
import * as livePreviewAction from '../../actions/livePreviewAction';

class AddContainer extends Component {
  /**
   * Fetch LivePreview record by id.
   *
   * @param {object} id
   */
  fetchTripDetailByIdentifier = id => {
    this.props.actions.fetchTripDetailByIdentifier(id);
  };

  /**
   * Clean livePreviews props.
   *
   */
  cleanTripDetialProps = () => {
    this.props.actions.livePreviewCleanRequest();
  };

  render() {
    return <TripDetail {...this.props} fetchTripDetailByIdentifier={this.fetchTripDetailByIdentifier} cleanTripDetialProps={this.cleanTripDetialProps} />;
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  tripDetails: state.livePreviews.tripDetail,
  errors: state.livePreviews.errors,
  loading: state.livePreviews.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, livePreviewService, livePreviewAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
