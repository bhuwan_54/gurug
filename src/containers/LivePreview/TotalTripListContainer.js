import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import TotalTripList from '../../components/LivePreview/TotalTripList';
import * as livePreviewService from '../../services/livePreviewService';
import * as livePreviewAction from '../../actions/livePreviewAction';

export class TotalTripListContainer extends Component {
  /**
   * fetch livePreview record with criteria.
   * @param {object} formData
   */
  fetchTotalTripListWithCriteria = formData => {
    this.props.actions.fetchTotalTripListWithCriteria(formData);
  };

  /**
   * Clean livePreviews props.
   *
   */
  cleanTotalTripListProps = () => {
    this.props.actions.livePreviewCleanRequest();
  };

  render() {
    return (
      <TotalTripList
        fetchTotalTripListWithCriteria={this.fetchTotalTripListWithCriteria}
        cleanTotalTripListProps={this.cleanTotalTripListProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  totalTrips: state.livePreviews.totalTrip.data,
  errors: state.livePreviews.errors,
  loading: state.livePreviews.loading,
  pagination: state.livePreviews.totalTrip,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, livePreviewService, livePreviewAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TotalTripListContainer);
