import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import Authorization from '../../routes/Authorization';

// Import custom components
import List from './ListContainer';
import TotalTripList from './TotalTripListContainer';
import TripDetail from './TripDetailContainer';
import Detail from './DetailContainer';
import NotFound from '../Exception/NotFoundContainer';

const LivePreview = ({ match }) => (
  <Fragment>
    <Switch>
      <Route exact path={`${match.url}/live`} component={List} />
      <Route exact path={`${match.url}`} component={TotalTripList} />
      <Route exact path={`${match.url}/:id/detail`} component={TripDetail} />
      <Route exact path={`${match.url}/live/:id/detail`} component={Detail} />

      <Route component={NotFound} />
    </Switch>
  </Fragment>
);

export default LivePreview;
