import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import Detail from '../../components/LivePreview/Detail';
import * as livePreviewService from '../../services/livePreviewService';
import * as livePreviewAction from '../../actions/livePreviewAction';

class AddContainer extends Component {
  /**
   * Fetch LivePreview record by id.
   *
   * @param {object} id
   */
  fetchLivePreviewByIdentifier = id => {
    this.props.actions.fetchLivePreviewByIdentifier(id);
  };

  /**
   * Clean livePreviews props.
   *
   */
  cleanLivePreviewProps = () => {
    this.props.actions.livePreviewCleanRequest();
  };

  render() {
    return <Detail {...this.props} />;
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  livePreviews: state.livePreviews.payload,
  errors: state.livePreviews.errors,
  loading: state.livePreviews.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, livePreviewService, livePreviewAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
