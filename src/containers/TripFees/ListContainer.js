import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/TripFees/List';
import * as tripFeeService from '../../services/tripFeeService';
import * as tripFeeAction from '../../actions/tripFeeAction';

export class ListContainer extends Component {
  /**
   * fetch trip fee record with criteria.
   * @param {object} formData
   */
  fetchTripFeeWithCriteria = formData => {
    this.props.actions.fetchTripFeeWithCriteria(formData);
  };

  /**
   * Clean trip fees props.
   *
   */
  cleanTripFeeProps = () => {
    this.props.actions.tripFeeCleanRequest();
  };

  render() {
    return (
      <List
        fetchTripFeeWithCriteria={this.fetchTripFeeWithCriteria}
        cleanTripFeeProps={this.cleanTripFeeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  tripFees: state.tripFees.payload,
  errors: state.tripFees.errors,
  loading: state.tripFees.loading,
  pagination: state.tripFees.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, tripFeeService, tripFeeAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
