import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import EditForm from '../../components/Drivers/AddForm';
import * as driverService from '../../services/driverService';
import * as driverAction from '../../actions/driverAction';

class AddContainer extends Component {
  /**
   * Update drivers record.
   *
   * @param {object} formData
   */
  updateDriver = formData => {
    this.props.actions.updateDriver(formData);
  };

  /**
   * Update drivers status record.
   *
   * @param {object} formData
   */
  updateDriverStatus = formData => {
    this.props.actions.updateDriverStatus(formData);
  };
  /**
   * Fetch driver detail by identifier.
   *
   * @param {string} id
   */

  fetchDriverByIdentifier = id => {
    this.props.actions.fetchDriverByIdentifier(id);
  };

  /**
   * Clean drivers props.
   *
   */
  cleanDriverProps = () => {
    this.props.actions.driverCleanRequest();
  };

  render() {
    return (
      <EditForm
        updateDriver={this.updateDriver}
        fetchDriverByIdentifier={this.fetchDriverByIdentifier}
        cleanDriverProps={this.cleanDriverProps}
        updateDriverStatus={this.updateDriverStatus}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  drivers: state.drivers.payload,
  errors: state.drivers.errors,
  loading: state.drivers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, driverService, driverAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
