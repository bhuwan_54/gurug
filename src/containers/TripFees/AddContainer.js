import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/TripFees/AddForm';
import * as tripFeeService from '../../services/tripFeeService';
import * as tripFeeAction from '../../actions/tripFeeAction';

class AddContainer extends Component {
  /**
   * Add trip fee record.
   *
   * @param {object} formData
   */
  addTripFee = formData => {
    this.props.actions.addTripFee(formData);
  };

  /**
   * Clean trip fees props.
   *
   */
  cleanTripFeeProps = () => {
    this.props.actions.tripFeeCleanRequest();
  };

  render() {
    return (
      <AddForm
        addTripFee={this.addTripFee}
        cleanTripFeeProps={this.cleanTripFeeProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  tripFees: state.tripFees.payload,
  errors: state.tripFees.errors,
  loading: state.tripFees.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, tripFeeService, tripFeeAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
