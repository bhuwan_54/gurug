import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/Customers/List';
import * as customerService from '../../services/customerService';
import * as customerAction from '../../actions/customerAction';

export class ListContainer extends Component {
  /**
   * fetch Customer record with criteria.
   * @param {object} formData
   */
  fetchCustomerWithCriteria = formData => {
    this.props.actions.fetchCustomerWithCriteria(formData);
  };

  /**
   * Clean customers props.
   *
   */
  cleanCustomerProps = () => {
    this.props.actions.customerCleanRequest();
  };

  render() {
    return (
      <List
        fetchCustomerWithCriteria={this.fetchCustomerWithCriteria}
        cleanCustomerProps={this.cleanCustomerProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  customers: state.customers.payload.data,
  errors: state.customers.errors,
  loading: state.customers.loading,
  pagination: state.customers.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, customerService, customerAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
