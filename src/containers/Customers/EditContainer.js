import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import EditForm from '../../components/Customers/EditForm';
import * as customerService from '../../services/customerService';
import * as customerAction from '../../actions/customerAction';

class EditContainer extends Component {
  /**
   * Update customer record.
   *
   * @param {object} formData
   */
  updateCustomer = formData => {
    this.props.actions.updateCustomer(formData);
  };
  /**
   * Update customer status record.
   *
   * @param {object} formData
   */
  updateCustomerStatus = formData => {
    this.props.actions.updateCustomerStatus(formData);
  };

  /**
   * Add customer record.
   *
   * @param {string} id
   */
  fetchCustomerByIdentifier = id => {
    this.props.actions.fetchCustomerByIdentifier(id);
  };

  /**
   * Clean customers props.
   *
   */
  cleanCustomerProps = () => {
    this.props.actions.customerCleanRequest();
  };

  render() {
    return (
      <EditForm
        updateCustomer={this.updateCustomer}
        fetchCustomerByIdentifier={this.fetchCustomerByIdentifier}
        updateCustomerStatus={this.updateCustomerStatus}
        cleanCustomerProps={this.cleanCustomerProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  customers: state.customers.payload,
  errors: state.customers.errors,
  loading: state.customers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, customerService, customerAction), dispatch),

});

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer);
