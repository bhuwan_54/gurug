import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import Detail from '../../components/Customers/Detail';
import * as customerService from '../../services/customerService';
import * as customerAction from '../../actions/customerAction';

class AddContainer extends Component {
  /**
   * Fetch Customer record by id.
   *
   * @param {object} id
   */
  fetchCustomerByIdentifier = id => {
    this.props.actions.fetchCustomerByIdentifier(id);
  };

  /**
   * Clean customers props.
   *
   */
  cleanCustomerProps = () => {
    this.props.actions.customerCleanRequest();
  };

  render() {
    return (
      <Detail
        addCustomer={this.addCustomer}
        cleanCustomerProps={this.cleanCustomerProps}
        fetchCustomerByIdentifier={this.fetchCustomerByIdentifier}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  customers: state.customers.payload,
  errors: state.customers.errors,
  loading: state.customers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, customerService, customerAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
