import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingList from '../../components/Customers/PendingList';
import * as customerService from '../../services/customerService';
import * as customerAction from '../../actions/customerAction';

class PendingListContainer extends Component {
  /**
   * Fetch pending customers records.
   *
   * @param {object} formData
   */
  fetchPendingCustomer = formData => {
    this.props.actions.fetchPendingCustomer(formData);
  };

  /**
   * Clean customers props.
   *
   */
  cleanCustomerProps = () => {
    this.props.actions.customerCleanRequest();
  };

  render() {
    return (
      <PendingList
        fetchPendingCustomer={this.fetchPendingCustomer}
        cleanCustomerProps={this.cleanCustomerProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  customers: state.customers.payload,
  errors: state.customers.errors,
  loading: state.customers.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, customerService, customerAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingListContainer);
