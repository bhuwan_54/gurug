import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingList from '../../components/Role/PendingList';
import * as roleService from '../../services/roleService';
import * as roleAction from '../../actions/roleAction';

class PendingListContainer extends Component {
  /**
   * Fetch pending roles records.
   *
   * @param {object} formData
   */
  fetchPendingRole = formData => {
    this.props.actions.fetchPendingRole(formData);
  };

  /**
   * Clean roles props.
   *
   */
  cleanRoleProps = () => {
    this.props.actions.roleCleanRequest();
  };

  render() {
    return (
      <PendingList
        fetchPendingRole={this.fetchPendingRole}
        cleanRoleProps={this.cleanRoleProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  roles: state.roles.payload,
  errors: state.roles.errors,
  loading: state.roles.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, roleService, roleAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingListContainer);
