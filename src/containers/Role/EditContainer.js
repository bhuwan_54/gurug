import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import EditForm from '../../components/Role/EditForm';
import * as roleService from '../../services/roleService';
import * as roleAction from '../../actions/roleAction';
import AddForm from '../../components/Role/AddForm';

class AddContainer extends Component {
  /**
   * Update roles record.
   *
   * @param {object} formData
   */
  updateRole = formData => {
    this.props.actions.updateRole(formData);
  };

  /**
   * Fetch permission record.
   *
   */
  fetchPermission = () => {
    this.props.actions.fetchPermission();
  };

  /**
   * Fetch role detail by identifier.
   *
   * @param {string} id
   */

  fetchRoleByIdentifier = id => {
    this.props.actions.fetchRoleByIdentifier(id);
  };

  /**
   * Clean roles props.
   *
   */
  cleanRoleProps = () => {
    this.props.actions.roleCleanRequest();
  };

  render() {
    return (
      <EditForm
        updateRole={this.updateRole}
        fetchRoleByIdentifier={this.fetchRoleByIdentifier}
        fetchPermission={this.fetchPermission}
        cleanRoleProps={this.cleanRoleProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  roles: state.roles.payload,
  errors: state.roles.errors,
  permissions: state.roles.permissions,
  loading: state.roles.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, roleService, roleAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
