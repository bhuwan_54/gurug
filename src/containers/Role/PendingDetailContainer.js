import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import PendingDetail from '../../components/Role/PendingDetail';
import * as roleService from '../../services/roleService';
import * as roleAction from '../../actions/roleAction';

class PendingDetailContainer extends Component {
  /**
   * Fetch pending role detail records.
   *
   * @param {string} id
   */
  fetchPendingDetailByIdentifier = id => {
    this.props.actions.fetchPendingDetailByIdentifier(id);
  };

  /**
   * Clean roles props.
   *
   */
  cleanRoleProps = () => {
    this.props.actions.roleCleanRequest();
  };

  render() {
    return (
      <PendingDetail
        fetchPendingDetailByIdentifier={this.fetchPendingDetailByIdentifier}
        cleanRoleProps={this.cleanRoleProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  roles: state.roles.payload,
  errors: state.roles.errors,
  loading: state.roles.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, roleService, roleAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingDetailContainer);
