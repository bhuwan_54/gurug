import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/Role/AddForm';
import * as roleService from '../../services/roleService';
import * as roleAction from '../../actions/roleAction';

class AddContainer extends Component {
  /**
   * Add role record.
   * @param {object} formData
   */
  addRole = formData => {
    this.props.actions.addRole(formData);
  };

  /**
   * Fetch role record by userType.
   * @param {string} userType
   */
  fetchRoleByIdentifier = userType => {
    this.props.actions.fetchRoleByIdentifier(userType);
  };

  /**
   * Fetch user type record.
   *
   */
  fetchUserType = () => {
    this.props.actions.fetchUserType();
  };


  /**
   * Fetch permission record.
   *
   */
  fetchPermission = () => {
    this.props.actions.fetchPermission();
  };
  /**
   * Clean roles props.
   *
   */
  cleanRoleProps = () => {
    this.props.actions.roleCleanRequest();
  };

  render() {
    return (
      <AddForm
        addRole={this.addRole}
        fetchUserType={this.fetchUserType}
        fetchPermission={this.fetchPermission}
        fetchRoleByIdentifier={this.fetchRoleByIdentifier}
        cleanRoleProps={this.cleanRoleProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  roles: state.roles.payload,
  errors: state.roles.errors,
  userTypes: state.roles.userTypes,
  permissions: state.roles.permissions,
  loading: state.roles.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, roleService, roleAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
