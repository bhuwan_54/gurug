import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/Role/List';
import * as roleService from '../../services/roleService';
import * as roleAction from '../../actions/roleAction';

export class ListContainer extends Component {
  /**
   * fetch role record with criteria.
   * @param {object} formData
   */
  fetchRoleWithCriteria = formData => {
    this.props.actions.fetchRoleWithCriteria(formData);
  };

  /**
   * Clean roles props.
   *
   */
  cleanRoleProps = () => {
    this.props.actions.roleCleanRequest();
  };

  render() {
    return (
      <List
        fetchRoleWithCriteria={this.fetchRoleWithCriteria}
        cleanRoleProps={this.cleanRoleProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  roles: state.roles.payload.data,
  errors: state.roles.errors,
  loading: state.roles.loading,
  pagination: state.roles.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, roleService, roleAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
