import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import List from '../../components/GeoFencing/List';
import * as geoFencingService from '../../services/geoFencingService';
import * as geoFencingAction from '../../actions/geoFencingAction';

export class ListContainer extends Component {
  /**
   * fetch geoFencing record with criteria.
   * @param {object} formData
   */
  fetchGeofencingWithCriteria = formData => {
    this.props.actions.fetchGeofencingWithCriteria(formData);
  };

  /**
   * Clean geoFencings props.
   *
   */
  cleanGeofencingProps = () => {
    this.props.actions.geoFencingCleanRequest();
  };

  deleteGeofenceById = (id) => {
    return (this.props.actions.deleteGeofenceById(id));
  };

  render() {
    return (
      <List
        fetchGeofencingWithCriteria={this.fetchGeofencingWithCriteria}
        deleteGeofenceById={this.deleteGeofenceById}
        cleanGeofencingProps={this.cleanGeofencingProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  geoFencings: state.geoFencings.payload.data,
  errors: state.geoFencings.errors,
  loading: state.geoFencings.loading,
  pagination: state.geoFencings.payload,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Object.assign({}, geoFencingService, geoFencingAction), dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
