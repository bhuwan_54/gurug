import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import Geofencing from '../../components/GeoFencing/Detail';
import * as geoFencingService from '../../services/geoFencingService';
import * as geoFencingAction from '../../actions/geoFencingAction';

class AddContainer extends Component {
  /**
   * Fetch geoFencing record.
   *
   */
  fetchGeoFencingById = (id) => {
    this.props.actions.fetchGeoFencingById(id);
  };
  /**
   * Update geoFencing record.
   *
   * @param {object} formData
   */
  updateGeoFencing = formData => {
    this.props.actions.updateGeoFencing(formData);
  };

  /**
   * Clean geoFencings props.
   *
   */
  cleanGeofencingProps = () => {
    this.props.actions.geoFencingCleanRequest();
  };

  render() {
    return (
      <Geofencing
        updateGeoFencing={this.updateGeoFencing}
        fetchGeoFencingById={this.fetchGeoFencingById}
        cleanGeofencingProps={this.cleanGeofencingProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  geoFencings: state.geoFencings.payload,
  errors: state.geoFencings.errors,
  loading: state.geoFencings.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, geoFencingService, geoFencingAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
