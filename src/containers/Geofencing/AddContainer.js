import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Import custom components
import AddForm from '../../components/GeoFencing/AddForm';
import * as geoFencingService from '../../services/geoFencingService';
import * as geoFencingAction from '../../actions/geoFencingAction';

class AddContainer extends Component {
  /**
   * Fetch geoFencing record.
   *
   */
  fetchGeoFencing = () => {
    this.props.actions.fetchGeoFencing();
  };
  /**
   * Add geoFencing record.
   *
   * @param {object} formData
   */
  addGeoFencing = formData => {
    this.props.actions.addGeoFencing(formData);
  };

  /**
   * Clean geoFencings props.
   *
   */
  cleanGeofencingProps = () => {
    this.props.actions.geoFencingCleanRequest();
  };

  render() {
    return (
      <AddForm
        addGeoFencing={this.addGeoFencing}
        fetchGeoFencing={this.fetchGeoFencing}
        cleanGeofencingProps={this.cleanGeofencingProps}
        {...this.props}
      />
    );
  }
}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  geoFencings: state.geoFencings.payload,
  errors: state.geoFencings.errors,
  loading: state.geoFencings.loading,
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, geoFencingService, geoFencingAction), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddContainer);
