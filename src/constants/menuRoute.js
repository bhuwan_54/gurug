export default [
  // dashboard
  {
    name: 'Dashboard',
    menuName: 'Dashboard',
    path: '/dashboard',
    iconName: 'dashboard',
  },

  //drivers
  {
    iconName: 'team',
    path: '/drivers',
    name: 'Drivers',
    rights: ['DRIVER_ADD', 'DRIVER_UPDATE', 'DRIVER_DELETE'],
    routes: [
      {
        name: 'Driver - Add',
        menuName: 'Add',
        path: '/drivers/new',
        iconName: 'plus-circle-o',
        rights: ['DRIVER_ADD'],
      },
      {
        name: 'Drivers - List',
        menuName: 'List',
        path: '/drivers',
        iconName: 'bars',
        // rights: ['driver:list'],
      },
      // {
      //   name: 'Drivers - Pending Lists',
      //   menuName: 'Pending',
      //   path: '/drivers/pending',
      //   iconName: 'bars',
      //   // rights: ['driver:list:pending'],
      // },
    ],
  },

  // roles
  {
    iconName: 'lock',
    path: '/roles',
    name: 'Roles',
    // rights: ['role:create', 'role:list', 'role:list:pending', 'role:audit:log'],
    routes: [
      {
        name: 'Roles - Manage',
        menuName: 'Manage',
        path: '/roles/new',
        iconName: 'plus-circle-o',
        // rights: ['role:create'],
      },
      {
        name: 'Roles - List',
        menuName: 'List',
        path: '/roles',
        iconName: 'bars',
        // rights: ['role:list'],
      },
      // {
      //   name: 'Roles - Pending Lists',
      //   menuName: 'Pending',
      //   path: '/roles/pending',
      //   iconName: 'bars',
      //   rights: ['role:list:pending'],
      // },
    ],
  },

  //customers
  {
    iconName: 'user-add',
    path: '/customers',
    name: 'Customers',
    // rights: ['customer:create', 'customer:list', 'customer:list:pending', 'customer:audit:log'],
    routes: [
      // {
      //   name: 'Customers - New',
      //   menuName: 'New',
      //   path: '/customers/new',
      //   iconName: 'plus-circle-o',
      //   // rights: ['customer:create'],
      // },
      {
        name: 'Customers - List',
        menuName: 'List',
        path: '/customers',
        iconName: 'bars',
        // rights: ['customer:list'],
      },
      // {
      //   name: 'Customers - Pending Lists',
      //   menuName: 'Pending',
      //   path: '/customers/pending',
      //   iconName: 'bars',
      //   // rights: ['customer:list:pending'],
      // },
    ],
  },
  //Geofencing
  {
    iconName: 'heat-map',
    path: '/geofencings',
    name: 'Geofencing',
    // rights: ['customer:create', 'customer:list', 'customer:list:pending', 'customer:audit:log'],
    routes: [
      {
        name: 'Geofencing - New',
        menuName: 'New',
        path: '/geofencings/new',
        iconName: 'plus-circle-o',
        // rights: ['customer:create'],
      },
      {
        name: 'Customers Geofencing ',
        menuName: 'List',
        path: '/geofencings',
        iconName: 'compass',
        // rights: ['customer:list'],
      },
      // {
      //   name: 'Customers - Pending Lists',
      //   menuName: 'Pending',
      //   path: '/geofencings/pending',
      //   iconName: 'bars',
      //   // rights: ['customer:list:pending'],
      // },
    ],
  },

  //LivePreview
  {
    iconName: 'global',
    path: '/trips',
    name: 'Trip List',
    // rights: [ 'driver:live:list',],
    routes: [
      {
        name: 'Customers Lives- List',
        menuName: 'Live Trip List',
        path: '/trips/live',
        iconName: 'bars',
        // rights: ['driver:live:list'],
      },
      {
        name: 'Customers Trip List- List',
        menuName: 'Total Trip List',
        path: '/trips',
        iconName: 'bars',
        // rights: ['driver:live:list'],
      },
    ],
  },

  //Review
  {
    iconName: 'database',
    path: '/reviews',
    name: 'Review',
    // rights: ['promoCode:create', 'promoCode:list', 'promoCode:list:pending'],
    routes: [
      {
        name: 'Review - List',
        menuName: 'List',
        path: '/reviews',
        iconName: 'bars',
        // rights: ['promoCode:list'],
      },
      // {
      //   name: 'Review - Question ',
      //   menuName: 'Question List',
      //   path: '/reviews/questions',
      //   iconName: 'bars',
      //   // rights: ['promoCode:list'],
      // },
    ],
  },


  //promo codes
  {
    iconName: 'dollar',
    path: '/promoCodes',
    name: 'Promo Codes',
    // rights: ['promoCode:create', 'promoCode:list', 'promoCode:list:pending'],
    routes: [
      {
        name: 'Promo Codes - New',
        menuName: 'New',
        path: '/promoCodes/new',
        iconName: 'plus-circle-o',
        // rights: ['promoCode:create'],
      },
      {
        name: 'Promo Codes - List',
        menuName: 'List',
        path: '/promoCodes',
        iconName: 'bars',
        // rights: ['promoCode:list'],
      },
    ],
  },

//supports
  {
    iconName: 'desktop',
    path: '/supports',
    name: 'Support Tickets',
    // rights: ['customer:create', 'customer:list', 'customer:list:pending', 'customer:audit:log'],
    routes: [
      // {
      //   name: 'Customers - New',
      //   menuName: 'New',
      //   path: '/customers/new',
      //   iconName: 'plus-circle-o',
      //   // rights: ['customer:create'],
      // },
      {
        name: 'Support - List',
        menuName: 'List',
        path: '/supports',
        iconName: 'bars',
        // rights: ['customer:list'],
      },
      // {
      //   name: 'Customers - Pending Lists',
      //   menuName: 'Pending',
      //   path: '/customers/pending',
      //   iconName: 'bars',
      //   // rights: ['customer:list:pending'],
      // },
    ],
  },


  //
  {
    iconName: 'bell',
    path: '/notifications',
    name: 'Notifications',
    // rights: ['customer:create', 'customer:list', 'customer:list:pending', 'customer:audit:log'],
    routes: [
      {
        name: 'Notifications - New',
        menuName: 'New',
        path: '/notifications/new',
        iconName: 'plus-circle-o',
        // rights: ['customer:create'],
      },
      {
        name: 'Notifications - List',
        menuName: 'List',
        path: '/notifications',
        iconName: 'bars',
        // rights: ['customer:list'],
      },
      // {
      //   name: 'Customers - Pending Lists',
      //   menuName: 'Pending',
      //   path: '/customers/pending',
      //   iconName: 'bars',
      //   // rights: ['customer:list:pending'],
      // },
    ],
  },

  // Application Setting
  {
    iconName: 'setting',
    path: '/settings',
    name: 'Application Settings',
    // rights: ['promoCode:create', 'promoCode:list', 'promoCode:list:pending'],
    routes: [
      {
        name: 'Commissions Add',
        menuName: 'Commission Add',
        path: '/commissions/new',
        iconName: 'plus-circle-o',
         },
      {
        name: 'Trip Fee List',
        menuName: 'Trip Fee',
        path: '/tripFees',
        iconName: 'schedule',
      },
    ],
  },

];
