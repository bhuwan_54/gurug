export const API_URL = `${process.env.REACT_APP_REST_API_HOST}/v1/api`;
export const LOGIN_URL = `${process.env.REACT_APP_REST_API_HOST}/v1/api`;
export const PAGE_SIZE = 10;
export const PAGE_NUMBER = 1;
export const DATE_FORMAT = 'MM/DD/YYYY HH:mm:ss A';
export const READ_ABLE_DATE_FORMAT = 'DD MMM YYYY HH:mm:ss A';
export const DATE_SERVER_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const DATE_SERVER = 'YYYY-MM-DD';
export const USER_FULL_NAME = 'gurug-fullName';
export const JWT_TOKEN = 'gurug-token';
export const PERMISSION_KEY = 'gurug-permissions';
export const DATE_FORMAT_VIEW2 = 'YYYY/MM/DD HH:mm';
export const DATE_FORMAT_VIEW = 'YYYY/MM/DD';
export const ERROR_MESSAGE = { message: 'Server Error. Please Try again' };

export const FIREBASE_CONFIG = {
  apiKey: `${process.env.REACT_APP_FIREBASE_API_KEY}`,
  authDomain: `${process.env.REACT_APP_FIREBASE_AUTH_DOMAIN}`,
  databaseURL: `${process.env.REACT_APP_FIREBASE_DATABASEURL}`,
  projectId: 'locaktm-f3224',
  storageBucket: 'locaktm-f3224.appspot.com',
  messagingSenderId: '496061634063',
  appId: '1:496061634063:web:2e5ea41ba7a6deecc2b57a',
  measurementId: 'G-1V03EY16ZS',
};

export const ROLE_ENTITY={
  'customer_add':'Add Customer',
  'customer_view':'View Customer',
  'customer_list':'List Customer',
  'customer_update':'Update Customer',
  'DRIVER_ADD':'Add Driver',
  'driver_view':'View Driver',
  'DRIVER_DELETE':'Driver Delete',
  'DRIVER_UPDATE':'Update Driver',
};

export const ENTITY_VALUE = {
  'DRIVER': 'Manage Driver Role',
  'CUSTOMER': 'Manage Customer Role',
};

let date = new Date();
let year = date.getFullYear();

export const colorStatus = {
    primary: '#1890ff',
    success: '#66BB6A',
    info: '#01BCD4',
    infoAlt: '#948aec',
    warning: '#ffc53d',
    danger: '#ff4d4f',
    text: '#3D4051',
}

const APPCONFIG = {
  brand: 'GuruG',
  year: year,
  AutoCloseMobileNav: true, // Boolean: true, false. Automatically close sidenav on route change (Mobile only)
  showCustomizer: false, // Boolean: true, false. Customizer will be opened (visible) first time app was loaded if set to true
  color: {
    primary: '#1890ff',
    success: '#66BB6A',
    info: '#01BCD4',
    infoAlt: '#948aec',
    warning: '#ffc53d',
    danger: '#ff4d4f',
    text: '#3D4051',
    gray: '#EDF0F1',
  },
  settings: {
    layout: '1', // String: 1, 2, 3, 4 and add your own
    boxedLayout: false, // Boolean: true, false
    fixedSidenav: false, // Boolean: true, false
    fixedHeader: false, // Boolean: true, false
    collapsedNav: false, // Boolean: true, false
    offCanvasNav: false, // Boolean: true, false
    sidenavWidth: 240, // Number
    offCanvasMobileNav: true, // Boolean: true, false. Mobile only, by default, it's true (off canvas)
    colorOption: '34', // String: 11,12,13,14,15,16; 21,22,23,24,25,26; 31,32,33,34,35,36
    theme: 'light', // (WIP) String: light, gray, dark
  },
};

export default APPCONFIG;
