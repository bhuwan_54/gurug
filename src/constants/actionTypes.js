export const CHANGE_LAYOUT = 'CHANGE_LAYOUT';
export const TOGGLE_BOXED_LAYOUT = 'TOGGLE_BOXED_LAYOUT';
export const TOGGLE_COLLAPSED_NAV = 'TOGGLE_COLLAPSED_NAV';
export const TOGGLE_OFFCANVAS_NAV = 'TOGGLE_OFFCANVAS_NAV';
export const TOGGLE_FIXED_SIDENAV = 'TOGGLE_FIXED_SIDENAV';
export const TOGGLE_FIXED_HEADER = 'TOGGLE_FIXED_HEADER';
export const CHANGE_SIDENAV_WIDTH = 'CHANGE_SIDENAV_WIDTH';
export const TOGGLE_OFFCANVAS_MOBILE_NAV = 'TOGGLE_OFFCANVAS_MOBILE_NAV';
export const CHANGE_COLOR_OPTION = 'CHANGE_COLOR_OPTION';
export const CHANGE_THEME = 'CHANGE_THEME';

export const HTTP_404_ERROR = 'HTTP_404_ERROR';
export const HTTP_500_ERROR = 'HTTP_500_ERROR';

export const DRIVER_FETCH_REQUEST = 'DRIVER_FETCH_REQUEST';
export const DRIVER_FETCH_REQUEST_SUCCESS = 'DRIVER_FETCH_REQUEST_SUCCESS';
export const DRIVER_FETCH_REQUEST_FAILURE = 'DRIVER_FETCH_REQUEST_FAILURE';

export const DRIVER_ADD_REQUEST = 'DRIVER_ADD_REQUEST';
export const DRIVER_ADD_REQUEST_SUCCESS = 'DRIVER_ADD_REQUEST_SUCCESS';
export const DRIVER_ADD_REQUEST_FAILURE = 'DRIVER_ADD_REQUEST_FAILURE';

export const DRIVER_UPDATE_REQUEST = 'DRIVER_UPDATE_REQUEST';
export const DRIVER_UPDATE_REQUEST_SUCCESS = 'DRIVER_UPDATE_REQUEST_SUCCESS';
export const DRIVER_UPDATE_REQUEST_FAILURE = 'DRIVER_UPDATE_REQUEST_FAILURE';

export const DRIVER_CLEAN_REQUEST = 'DRIVER_CLEAN_REQUEST';

export const CUSTOMER_FETCH_REQUEST = 'CUSTOMER_FETCH_REQUEST';
export const CUSTOMER_FETCH_REQUEST_SUCCESS = 'CUSTOMER_FETCH_REQUEST_SUCCESS';
export const CUSTOMER_FETCH_REQUEST_FAILURE = 'CUSTOMER_FETCH_REQUEST_FAILURE';

export const CUSTOMER_ADD_REQUEST = 'CUSTOMER_ADD_REQUEST';
export const CUSTOMER_ADD_REQUEST_SUCCESS = 'CUSTOMER_ADD_REQUEST_SUCCESS';
export const CUSTOMER_ADD_REQUEST_FAILURE = 'CUSTOMER_ADD_REQUEST_FAILURE';

export const CUSTOMER_UPDATE_REQUEST = 'CUSTOMER_UPDATE_REQUEST';
export const CUSTOMER_UPDATE_REQUEST_SUCCESS = 'CUSTOMER_UPDATE_REQUEST_SUCCESS';
export const CUSTOMER_UPDATE_REQUEST_FAILURE = 'CUSTOMER_UPDATE_REQUEST_FAILURE';

export const CUSTOMER_CLEAN_REQUEST = 'CUSTOMER_CLEAN_REQUEST';

export const ROLE_FETCH_REQUEST = 'ROLE_FETCH_REQUEST';
export const ROLE_FETCH_REQUEST_SUCCESS = 'ROLE_FETCH_REQUEST_SUCCESS';
export const ROLE_FETCH_REQUEST_FAILURE = 'ROLE_FETCH_REQUEST_FAILURE';

export const PERMISSION_FETCH_REQUEST = 'PERMISSION_FETCH_REQUEST';
export const PERMISSION_FETCH_REQUEST_SUCCESS = 'PERMISSION_FETCH_REQUEST_SUCCESS';
export const PERMISSION_FETCH_REQUEST_FAILURE = 'PERMISSION_FETCH_REQUEST_FAILURE';

export const ROLE_USER_TYPE_FETCH_REQUEST = 'ROLE_USER_TYPE_FETCH_REQUEST';
export const ROLE_USER_TYPE_FETCH_REQUEST_SUCCESS = 'ROLE_USER_TYPE_FETCH_REQUEST_SUCCESS';
export const ROLE_USER_TYPE_FETCH_REQUEST_FAILURE = 'ROLE_USER_TYPE_FETCH_REQUEST_FAILURE';

export const ROLE_ADD_REQUEST = 'ROLE_ADD_REQUEST';
export const ROLE_ADD_REQUEST_SUCCESS = 'ROLE_ADD_REQUEST_SUCCESS';
export const ROLE_ADD_REQUEST_FAILURE = 'ROLE_ADD_REQUEST_FAILURE';

export const ROLE_UPDATE_REQUEST = 'ROLE_UPDATE_REQUEST';
export const ROLE_UPDATE_REQUEST_SUCCESS = 'ROLE_UPDATE_REQUEST_SUCCESS';
export const ROLE_UPDATE_REQUEST_FAILURE = 'ROLE_UPDATE_REQUEST_FAILURE';

export const ROLE_CLEAN_REQUEST = 'ROLE_CLEAN_REQUEST';

export const GEOFENCING_FETCH_REQUEST = 'GEOFENCING_FETCH_REQUEST';
export const GEOFENCING_FETCH_REQUEST_SUCCESS = 'GEOFENCING_FETCH_REQUEST_SUCCESS';
export const GEOFENCING_FETCH_REQUEST_FAILURE = 'GEOFENCING_FETCH_REQUEST_FAILURE';

export const GEOFENCING_ADD_REQUEST = 'GEOFENCING_ADD_REQUEST';
export const GEOFENCING_ADD_REQUEST_SUCCESS = 'GEOFENCING_ADD_REQUEST_SUCCESS';
export const GEOFENCING_ADD_REQUEST_FAILURE = 'GEOFENCING_ADD_REQUEST_FAILURE';

export const GEOFENCING_UPDATE_REQUEST = 'GEOFENCING_UPDATE_REQUEST';
export const GEOFENCING_UPDATE_REQUEST_SUCCESS = 'GEOFENCING_UPDATE_REQUEST_SUCCESS';
export const GEOFENCING_UPDATE_REQUEST_FAILURE = 'GEOFENCING_UPDATE_REQUEST_FAILURE';

export const GEOFENCING_CLEAN_REQUEST = 'GEOFENCING_CLEAN_REQUEST';

export const PROMO_CODE_FETCH_REQUEST = 'PROMO_CODE_FETCH_REQUEST';
export const PROMO_CODE_FETCH_REQUEST_SUCCESS = 'PROMO_CODE_FETCH_REQUEST_SUCCESS';
export const PROMO_CODE_FETCH_REQUEST_FAILURE = 'PROMO_CODE_FETCH_REQUEST_FAILURE';

export const PROMO_CODE_DISCOUNT_FETCH_REQUEST = 'PROMO_CODE_DISCOUNT_FETCH_REQUEST';
export const PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS =
  'PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS';
export const PROMO_CODE_DISCOUNT_FETCH_REQUEST_FAILURE =
  'PROMO_CODE_DISCOUNT_FETCH_REQUEST_FAILURE';

export const PROMO_CODE_ADD_REQUEST = 'PROMO_CODE_ADD_REQUEST';
export const PROMO_CODE_ADD_REQUEST_SUCCESS = 'PROMO_CODE_ADD_REQUEST_SUCCESS';
export const PROMO_CODE_ADD_REQUEST_FAILURE = 'PROMO_CODE_ADD_REQUEST_FAILURE';

export const PROMO_CODE_UPDATE_REQUEST = 'PROMO_CODE_UPDATE_REQUEST';
export const PROMO_CODE_UPDATE_REQUEST_SUCCESS = 'PROMO_CODE_UPDATE_REQUEST_SUCCESS';
export const PROMO_CODE_UPDATE_REQUEST_FAILURE = 'PROMO_CODE_UPDATE_REQUEST_FAILURE';

export const PROMO_CODE_CLEAN_REQUEST = 'PROMO_CODE_CLEAN_REQUEST';
export const PROMO_CODE = 'PROMO_CODE';

export const LIVE_PREVIEW_FETCH_REQUEST = 'LIVE_PREVIEW_FETCH_REQUEST';
export const LIVE_PREVIEW_FETCH_REQUEST_SUCCESS = 'LIVE_PREVIEW_FETCH_REQUEST_SUCCESS';
export const LIVE_PREVIEW_FETCH_REQUEST_FAILURE = 'LIVE_PREVIEW_FETCH_REQUEST_FAILURE';

export const TOTAL_TRIP_FETCH_REQUEST = 'TOTAL_TRIP_FETCH_REQUEST';
export const TOTAL_TRIP_FETCH_REQUEST_SUCCESS = 'TOTAL_TRIP_FETCH_REQUEST_SUCCESS';
export const TOTAL_TRIP_FETCH_REQUEST_FAILURE = 'TOTAL_TRIP_FETCH_REQUEST_FAILURE';

export const TRIP_DETAIL_FETCH_REQUEST = 'TRIP_DETAIL_FETCH_REQUEST';
export const TRIP_DETAIL_FETCH_REQUEST_SUCCESS = 'TRIP_DETAIL_FETCH_REQUEST_SUCCESS';
export const TRIP_DETAIL_FETCH_REQUEST_FAILURE = 'TRIP_DETAIL_FETCH_REQUEST_FAILURE';

export const LIVE_PREVIEW_ADD_REQUEST = 'LIVE_PREVIEW_ADD_REQUEST';
export const LIVE_PREVIEW_ADD_REQUEST_SUCCESS = 'LIVE_PREVIEW_ADD_REQUEST_SUCCESS';
export const LIVE_PREVIEW_ADD_REQUEST_FAILURE = 'LIVE_PREVIEW_ADD_REQUEST_FAILURE';

export const LIVE_PREVIEW_UPDATE_REQUEST = 'LIVE_PREVIEW_UPDATE_REQUEST';
export const LIVE_PREVIEW_UPDATE_REQUEST_SUCCESS = 'LIVE_PREVIEW_UPDATE_REQUEST_SUCCESS';
export const LIVE_PREVIEW_UPDATE_REQUEST_FAILURE = 'LIVE_PREVIEW_UPDATE_REQUEST_FAILURE';

export const LIVE_PREVIEW_CLEAN_REQUEST = 'LIVE_PREVIEW_CLEAN_REQUEST';

export const SUPPORT_TICKET_FETCH_REQUEST = 'SUPPORT_TICKET_FETCH_REQUEST';
export const SUPPORT_TICKET_FETCH_REQUEST_SUCCESS = 'SUPPORT_TICKET_FETCH_REQUEST_SUCCESS';
export const SUPPORT_TICKET_FETCH_REQUEST_FAILURE = 'SUPPORT_TICKET_FETCH_REQUEST_FAILURE';

export const EMPLOYEE_FETCH_REQUEST = 'EMPLOYEE_FETCH_REQUEST';
export const EMPLOYEE_FETCH_REQUEST_SUCCESS = 'EMPLOYEE_FETCH_REQUEST_SUCCESS';
export const EMPLOYEE_FETCH_REQUEST_FAILURE = 'EMPLOYEE_FETCH_REQUEST_FAILURE';

export const SUPPORT_TICKET_ASSIGN_REQUEST = 'SUPPORT_TICKET_ASSIGN_REQUEST';
export const SUPPORT_TICKET_ASSIGN_REQUEST_SUCCESS = 'SUPPORT_TICKET_ASSIGN_REQUEST_SUCCESS';
export const SUPPORT_TICKET_ASSIGN_REQUEST_FAILURE = 'SUPPORT_TICKET_ASSIGN_REQUEST_FAILURE';

export const SUPPORT_TICKET_CLEAN_REQUEST = 'SUPPORT_TICKET_CLEAN_REQUEST';

export const REVIEW_FETCH_REQUEST = 'REVIEW_FETCH_REQUEST';
export const REVIEW_FETCH_REQUEST_SUCCESS = 'REVIEW_FETCH_REQUEST_SUCCESS';
export const REVIEW_FETCH_REQUEST_FAILURE = 'REVIEW_FETCH_REQUEST_FAILURE';

export const REVIEW_CLEAN_REQUEST = 'REVIEW_CLEAN_REQUEST';

export const NOTIFICATION_FETCH_REQUEST = 'NOTIFICATION_FETCH_REQUEST';
export const NOTIFICATION_FETCH_REQUEST_SUCCESS = 'NOTIFICATION_FETCH_REQUEST_SUCCESS';
export const NOTIFICATION_FETCH_REQUEST_FAILURE = 'NOTIFICATION_FETCH_REQUEST_FAILURE';

export const NOTIFICATION_ADD_REQUEST = 'NOTIFICATION_ADD_REQUEST';
export const NOTIFICATION_ADD_REQUEST_SUCCESS = 'NOTIFICATION_ADD_REQUEST_SUCCESS';
export const NOTIFICATION_ADD_REQUEST_FAILURE = 'NOTIFICATION_ADD_REQUEST_FAILURE';

export const NOTIFICATION_CLEAN_REQUEST = 'NOTIFICATION_CLEAN_REQUEST';

export const DRIVER_COMMISSION_FETCH_REQUEST = 'DRIVER_COMMISSION_FETCH_REQUEST';
export const DRIVER_COMMISSION_FETCH_REQUEST_SUCCESS = 'DRIVER_COMMISSION_FETCH_REQUEST_SUCCESS';
export const DRIVER_COMMISSION_FETCH_REQUEST_FAILURE = 'DRIVER_COMMISSION_FETCH_REQUEST_FAILURE';

export const DRIVER_COMMISSION_ADD_REQUEST = 'DRIVER_COMMISSION_ADD_REQUEST';
export const DRIVER_COMMISSION_ADD_REQUEST_SUCCESS = 'DRIVER_COMMISSION_ADD_REQUEST_SUCCESS';
export const DRIVER_COMMISSION_ADD_REQUEST_FAILURE = 'DRIVER_COMMISSION_ADD_REQUEST_FAILURE';

export const DRIVER_COMMISSION_TYPE_REQUEST = 'DRIVER_COMMISSION_TYPE_REQUEST';
export const DRIVER_COMMISSION_TYPE_REQUEST_SUCCESS = 'DRIVER_COMMISSION_TYPE_REQUEST_SUCCESS';
export const DRIVER_COMMISSION_TYPE_REQUEST_FAILURE = 'DRIVER_COMMISSION_TYPE_REQUEST_FAILURE';

export const COMMISSION_PROFILE_FETCH_REQUEST = 'COMMISSION_PROFILE_FETCH_REQUEST';
export const COMMISSION_PROFILE_FETCH_REQUEST_SUCCESS = 'COMMISSION_PROFILE_FETCH_REQUEST_SUCCESS';
export const COMMISSION_PROFILE_FETCH_REQUEST_FAILURE = 'COMMISSION_PROFILE_FETCH_REQUEST_FAILURE';

export const DRIVER_COMMISSION_CLEAN_REQUEST = 'DRIVER_COMMISSION_CLEAN_REQUEST';

export const TRIP_FEE_FETCH_REQUEST = 'TRIP_FEE_FETCH_REQUEST';
export const TRIP_FEE_FETCH_REQUEST_SUCCESS = 'TRIP_FEE_FETCH_REQUEST_SUCCESS';
export const TRIP_FEE_FETCH_REQUEST_FAILURE = 'TRIP_FEE_FETCH_REQUEST_FAILURE';

export const TRIP_FEE_ADD_REQUEST = 'TRIP_FEE_ADD_REQUEST';
export const TRIP_FEE_ADD_REQUEST_SUCCESS = 'TRIP_FEE_ADD_REQUEST_SUCCESS';
export const TRIP_FEE_ADD_REQUEST_FAILURE = 'TRIP_FEE_ADD_REQUEST_FAILURE';

export const TRIP_FEE_CLEAN_REQUEST = 'TRIP_FEE_CLEAN_REQUEST';

export const PROFILE_FETCH_REQUEST = 'PROFILE_FETCH_REQUEST';
export const PROFILE_FETCH_REQUEST_SUCCESS = 'PROFILE_FETCH_REQUEST_SUCCESS';
export const PROFILE_FETCH_REQUEST_FAILURE = 'PROFILE_FETCH_REQUEST_FAILURE';

export const PROFILE_UPDATE_REQUEST = 'PROFILE_UPDATE_REQUEST';
export const PROFILE_UPDATE_REQUEST_SUCCESS = 'PROFILE_UPDATE_REQUEST_SUCCESS';
export const PROFILE_UPDATE_REQUEST_FAILURE = 'PROFILE_UPDATE_REQUEST_FAILURE';

export const SECURITY_QUESTION_FETCH_REQUEST = 'SECURITY_QUESTION_FETCH_REQUEST';
export const SECURITY_QUESTION_FETCH_REQUEST_SUCCESS = 'SECURITY_QUESTION_FETCH_REQUEST_SUCCESS';
export const SECURITY_QUESTION_FETCH_REQUEST_FAILURE = 'SECURITY_QUESTION_FETCH_REQUEST_FAILURE';

export const DASHBOARD_FETCH_REQUEST = 'DASHBOARD_FETCH_REQUEST';
export const DASHBOARD_FETCH_REQUEST_SUCCESS = 'DASHBOARD_FETCH_REQUEST_SUCCESS';
export const DASHBOARD_FETCH_REQUEST_FAILURE = 'DASHBOARD_FETCH_REQUEST_FAILURE';
