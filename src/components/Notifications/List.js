import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form,Tag, Button } from 'antd';
import { EyeOutlined, HomeOutlined, EditOutlined } from '@ant-design/icons';
import moment from 'moment';

import { PAGE_SIZE, colorStatus, PAGE_NUMBER,READ_ABLE_DATE_FORMAT } from '../../constants/appConfig';
import Message from '../Common/Message';
import { searchItemLayout, getSortingOrder } from '../../utils/commonUtil';

const Search = Input.Search;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [form] = Form.useForm();

  const { notifications, fetchNotificationWithCriteria, errors, pagination, loading, cleanNotificationProps } = props;
  const { validateFields, } = form;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchNotificationWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = PAGE_NUMBER ;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchNotificationWithCriteria(fieldsValue);
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Title',
      dataIndex: 'title',
      align: 'left',
      render: (text, record) => {
        return <div>{`${record.title}`}</div>;
      },
    },
    {
      title: 'Body',
      dataIndex: 'body',
      align: 'left',
    },
    {
      title: 'User Type',
      dataIndex: 'userType',
      align: 'left',
    },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      render: (text, record) => {
        return <div>{moment(record.createdOn).format(READ_ABLE_DATE_FORMAT)}</div>;
      },
    },
  ];

  useEffect(() => {
    fetchNotificationWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });
    return () => {
      cleanNotificationProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Notifications List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Notifications</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="search-form mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size="middle"
                      bordered
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={notifications instanceof Array ? notifications : []}
                      pagination={{
                        total: pagination.totalData,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
