import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import {  Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  DatePicker,
  InputNumber,
  Checkbox,
  Typography,
  Steps,
  Affix,
  Card,
  Tabs,
} from 'antd';

import Message from '../Common/Message';
import { HomeOutlined } from '@ant-design/icons';
import history from '../../utils/history';

const FormItem = Form.Item;
const { Option } = Select;

const AddForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();
  const [keyValue, setKeyValue] = useState(1);

  const [buttonLoading, setButttonLoading] = useState(false);
  const { notifications,loading, errors, addNotification, cleanNotificationProps } = props;

  const { validateFields, getFieldValue } = form;

  const onFinish = () => {
    validateFields().then(values => {
      addNotification(values);
    });
  };
  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 12 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 6 },
      lg: { span: 24, offset: 6 },
      md: { span: 12, offset: 0 },
      sm: { span: 12, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  const handleCancel = () => {
    form.resetFields();
  };

  const handleBack = () => {
    history.push('/notifications');
  };

  useEffect(() => {
    return () => {
      cleanNotificationProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Add Notification</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined/>
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/notifications'}>Notifications</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>New
                </Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors}/>
          <Card>
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <Row type="flex" justify="center">
              <div className="box-body edit-driver">
                <section className="form-v1-container">
                  <Form
                    {...formItemLayout}
                    name="updateForm"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                    form={form}
                  >
                    <Row>
                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'title'}
                          rules={[{ required: true, message: 'Notification message title is required.' }, {
                            max: 50,
                            message: 'Please enter less than 50 character.',
                          }]}
                          label="Notification Message Title"
                        >
                          <Input.TextArea placeholder={'Notification message title '}/>
                        </FormItem>
                      </Col>
                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'body'}
                          rules={[{ required: true, message: 'Notification message body is required.' }, {
                            max: 150,
                            message: 'Please enter less than 150 character.',
                          }]}
                          label="Notification Message Body"
                        >
                          <Input.TextArea placeholder={'Notification message body '}/>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'notificationType'}
                          rules={[{ required: true, message: 'Notification type is required.' }]}
                          label="Notification Type"
                        >
                          <Select
                            showSearch
                            placeholder="Select notification type"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            <Option value="promocode">Promo code</Option>
                            <Option value="genral">GENERAL</Option>
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'to'}
                          rules={[{ required: true, message: 'Receivers type is required.' }]}
                          label="Receviers"
                        >
                          <Select
                            showSearch
                            placeholder="Select recevier type"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            <Option value="ALL">ALL</Option>
                            <Option value="driver">DRIVER</Option>
                            <Option value="customer">CUSTOMER</Option>
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={16} lg={24} md={24} sm={24}>
                        <FormItem {...submitFormLayout}>
                          <Button
                            className="btn-custom-primary mr-1"
                            shape="round"
                            htmlType="submit"
                            size={'large'}
                            loading={loading}
                          >
                            Add
                          </Button>

                          <Button
                            className=" mr-1"
                            shape="round"
                            onClick={handleCancel}
                            size={'large'}
                            danger
                          >
                            Cancel
                          </Button>
                          <Button
                            className=" mr-1"
                            shape="round"
                            onClick={handleBack}
                            size={'large'}
                          >
                            Back
                          </Button>
                        </FormItem>
                      </Col>
                    </Row>
                  </Form>
                </section>
              </div>
              </Row>
            </Spin>
          </Card>

        </article>
      </QueueAnim>
    </div>
  );
};

export default AddForm;
