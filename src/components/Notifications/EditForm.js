import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  DatePicker,
  InputNumber,
  Checkbox,
  Typography,
  Affix,
} from 'antd';
import moment from 'moment';
import {  HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import { DATE_FORMAT_VIEW, DATE_SERVER_FORMAT } from '../../constants/appConfig';
import { isEmpty } from '../../utils/commonUtil';
import ImageUploadManually from '../Common/ImageUpload/ImageUploadManually';
import history from '../../utils/history';
import ApproveModel from '../Common/Modal/ApproveModal';

const FormItem = Form.Item;
const { Title } = Typography;
const { Option } = Select;

// const drivers = {
//   id: 1,
//   fullName: 'Ashish Shrestha',
//   mobileNumber: '9808997878',
//   emailId: 'email@email.com',
//   city: 'Kathmandu',
//   streetNumber: '123',
//   vehicleType: 'Bike',
//   dob: '2020-02-20',
//   vehicleCC: 250,
//   vehicleBrandModel: 'ABC',
//   vehicleLicensePlate: 'BA 2 PA 9876',
//   expiryDate: null,
//   issueDate: '2020-02-20',
//   identityFrontPhoto: 'driver/1DRIVER_IDENTITY_FRONT',
//   identityBackPhoto: 'driver/1DRIVER_IDENTITY_BACK',
//   photoPerson: 'driver/1DRIVER_PHOTO',
//   photoVehicleFront: 'driver/1DRIVER_VEHICLE_FRONT',
//   photoVehicleBack: 'driver/1DRIVER_VEHICLE_BACK',
//   photoVehicleSide: 'driver/1DRIVER_VEHICLE_SIDE',
//   deviceUid: '123456',
//   deviceMac: '123456',
//   createdBy: 'admin@gmail.com',
//   updatedBy: 'yessss',
//   verifiedBy: null,
//   active: true,
//   verified: true,
// };

const EditForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const [buttonLoading, setButttonLoading] = useState(false);
  const { drivers,loading, errors, fetchDriverByIdentifier, updateDriverStatus, updateDriver, cleanDriverProps } = props;
  const { setFieldsValue, setFields, validateFields, getFieldValue } = form;

  const onFinish = values => {
    validateFields().then(values => {
      values.id = id;
      const formData = new FormData();
      if (values) {
        Object.keys(values).map(key => {
          formData.append(
            key,
            Array.isArray(values[key])
              ? values[key][0].originFileObj
              : moment.isMoment(values[key])
              ? moment(values[key], DATE_SERVER_FORMAT)
                .startOf('days')
                .format(DATE_SERVER_FORMAT)
              : values[key],
          );
        });
      }
      updateDriver(formData);
    });
  };


  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 5 },
      lg: { span: 24, offset: 5 },
      md: { span: 24, offset: 0 },
      sm: { span: 24, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  const driverPhotoPreview = getFieldValue('photoPerson');
  const driverIDFrontPhotoPreview = getFieldValue('identityFrontPhoto');
  const driverIDBackPhotoPreview = getFieldValue('identityBackPhoto');

  const driverIdFrontPhotoSource = drivers ? drivers.identityFrontPhoto : '';
  const driverIdBackPhotoSource = drivers ? drivers.identityBackPhoto : '';
  const driverPhotoSource = drivers ? drivers.photoPerson : '';

  const vehicleFrontPhotoSource = drivers ? drivers.photoVehicleFront : '';
  const vehicleBackPhotoSource = drivers ? drivers.photoVehicleBack : '';
  const vehicleSidePhotoSource = drivers ? drivers.photoVehicleSide : '';


  const handleInitialValue = values => {
    if (values) {
      Object.keys(values).map(key => {
        if (key === 'dob' || key === 'expiryDate' || key === 'issueDate') {
          values[key] = values[key] && moment(values[key], DATE_FORMAT_VIEW);
        }
        if(key === 'photoPerson'||'photoVehicleFront' || 'photoVehicleBack' ||'photoVehicleSide'){
          values[key]= undefined;
        }
      });
      return values;
    }
  };

  useEffect(() => {
    form.setFieldsValue({
      id: id,
    });
    fetchDriverByIdentifier(id);
    return () => {
      cleanDriverProps();
    };
  }, []);

  const handleBack = () => {
    history.push('/drivers');
  };

  const handleCancel = () => {
    form.resetFields();
  };

  const modalProps = {
    title: drivers.active ? 'Are you sure want to deactivate ?' : 'Are you sure want to activate ?',
    apiUrlKey: drivers.active ? 'disable' : 'activate',
    width: '380px',
    id: id,
    approveEntity: updateDriverStatus,
  };

  const verifyModalProps = {
    title: 'Verify the driver record ?',
    apiUrlKey: 'verify',
    width: '400px',
    id: id,
    approveEntity: updateDriverStatus,
  };
  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Update Driver</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined/>
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/drivers'}>Drivers</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Update</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors}/>

          <div className="box box-default">
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    form={form}
                    {...formItemLayout}
                    name="updateForm"
                    initialValues={drivers ? handleInitialValue(drivers): ''}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                  >
                    <Row gutter={18}>
                      <Row justify="end" gutter={[8, 16]}>
                        <Col xl={5} lg={5} md={12} sm={18} xs={18}>
                          <Affix offsetTop={10}>
                            <Button type="dashed" shape="round" block onClick={() => ApproveModel(modalProps)}>
                              {drivers && drivers.active ? 'Diactivate' : 'Activate'}
                            </Button>
                          </Affix>
                        </Col>
                        {
                          !drivers.verified && <Col xl={5} lg={5} md={12} sm={18} xs={18}>
                            <Affix offsetTop={10}>
                              <Button type="dashed" shape="round" block onClick={() => ApproveModel(verifyModalProps)}>
                                Verify
                              </Button>
                            </Affix>
                          </Col>
                        }
                      </Row>
                      <Row>
                        <Title level={4}>Personal Information</Title>
                        <Divider/>
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'fullName'}
                              label="Full Name"
                              rules={[{ required: true, message: 'Full name is required.' }]}
                            >
                              <Input/>
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'emailId'}
                              label={'Email'}
                              rules={[
                                { required: true, message: 'Email is required' },
                                {
                                  type: 'email',
                                  message: 'Please enter valid email address.',
                                },
                              ]}
                            >
                              <Input/>
                            </FormItem>
                          </Col>
                        </Row>
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'mobileNumber'}
                              rules={[{ required: true, message: 'Mobile number is required.' }]}
                              label="Mobile Number"
                            >
                              <Input/>
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'dob'}
                              label="Date of Birth"
                              rules={[{ required: true, message: 'Date of birth is required.' }]}
                            >
                              <DatePicker format={DATE_FORMAT_VIEW}/>
                            </FormItem>
                          </Col>
                        </Row>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'streetNumber'}
                            label="Street Number"
                            rules={[{ required: true, message: 'Street number is required.' }]}
                          >
                            <InputNumber/>
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'city'}
                            label="City"
                            rules={[{ required: true, message: 'City is required.' }]}
                          >
                            <Input/>
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            name={'personPhoto'}
                            label="Person Photo"
                            rules={[
                              {
                                required: true,
                                message: 'Person photo is required',
                              },
                            ]}
                          >
                            <ImageUploadManually
                              {...props}
                              fileName="personPhoto"
                              acceptType="image/jpeg,image/png"
                              placeholder="Person photo"
                              fileType="picture"
                              sizeOfFile="512000"
                              form={form}
                            />
                            {driverPhotoPreview === undefined && (
                              <div>
                                <img
                                  src={`${driverPhotoSource}?${Date.now()}`}
                                  className="img-thumbnail m-1"
                                  alt="Vechile photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            label="Licence Number"
                            name={'licenceNumber'}
                            rules={[{ required: true, message: 'Licence number is required.' }]}
                          >
                            <InputNumber/>
                          </FormItem>
                        </Col>

                        <Title level={4}>Vehicle Information</Title>
                        <Divider/>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'vehicleType'}
                            rules={[{ required: true, message: 'Vehicle type is required.' }]}
                            label="Vehicle Type"
                          >
                            <Select
                              showSearch
                              placeholder="Select vehicle type"
                              optionFilterProp="children"
                              filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
                                0
                              }
                            >
                              <Option value="Bike">Bike</Option>
                              <Option value="Taxi">Taxi</Option>
                            </Select>
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'vehicleCC'}
                            label="Vehicle CC"
                            rules={[{ required: true, message: 'Vehicle CC is required.' }]}
                          >
                            <InputNumber/>
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'vehicleBrandModel'}
                            label="Vehicle Brand Model"
                            rules={[{ required: true, message: 'Vehicle brand is required.' }]}
                          >
                            <Input/>
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem name={'vehicleLicensePlate'} label="Vehicle License Plate">
                            <Input/>
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem name={'photoVehicleFront'} label="Vehicle Front Photo">
                            <ImageUploadManually
                              {...props}
                              fileName="photoVehicleFront"
                              acceptType="image/jpeg,image/png"
                              placeholder="Vehicle front photo"
                              fileType="picture"
                              sizeOfFile="512000"
                              form={form}
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${vehicleFrontPhotoSource}?${Date.now()}`}
                                  className="img-thumbnail m-1"
                                  alt="Vechile photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem name={'photoVehicleBack'} label="Vehicle Back Photo">
                            <ImageUploadManually
                              {...props}
                              fileName="photoVehicleBack"
                              acceptType="image/jpeg,image/png"
                              placeholder="Vehicle back photo"
                              fileType="picture"
                              sizeOfFile="512000"
                              form={form}
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${vehicleBackPhotoSource}?${Date.now()}`}
                                  className="img-thumbnail m-1"
                                  alt="Vechile"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem name={'photoVehicleSide'} label="Vehicle Side Photo">
                            <ImageUploadManually
                              {...props}
                              fileName="photoVehicleSide"
                              acceptType="image/jpeg,image/png"
                              placeholder="Vehicle side photo"
                              fileType="picture"
                              sizeOfFile="512000"
                              form={form}
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${vehicleSidePhotoSource}?${Date.now()}`}
                                  className="img-thumbnail m-1"
                                  alt="Vechile"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}/>

                        <Title level={4}>Identification </Title>
                        <Divider/>
                        <Row>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              name={'issueDate'}
                              label="Issue Date"
                              rules={[{ required: true, message: 'ID issue date is required.' }]}
                            >
                              <DatePicker format={DATE_FORMAT_VIEW}/>
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem name={'expiryDate'} label="Expiry Date">
                              <DatePicker format={DATE_FORMAT_VIEW}/>
                            </FormItem>
                          </Col>
                        </Row>

                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            label="ID Front Photo"
                            name={'identityFrontPhoto'}
                            rules={[
                              {
                                required: false,
                                message: 'Id front photo is required',
                              },
                            ]}
                          >
                            <ImageUploadManually
                              {...props}
                              fileName="identityFrontPhoto"
                              acceptType="image/jpeg,image/png"
                              placeholder="ID front photo"
                              fileType="picture"
                              sizeOfFile="512000"
                              form={form}
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${driverIdFrontPhotoSource}?${Date.now()}`}
                                  className="img-thumbnail m-1"
                                  alt="Identity photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            name={'identityBackPhoto'}
                            label="ID Back Photo"
                            rules={[
                              {
                                required: isEmpty(drivers),
                                message: 'Id back photo is required',
                              },
                            ]}
                          >
                            <ImageUploadManually
                              {...props}
                              fileName="identityBackPhoto"
                              acceptType="image/jpeg,image/png"
                              placeholder="ID back photo"
                              fileType="picture"
                              sizeOfFile="512000"
                              form={form}
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${driverIdBackPhotoSource}?${Date.now()}`}
                                  className="img-thumbnail m-1"
                                  alt="Identity photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Title level={4}>Others </Title>
                        <Divider/>
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              label="Device UID"
                              name={'deviceUid'}
                              rules={[{ required: true, message: 'Device UID is required.' }]}
                            >
                              <InputNumber/>
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'deviceMac'}
                              label="Device MAc"
                              rules={[{ required: true, message: 'Device MAC is required.' }]}
                            >
                              <InputNumber/>
                            </FormItem>
                          </Col>
                        </Row>
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'createdBy'}
                              rules={[{ required: true, message: 'Created by is required.' }]}
                              label="Created By"
                            >
                              <Input/>
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              label="Updated By"
                              name={'updatedBy'}
                              rules={[{ required: true, message: 'Updated by is required.' }]}
                            >
                              <Input/>
                            </FormItem>
                          </Col>
                        </Row>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem label="Status" name={'active'} valuePropName="checked">
                            <Checkbox/>
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            name={'verified'}
                            label="Verified"
                            rules={[{ required: true, message: 'Verified is required.' }]}
                          >
                            <Input/>
                          </FormItem>
                        </Col>

                        <Row>
                          <FormItem {...submitFormLayout}>
                            <Affix offsetBottom={30}>
                              <Button
                                className="btn-custom-primary mr-1"
                                shape="round"
                                loading={loading}
                                htmlType="submit"
                                size={'large'}
                              >
                                Update
                              </Button>
                              <Button
                                className=" mr-1"
                                shape="round"
                                onClick={handleCancel}
                                size={'large'}
                                danger
                              >
                                Cancel
                              </Button>
                              <Button
                                className=" mr-1"
                                shape="round"
                                onClick={handleBack}
                                size={'large'}
                              >
                                Back
                              </Button>
                            </Affix>
                          </FormItem>
                        </Row>
                      </Row>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
  );
};

export default EditForm;
