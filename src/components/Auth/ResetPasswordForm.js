import React from 'react';
import { Form, Input, Divider, Button } from 'antd';
import { MailOutlined } from '@ant-design/icons';

import './index.css';
import LogoImage from '../../assets/Guruji.png';

const FormItem = Form.Item;

const ResetPasswordForm = () => {
  const [form] = Form.useForm();
  const onFinish = () => {};

  return (
    <div className="reset-box">
      <div className="box box-default">
        <div className="box-body p-5">
          <section className="form-v1-container">
            <h4 className="company-title">Forget Password</h4>
            <Divider />
            <div className="logo">
              <img src={LogoImage} alt="Logo" style={{ width: 130 }} />
            </div>
            <p className="additional-info col-lg-10 mx-lg-auto mb-3">
              Enter the email address you used when you joined and we’ll send you instructions to
              reset your password.
            </p>
            <Form onFinish={onFinish} form={form} className="form-v1">
              <FormItem
                className="mb-3"
                name={'resetpassword1-email'}
                rules={[
                  { type: 'email', message: 'The input is not valid E-mail!' },
                  { required: true, message: 'Please input your email!' },
                ]}
              >
                <Input
                  size="large"
                  prefix={<MailOutlined className={'site-form-item-icon'} />}
                  placeholder="Email"
                />
              </FormItem>
              <FormItem>
                <Button type="primary" htmlType="submit" className="btn-cta btn-block">
                  Send Reset Instructions
                </Button>
              </FormItem>
            </Form>
          </section>
        </div>
      </div>
    </div>
  );
};

export default ResetPasswordForm;
