import React, { useContext, useEffect } from 'react';
import { Form, Input, Button, Checkbox, Spin } from 'antd';
import { Link } from 'react-router-dom';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

import APPCONFIG from 'constants/appConfig';
import DEMO from 'constants/demoData';
import './index.css';
import LogoImage from '../../assets/Guruji.png';
import { AuthContext } from './AuthContext';
import Message from '../Common/Message';

const FormItem = Form.Item;

const LoginForm = () => {
  const { loading, setLoading, login, message, setMessage } = useContext(AuthContext);
  const [form] = Form.useForm();
  const { validateFields } = form;

  useEffect(() => {
    return () => {
      setLoading(false);
      setMessage(message);
    };
  }, []);

  const onFinish = values => {
    validateFields().then(values => {
      login(values);
    });
  };

  return (
    <Spin spinning={loading} delay={1000} size={'large'}>
      <div className="login-box">
        <div className="box box-default">
          <div className="box-body p-5">
            <section className="form-v1-container">
              <div className="logo">
                <img src={LogoImage} style={{ width: 130 }} alt={APPCONFIG.brand} />
              </div>
              <Message error={message} />

              <Form
                form={form}
                name="normal_login"
                className="login-form"
                initialValues={{ remember: true }}
                onFinish={onFinish}
              >
                <FormItem
                  name={'username'}
                  rules={[{ required: true, message: 'Please input your username' }]}
                >
                  <Input
                    size="large"
                    prefix={<UserOutlined className="site-form-item-icon" />}
                    placeholder="Username"
                  />
                </FormItem>
                <FormItem
                  name={'password'}
                  rules={[{ required: true, message: 'Please input your Password' }]}
                >
                  <Input.Password
                    size="large"
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                  />
                </FormItem>
                <Form.Item name="remember" valuePropName="checked">
                  <Checkbox>Remember me</Checkbox>
                </Form.Item>
                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={loading}
                    className="btn-cta btn-block"
                  >
                    Log in
                  </Button>
                </FormItem>
              </Form>
              <p className="additional-info">
                Forgot your username or password?
                <Link to={'/reset'} className="login-form-forgot">
                  Forgot password?
                </Link>
              </p>
            </section>
          </div>
        </div>
      </div>
    </Spin>
  );
};

export default LoginForm;
