import React, { createContext, useState } from 'react';
import axios from 'axios';

import {
  API_URL,
  ERROR_MESSAGE,
  JWT_TOKEN,
  LOGIN_URL,
  USER_FULL_NAME,
  PERMISSION_KEY,
} from '../../constants/appConfig';
import { clearLocalStorage, getLocalStorage, setLocalStorage } from '../../utils/storageUtil';
import { isAuthenticated } from '../../utils/jwtUtil';
import configureStore from '../../store/configureStore';
import history from '../../utils/history';

const store = configureStore({}, history);

const AuthContext = createContext({
  user: {},
  isAuthenticated: false,
});

const AuthProvider = props => {
  const [user, setUser] = useState(getLocalStorage('user') || {});
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const [authenticated, setAuthenticated] = useState(isAuthenticated() || false);
  const state = { user, loading, authenticated };

  const login = ({ username, password }) => {
    setLoading(true);
    // history.push('/dashboard');

    return axios
      .post(LOGIN_URL + '/auth/admin', { username, password })
      .then(response => {
        setLocalStorage(JWT_TOKEN, response.data.accessToken);
        setLocalStorage(PERMISSION_KEY, response.data.permissions);
        history.push('/dashboard');
        setAuthenticated(true);
        return response;
      })
      .catch(error => {
        setLoading(false);
        setMessage(error.response ? error.response.data : ERROR_MESSAGE);
      });
  };

  const logout = () => {
    store.dispatch({ type: 'LOG_OUT_SUCCESS' });
    clearLocalStorage(JWT_TOKEN);
    clearLocalStorage(PERMISSION_KEY);
    setUser({});
    setAuthenticated(false);
    history.push('/');
  };

  return (
    <AuthContext.Provider
      {...props}
      value={{
        ...state,
        message: message,
        setMessage: setMessage,
        loading: loading,
        setLoading: setLoading,
        login: login,
        logout: logout,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export { AuthProvider, AuthContext };
