import React from 'react';
import { connect } from 'react-redux';
import APPCONFIG from 'constants/appConfig';

import App from './App';
import AppV2 from './AppV2';
import ContentOnly from './ContentOnly';
import HeaderContentFooter from './HeaderContentFooter';

const AppLayout = props => {
  const { layout, boxedLayout, fixedSidenav, fixedHeader } = props;

  const updateLayout = (layout, boxedLayout, fixedSidenav, fixedHeader) => {
    switch (layout) {
      case '1':
        return (
          <App
            boxedLayout={boxedLayout}
            fixedSidenav={fixedSidenav}
            fixedHeader={fixedHeader}
            {...props}
          />
        );
      case '2':
        return <AppV2 boxedLayout={boxedLayout} {...props} />;
      case '3':
        return (
          <HeaderContentFooter boxedLayout={boxedLayout} fixedHeader={fixedHeader} {...props} />
        );
      case '4':
        return <ContentOnly boxedLayout={boxedLayout} {...props} />;
      default:
        return <App />;
    }
  };

  return (
    <div id="app-layout-container">
      {updateLayout(layout, boxedLayout, fixedSidenav, fixedHeader)}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  layout: state.settings.layout,
  boxedLayout: state.settings.boxedLayout,
  fixedSidenav: state.settings.fixedSidenav,
  fixedHeader: state.settings.fixedHeader,
});

export default connect(mapStateToProps)(AppLayout);
