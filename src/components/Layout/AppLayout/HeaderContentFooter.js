import React from 'react';
import classnames from 'classnames';
import { Layout } from 'antd';
import AppHeader from 'components/Layout/Header';
import AppFooter from 'components/Layout/Footer';

const AppLayout = props => {
  const { boxedLayout, fixedHeader } = props;

  return (
    <Layout
      id="app-layout"
      className={classnames('app-layout', {
        'boxed-layout': boxedLayout,
        'fixed-header': fixedHeader,
      })}
    >
      <AppHeader showLogo={true} />
      {props.children}
      <AppFooter />
    </Layout>
  );
};

export default AppLayout;
