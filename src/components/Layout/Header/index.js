import React, { useContext } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import DEMO from 'constants/demoData';
import { Link } from 'react-router-dom';
import { Layout, Menu, Dropdown, Avatar, Button, Divider } from 'antd';
import {
  LogoutOutlined,
  UserOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';

import Logo from 'components/Logo';

import { toggleCollapsedNav, toggleOffCanvasMobileNav } from 'actions/settingsActions';
import { AuthContext } from '../../Auth/AuthContext';
import { decodeUsername, getToken } from '../../../utils/jwtUtil';

const { Header } = Layout;

const AppHeader = props => {
  const { logout } = useContext(AuthContext);

  const token = getToken();
  const userName = decodeUsername(token);
  const onToggleCollapsedNav = () => {
    const { handleToggleCollapsedNav, collapsedNav } = props;
    handleToggleCollapsedNav(!collapsedNav);
  };

  const onToggleOffCanvasMobileNav = () => {
    const { handleToggleOffCanvasMobileNav, offCanvasMobileNav } = props;
    handleToggleOffCanvasMobileNav(!offCanvasMobileNav);
  };

  const { collapsedNav, offCanvasMobileNav, colorOption, showLogo } = props;

  const avatarDropdown = (
    <Menu className="app-header-dropdown">
      <Menu.Item key="1" className="d-block d-md-none">
        {' '}
        Signed in as <strong>{userName}</strong>{' '}
      </Menu.Item>
      <Menu.Divider className="d-block d-md-none" />
      <Menu.Item key="0">
        {' '}
        <Link to="/profile">
          {' '}
          <UserOutlined
            style={{ fontSize: '16px' }}
            className={'site-form-item-icon'}
          /> Profile{' '}
        </Link>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">
        {' '}
        <Button type={'link'} onClick={() => logout()} style={{ color: 'blue', paddingLeft: 0 }}>
          <LogoutOutlined style={{ fontSize: '16px' }} className={'site-form-item-icon'} /> Sign out
        </Button>{' '}
      </Menu.Item>
    </Menu>
  );
  return (
    <Header className="app-header">
      <div
        className={classnames('app-header-inner', {
          'bg-white': ['11', '12', '13', '14', '15', '16', '21'].indexOf(colorOption) >= 0,
          'bg-dark': colorOption === '31',
          'bg-primary': ['22', '32'].indexOf(colorOption) >= 0,
          'bg-success': ['23', '33'].indexOf(colorOption) >= 0,
          'bg-info': ['24', '34'].indexOf(colorOption) >= 0,
          'bg-warning': ['25', '35'].indexOf(colorOption) >= 0,
          'bg-danger': ['26', '36'].indexOf(colorOption) >= 0,
        })}
      >
        <div className="header-left">
          <div className="list-unstyled list-inline">
            {showLogo && [<Logo key="logo" />, <Divider type="vertical" key="line" />]}
            <Button
              type="link"
              className="list-inline-item d-none d-md-inline-block"
              onClick={onToggleCollapsedNav}
            >
              {collapsedNav ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            </Button>
            <Button
              type={'link'}
              className="list-inline-item d-md-none"
              onClick={onToggleOffCanvasMobileNav}
            >
              {offCanvasMobileNav ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            </Button>
          </div>
        </div>

        <div className="header-right">
          <div className="list-unstyled list-inline">
            <Dropdown className="list-inline-item" overlay={avatarDropdown} trigger={['click']}>
              <Button className="ant-dropdown-link no-link-style" type={'link'}>
                <Avatar src="assets/images-demo/avatars/4.jpg" size="small" />
                <span className="avatar-text d-none d-md-inline">{userName}</span>
              </Button>
            </Dropdown>
          </div>
        </div>
      </div>
    </Header>
  );
};

const mapStateToProps = state => ({
  offCanvasMobileNav: state.settings.offCanvasMobileNav,
  collapsedNav: state.settings.collapsedNav,
  colorOption: state.settings.colorOption,
});

const mapDispatchToProps = dispatch => ({
  handleToggleCollapsedNav: isCollapsedNav => {
    dispatch(toggleCollapsedNav(isCollapsedNav));
  },
  handleToggleOffCanvasMobileNav: isOffCanvasMobileNav => {
    dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AppHeader);
