import React from 'react';
import { Layout } from 'antd';
import APPCONFIG from 'constants/appConfig';
import DEMO from 'constants/demoData';
import { EyeOutlined,HomeOutlined } from '@ant-design/icons';

import LogoImage from '../../../assets/Guruji.png';

const { Footer } = Layout;

const AppFooter = () => (
  <Footer className="app-footer app-footer-custom" style={{ marginTop: 'auto' }}>
    <div className="footer-inner-v2">
      {/*<ul className="footer-social-list">*/}
      {/*  <li>*/}
      {/*    <Button type="link">*/}
      {/*      <Icon type="google" />*/}
      {/*    </Button>*/}
      {/*  </li>*/}
      {/*  <li>*/}
      {/*    <Button type={'link'}>*/}
      {/*      <Icon type="facebook" theme="filled" />*/}
      {/*    </Button>*/}
      {/*  </li>*/}
      {/*  <li>*/}
      {/*    <Button type={'link'}>*/}
      {/*      <Icon type="twitter" />*/}
      {/*    </Button>*/}
      {/*  </li>*/}
      {/*  <li>*/}
      {/*    <Button type={'link'}>*/}
      {/*      <Icon type="instagram" />*/}
      {/*    </Button>*/}
      {/*  </li>*/}
      {/*</ul>*/}
      <div className="footer-copyright">
        <img src={LogoImage} alt="Guruji Logo" style={{ height: '45px' }} />
        <span>
          © {APPCONFIG.year} {APPCONFIG.brand}. All Rights Reserved.
        </span>
      </div>
    </div>
  </Footer>
);

export default AppFooter;
