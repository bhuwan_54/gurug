import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu } from 'antd';
import { Icon } from '@ant-design/compatible';

import { toggleOffCanvasMobileNav } from '../../../actions/settingsActions';
import APP_CONFIG, { PERMISSION_KEY } from '../../../constants/appConfig';
import MenuRoute from '../../../constants/menuRoute';
import { urlToList } from '../../../utils/commonUtil';
import { getLocalStorage } from '../../../utils/storageUtil';

const SubMenu = Menu.SubMenu;

class AppMenu extends Component {
  constructor(props) {
    super(props);

    const activeMenuKey = urlToList(props.location.pathname);
    this.state = {
      openKeys: activeMenuKey ? activeMenuKey : ['/dashboard'],
    };
  }

  // without submenu
  rootMenuItemKeys = ['/dashboard'];

  rootSubmenuKeys = [
    '/drivers',
    '/customers',
    '/roles',
    '/settings',
    '/geofencings',
    '/trips',
    '/promoCodes',
    '/settings/commissions',
    '/reviews',
    '/supports',
    '/notifications',
    '/settings/tripFees',
    '/settings'
  ];

  onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  onMenuItemClick = item => {
    const itemKey = item.key;
    if (this.rootMenuItemKeys.indexOf(itemKey) >= 0) {
      this.setState({ openKeys: [itemKey] });
    }

    const { isMobileNav } = this.props;
    if (isMobileNav) {
      this.closeMobileSidenav();
    }
  };

  closeMobileSidenav = () => {
    if (APP_CONFIG.AutoCloseMobileNav) {
      const { handleToggleOffCanvasMobileNav } = this.props;
      handleToggleOffCanvasMobileNav(true);
    }
  };

  getSubMenuOrItem = item => {
    const { location } = this.props;
    const segmentURL = window.location.pathname.split('/');
    const secondSegmentURL = '/' + segmentURL[1];
    const currentPathname = location && location.pathname ? location.pathname : '/';
    const lastSegmentURL = currentPathname.substr(currentPathname.lastIndexOf('/') + 1);

    if (item.children && item.children.some(child => child.name)) {
      const childrenItems = this.getNavMenuItems(item.children);

      // hide submenu if there's no children items
      if (childrenItems && childrenItems.length > 0) {
        return (
          <SubMenu
            title={
              item.iconName ? (
                <span>
                  <Icon type={item.iconName} />
                  <span className="nav-text">{item.menuName || item.name}</span>
                </span>
              ) : (
                item.name
              )
            }
            key={item.path}
            className={
              secondSegmentURL === item.path &&
              (lastSegmentURL === 'edit' || lastSegmentURL === 'detail')
                ? 'ant-menu-item-selected'
                : ''
            }
          >
            {childrenItems}
          </SubMenu>
        );
      }
      return null;
    } else {
      return (
        <Menu.Item
          key={item.path}
          className={
            secondSegmentURL === item.path &&
            (lastSegmentURL === 'edit' || lastSegmentURL === 'detail')
              ? 'ant-menu-item-selected'
              : ''
          }
        >
          <Link to={item.path}>
            <Icon type={item.iconName} />
            <span className="nav-text">{item.menuName || item.name}</span>
          </Link>
        </Menu.Item>
      );
    }
  };

  getNavMenuItems = menusData => {
    if (!menusData) {
      return [];
    }
    return menusData
      .filter(item => !item.hideInMenu)
      .map(item => {
        // make dom
        const ItemDom = this.getSubMenuOrItem(item);
        if (item.rights) {
          return this.checkPermissionItem(item.rights, ItemDom);
        } else {
          return ItemDom;
        }
        if (item) {
          return ItemDom;
        }
      })
      .filter(item => item);
  };

  checkPermissionItem = (rights, target) => {
    const permissions = getLocalStorage(PERMISSION_KEY);
    if (!rights) {
      return target;
    }

    if (Array.isArray(rights)) {
      if (rights.indexOf(permissions) >= 0) {
        return target;
      }
      if (Array.isArray(permissions)) {
        for (let i = 0; i < permissions.length; i += 1) {
          const element = permissions[i];
          if (rights.indexOf(element) >= 0) {
            return target;
          }
        }
      }
      return;
    }
  };

  // Conversion router to menu.
  formatter = (data, parentPath = '', parentAuthority, parentName) => {
    return data.map(item => {
      let locale = 'menu';
      if (parentName && item.name) {
        locale = `${parentName}.${item.name}`;
      } else if (item.name) {
        locale = `menu.${item.name}`;
      } else if (parentName) {
        locale = parentName;
      }
      const result = {
        ...item,
        locale,
        authority: item.authority || parentAuthority,
      };
      if (item.routes) {
        const children = this.formatter(
          item.routes,
          `${parentPath}${item.path}/`,
          item.authority,
          locale
        );
        // Reduce memory usage
        result.children = children;
      }
      delete result.routes;
      return result;
    });
  };

  getMenuData() {
    return this.formatter(MenuRoute);
  }

  render() {
    const { collapsedNav, colorOption, location } = this.props;
    // const mode = collapsedNav ? 'vertical' : 'inline';
    const menuTheme =
      ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0 ? 'light' : 'dark';
    const currentPathname = location && location.pathname ? location.pathname : '/';

    const menuProps = collapsedNav
      ? {}
      : {
          openKeys: this.state.openKeys,
        };

    return (
      <Menu
        theme={menuTheme}
        mode="inline"
        inlineCollapsed={collapsedNav}
        {...menuProps}
        onOpenChange={this.onOpenChange}
        onClick={this.onMenuItemClick}
        selectedKeys={[currentPathname]}
      >
        {this.getNavMenuItems(this.getMenuData())}
      </Menu>
    );
  }
}

const mapStateToProps = state => {
  return {
    collapsedNav: state.settings.collapsedNav,
    colorOption: state.settings.colorOption,
    location: state.router.location,
  };
};

const mapDispatchToProps = dispatch => ({
  handleToggleOffCanvasMobileNav: isOffCanvasMobileNav => {
    dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AppMenu);
