
import React, { Component , Fragment} from 'react';
import {Result, Button} from 'antd';

class ErrorHandler extends Component {
  constructor(props) {
    super(props)
    this.state = { errorOccurred: false }
  }

  componentDidCatch(error, info) {
    this.setState({ errorOccurred: true })
    this.setState({ errorInfo: error })
    //logErrorToMyService(error, info)
  }

  render() {
    return this.state.errorOccurred ? <Result
      status="500"
      title="Sorry, something went wrong."
      extra={  <p
        className="ant-btn ant-btn-round ant-btn-lg no-underline"
      >Please refresh the page.</p>}
    /> : this.props.children
  }
}

export default ErrorHandler;
