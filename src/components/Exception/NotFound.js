import React from 'react';
import { Button, Result } from 'antd';

const NotFound = () => {
  return (
    <div className="page-err">
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={<Button href="/#/dashboard">Go Back to Dashboard Page</Button>}
      />
    </div>
  );
};

export default NotFound;
