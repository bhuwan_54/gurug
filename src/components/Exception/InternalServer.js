import React from 'react';
import { Button, Result } from 'antd';

const InternalServer = () => {
  return (
    <Result
      status="500"
      title="500"
      subTitle="Sorry, the server is wrong."
      extra={<Button href="/#/dashboard">Go Back to Dashboard Page</Button>}
    />
  );
};

export default InternalServer;
