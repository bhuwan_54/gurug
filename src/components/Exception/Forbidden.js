import React from 'react';
import { Button, Result } from 'antd';

const Forbidden = () => {
  return (
    <div className="page-err">
      <Result
        status="403"
        title="403"
        subTitle="Sorry, you are not authorized to access this page."
        extra={<Button href="/#/dashboard">Go Back to Dashboard Page</Button>}
      />
    </div>
  );
};

export default Forbidden;
