import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form, Button, Tag } from 'antd';
import { getSortingOrder, searchItemLayout } from '../../utils/commonUtil';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import { PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';
import Message from '../Common/Message';
import moment from 'moment';

const Search = Input.Search;

export const statusColor = {
  TRIP_REQUESTED: '#1890ff',
  TRIP_COMPLETED: '#66BB6A',
  OPEN: '#ffc53d',
};

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [form] = Form.useForm();
  const { validateFields } = form;

  const {
    fetchTotalTripListWithCriteria,
    errors,
    pagination,
    loading,
    totalTrips,
    cleanLivePreviewProps,
  } = props;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchTotalTripListWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = PAGE_NUMBER ;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchTotalTripListWithCriteria(fieldsValue);
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      align:'center',
      render: (text, record, index) => {
         return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        //  return (page.pageNumber - 1) * page.pageSize + index + 1;
      },
    },
    {
      title: 'Trip Code',
      dataIndex: 'tirpCode',
      align: 'left',
      render: (text, record) => {
        return <span style={{fontSize:'10px'}}>{`${record.tripCode}`}</span>;
      },
    },
    {
      title: 'Trip Start Location',
      dataIndex: 'tripStartLocation',
      align: 'left',
    },
    {
      title: 'Trip End Location',
      dataIndex: 'tripEndLocation',
      align: 'left',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      align: 'left',
      render: status => (
        <span>
            <Tag color={statusColor[`${status}`]}>
              {status}
            </Tag>
      </span>
      ),
    },
    {
      title: 'Created At',
      dataIndex: 'createdAt',
      align: 'left',
      render: (text,render) => {
        return render.createdAt ? moment(render.createdAt).format('DD MMM YYYY HH:mm:ss') : ''
      }
    },
    {
      title: 'Action',
      key: 'operation',
      align:'center',
      render: (text, record) => {
        return (
          <Link to={`/trips/${record.tripCode}/detail`} title="View">
            <Button shape="circle"  icon={<EyeOutlined  style={{ fontSize: '16px' }}/>}  style={{margin:'5px'}}/>
          </Link>
        );
      },
    },
  ];

  useEffect(() => {
    fetchTotalTripListWithCriteria({ pageSize: PAGE_SIZE, pageNumber: PAGE_NUMBER });
    return () => {
      //cleanLivePreviewProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Total Trip List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Total Trip </Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>

                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size={ 'middle'}
                      bordered
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={totalTrips instanceof Array ? totalTrips : []}
                      pagination={{
                        total: pagination.totalData,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
