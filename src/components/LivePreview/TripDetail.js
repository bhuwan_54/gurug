import React, { useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Breadcrumb, Card, Skeleton } from 'antd';
import { HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import moment from 'moment';

const FormItem = Form.Item;
const Detail = props => {
  const { id } = useParams();
  const { tripDetails,errors, loading, fetchTripDetailByIdentifier, cleanTripDetialProps } = props;

  const formItemLayout = {
    labelCol: {
      xl: { span: 8 },
      lg: { span: 8 },
      md: { span: 12 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 6 },
      lg: { span: 24, offset: 6 },
      md: { span: 12, offset: 8 },
      sm: { span: 12, offset: 6 },
      xs: { span: 24, offset: 0 },
    },
  };

  useEffect(() => {
    fetchTripDetailByIdentifier(id);
    return () => {
      cleanTripDetialProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Trip View'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined className={'mr-1'} />
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/trips'}>{'Trip List'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'Detail'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />
          <Card className="detail-view">
            <Skeleton loading={loading} active>
              <Col span={24}>
                <Form {...formItemLayout}>
                  <Row>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Trip Code">{ tripDetails?.tripCode}</FormItem>
                    </Col>

                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Ride Type">
                        { tripDetails?.rideType}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Driver Name">{ tripDetails?.driver}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Customer Name">{ tripDetails?.customer}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Trip Start Location">{  tripDetails?.tripStartLocation}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Trip End Location">{  tripDetails?.tripEndLocation}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Trip Fee">
                        { tripDetails?.tripFee}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Status">
                        { tripDetails?.status}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Trip Start Time">{ tripDetails && tripDetails.tripStartTs &&  moment(tripDetails.tripStartTs).format('YYYY-MM-DD HH:mm:ss A')}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Trip End Time">{  tripDetails && tripDetails.tripEndTs &&  moment(tripDetails.tripEndTs).format('YYYY-MM-DD HH:mm:ss A')}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Commission">
                        { tripDetails?.commission}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Commission Type">
                        { tripDetails?.commissionType}
                      </FormItem>
                    </Col>
                  </Row>

                  <Col xl={16} lg={24} md={24} sm={24}>
                    <FormItem {...submitFormLayout} className="mt-2 ">

                      <Link
                        to="/trips"
                        className="ant-btn ant-btn-round ant-btn-lg no-underline"
                      >
                        {'Back'}
                      </Link>
                    </FormItem>
                  </Col>
                </Form>
              </Col>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default Detail;
