import React, { Fragment, useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Breadcrumb, Card, Skeleton, Button, Form, DatePicker, Spin, Descriptions } from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import {
  GoogleMap,
  useLoadScript,
  Circle,
  Polygon,
  Marker,
  InfoWindow,
  Polyline,
} from '@react-google-maps/api';

import Message from '../Common/Message';
import { isEmpty } from '../../utils/commonUtil';
import firebase from '../../firebase';
import moment from 'moment';
import { DATE_FORMAT_VIEW } from '../../constants/appConfig';

const FormItem = Form.Item;

const Detail = props => {
  const livePreviewId = props.match.params.id;
  const [driverLocation, setDriverLocation] = useState([]);
  const [visible, setVisible] = useState(false);
  const [activeMarker, setActiveMarker] = useState();

  const {
    // driverLocations,
    errors,
    fetchlivePreviewByIdentifier,
    fetchlivePreviewLocationWithCriteria,
    cleanlivePreviewProps,
  } = props;

  const driverLocations = {
    id: 'f554417c027346e6bf1b7f61d977dbf3',
    serialNumber: 'testSerial',
    modelName: 'testModel',
    imeiNumber: 'testImei',
    description: 'test',
    status: 'inUse',
    active: true,
    locked: true,
    geofenceType: 'circle',
    circle: {
      center: {
        lat: 27.702477,
        lng: 85.31531,
      },
      //radius for the circular fencing
      // "radius": 20000
      locations: [{ lat: 27.7066, lng: 85.3145 }],
    },
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 6 },
      md: { span: 8 },
      sm: { span: 6 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 8 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };

  const toggleInfoWindow = (event, index) => {
    setVisible(!visible);
    setActiveMarker(index);
  };

  useEffect(() => {
    const locationRef = firebase.database().ref('online_drivers');
    locationRef.on('value', snapshot => {
      const trackLocation = snapshot.val();

      console.log(trackLocation, 'trackLocation in firebase');

      const locationValues = [];

      trackLocation && Object.keys(trackLocation).map(key => {
        if (key) {
          locationValues.push({ lat: trackLocation[key].lat, lng: trackLocation[key].lng });
        }
      });

      setDriverLocation(locationValues);
    });
    return () => {
    };
  }, []);

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: `${process.env.REACT_APP_GOOGLE_API_KEY}`,
    preventGoogleFontsLoading: false,
  });
  const image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

  var icon = { // car icon
    path: 'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805',
    scale: 0.4,
    fillColor: '#f4340d', //<-- Car Color, you can change it
    fillOpacity: 1,
    strokeWeight: 1,
    size: 54,
    // anchor: new google.maps.Point(0, 5),
    // rotation: data.val().angle //<-- Car angle
  };

  const regionOptions = { fillOpacity: 0.1, strokeWidth: 1, strokeOpacity: 0.2 };
  const circleCenter = driverLocations && driverLocations.circle && driverLocations.circle.center;
  const circleRadius = driverLocations && driverLocations.circle && driverLocations.circle.radius;

  const polygonPaths = driverLocations && driverLocations.polygon && driverLocations.polygon.path;

  const locationHistory =
    driverLocations && driverLocations.locations ? driverLocations.locations : [];

  const uniqueLocationHistory = locationHistory
    .map(e => e.pos['lat'])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => locationHistory[e])
    .map(e => locationHistory[e]);

  const locationPositions = locationHistory.map(points => {
    return { lat: points.pos.lat, lng: points.pos.lng };
  });

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col span={12}>
                <h4 className="article-title">{'Live Preview of Driver'}</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined/>
                    <Link to={'/dashboard'}>{'Dashboard'}</Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    {' '}
                    <Link to={'/lives'}>{'Live Driver'}</Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>{'Live Detail'}</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Card className={'geofencing-card'}>
              <Col>
                <Descriptions layout="horizontal">
                  <Descriptions.Item label={'Driver Name'}>
                    {' '}
                    <strong>
                      {'Radhee Kumar Gupa '}
                      {''}
                    </strong>
                  </Descriptions.Item>
                  <Descriptions.Item label={'Customer'}>
                    <strong> {'Radha Kumari Jha'}</strong>
                  </Descriptions.Item>
                </Descriptions>
                <Descriptions>
                  <Descriptions.Item label={'From'}>
                    <strong> {'Kalankk'}</strong>
                  </Descriptions.Item>
                  <Descriptions.Item label={'To'}>
                    <strong> {'Ratna Park'}</strong>
                  </Descriptions.Item>
                </Descriptions>
              </Col>


              <Spin spinning={false} size="large" delay={300} tip={'loading...'}>
                <Skeleton loading={false} active>
                  {isLoaded && !isEmpty(driverLocations) && (
                    <GoogleMap
                      mapContainerStyle={{
                        height: '70vh',
                        width: '100%',
                      }}
                      id="geofencing-map"
                      //center={driverLocations.defaultCenter}
                      center={{ lat: 27.702477, lng: 85.31531 }}
                      zoom={13}
                    >
                      {/*<Polyline*/}
                      {/*  path={locationPositions}*/}
                      {/*  options={{*/}
                      {/*    strokeColor: '#72adf4',*/}
                      {/*    strokeOpacity: 1.0,*/}
                      {/*    strokeWeight: 7,*/}
                      {/*    visible: true,*/}
                      {/*  }}*/}
                      {/*/>*/}

                      {driverLocation &&
                      driverLocation.map((location, i) => (

                        <Marker
                          // key={'location'}
                          animation={true}
                          key={i}
                          // label={{
                          //   text: (i === 0) ? 'A' : (i === uniqueWayPoints.length - 1) ? 'B' : {},
                          //   color: 'white',
                          // }}
                          icon={icon}
                          position={location}
                          // position={driverLocation && driverLocation[0] }
                          // onClick={event => {
                          //  toggleInfoWindow(event, i);
                          // }}
                        >
                        </Marker>
                      ))}
                      <Fragment>
                        <Circle
                          center={circleCenter}
                          radius={circleRadius}
                          options={regionOptions}
                        />
                      </Fragment>
                    </GoogleMap>
                  )}

                  <div style={{ marginTop: '20px' }}>
                    <FormItem>
                      <Link to="/lives" className="ant-btn no-underline" tabIndex={5}>
                        {'Cancel'}
                      </Link>
                    </FormItem>
                  </div>
                </Skeleton>
              </Spin>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default Detail;
