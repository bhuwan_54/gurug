import React, { useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Breadcrumb, Card, Skeleton, Select, Button } from 'antd';
import { HomeOutlined } from '@ant-design/icons';
import moment from 'moment';

import Message from '../Common/Message';
import { READ_ABLE_DATE_FORMAT } from '../../constants/appConfig';

const FormItem = Form.Item;
const { Option} = Select;

const Detail = props => {
  const { id } = useParams();
  const { errors,supportTickets, employee, loading, fetchSupportTicketByIdentifier,closeSupportTicket, cleanSupportTicketProps,assignSupportTicket, fetchEmployeeDropdown } = props;
  const [form] = Form.useForm();
  const { validateFields } = form;

  const formItemLayout = {
    labelCol: {
      xl: { span: 8 },
      lg: { span: 8 },
      md: { span: 10 },
      sm: { span: 10 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 12 },
      sm: { span: 14 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 6 },
      lg: { span: 24, offset: 6 },
      md: { span: 12, offset: 8 },
      sm: { span: 12, offset: 6 },
      xs: { span: 24, offset: 0 },
    },
  };
  const onFinish = ( values)=> {
    values.ticketId = supportTickets.ticketId;
      assignSupportTicket(values);
  };

  const handleCloseSupportTicket = () => {
    let values = {};
    values.ticketId = supportTickets.ticketId
    closeSupportTicket(values)
  };

  useEffect(() => {
    fetchSupportTicketByIdentifier(id);
    fetchEmployeeDropdown();
    return () => {
      cleanSupportTicketProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Support Ticket View'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined className={'mr-1'} />
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/supports'}>{'Support Tickets'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'View'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />
          <Card className="detail-view">
            <Skeleton loading={loading} active>
              <Col span={24}>
                <Form {...formItemLayout} onFinish={onFinish}>
                  <Row>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Ticket ID">{supportTickets ? supportTickets.ticketId : ''}</FormItem>
                    </Col>

                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Ticket Type">
                        {supportTickets ? supportTickets.ticketType : ''}
                      </FormItem>
                    </Col>

                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="User ID">{supportTickets ? supportTickets.userId : ''}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="User Type">{supportTickets ? supportTickets.userType : ''}</FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem label="Status">{supportTickets ? supportTickets.status : ''}</FormItem>
                    </Col>
                    {
                      supportTickets.status !== 'OPEN' &&
                      <>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem label="Assigned By">
                            {supportTickets ? supportTickets.assignedBy : ''}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem label="Assigned By ID ">
                            {supportTickets ? supportTickets.assignedById : ''}
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={24} md={24} sm={24}>
                          <FormItem label={'Assign To'}>
                            {supportTickets ? supportTickets.assignedTo : ''}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={24} md={24} sm={24}>
                          <FormItem label={'Assign To Id'}>
                            {supportTickets ? supportTickets.assignedToId : ''}
                          </FormItem>
                        </Col>
                      </>
                    }

                    <Col xl={12} lg={24} md={24} sm={24}>
                      <FormItem label="Active">{supportTickets.active ? 'True' : 'False'}</FormItem>
                    </Col>
                    <Col xl={12} lg={24} md={24} sm={24}>
                      <FormItem label="Created On">{moment(supportTickets?.createdOn).format(READ_ABLE_DATE_FORMAT)} </FormItem>
                    </Col>
                    <Col xl={12} lg={24} md={24} sm={24}>
                      <FormItem label="Modified On">{moment(supportTickets?.modifiedOn).format(READ_ABLE_DATE_FORMAT)} </FormItem>
                    </Col>
                    {
                      supportTickets.status === 'OPEN' &&
                      <Col xl={12} lg={24} md={24} sm={24}>
                        <FormItem label="Assign To" name={'assignedToId'}>
                          <Select
                            placeholder="Select"
                          >
                            {employee instanceof Array &&
                            employee.map(d => <Option key={d.id}>{d.value}</Option>)}
                          </Select>

                        </FormItem>
                      </Col>
                    }
                  </Row>

                  <Col xl={16} lg={24} md={24} sm={24}>
                    <FormItem {...submitFormLayout} className="mt-2 ">
                      {supportTickets.status === 'OPEN' &&
                      <Button
                        className="btn-custom-primary mr-1"
                        shape="round"
                        htmlType="submit"
                        size={'large'}
                      > Assign
                      </Button>
                      }
                      {supportTickets.status === 'ASSIGNED' &&
                      <Button
                        className="btn-custom-primary mr-1"
                        shape="round"
                        size={'large'}
                        onClick={() => closeSupportTicket({ticketId: supportTickets.ticketId})}
                      > Close
                      </Button>
                      }
                      <Link
                        to="/supports"
                        className="ant-btn ant-btn-round ant-btn-lg no-underline"
                      >
                        {'Back'}
                      </Link>
                    </FormItem>
                  </Col>
                </Form>
              </Col>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default Detail;
