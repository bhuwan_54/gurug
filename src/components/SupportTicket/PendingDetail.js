import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Button, Breadcrumb, Card, Descriptions, Skeleton } from 'antd';
import dayjs from 'dayjs';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import ApproveModel from '../Common/Modal/ApproveModal';
import { DATE_FORMAT } from '../../constants/appConfig';
import Message from '../Common/Message';

const PendingDetail = props => {
  const { id } = useParams();

  const {
    customers,
    errors,
    loading,
    fetchPendingDetailByIdentifier,
    approveRequest,
    cleanCustomerProps,
  } = props;

  const approveModalProps = {
    title: 'Arrpove Request?',
    width: '380px',
    id: customers.id,
    approveEntity: approveRequest,
  };

  const newValues = customers ? customers.newData : '';
  const oldValues = customers ? customers.oldData : '';

  const oldActive = oldValues ? oldValues.active : null;
  const newActive = newValues ? newValues.active : null;

  useEffect(() => {
    fetchPendingDetailByIdentifier(id);
    return () => {
      cleanCustomerProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Pending Detail'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/customers'}>Customers</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/customers/pending'}>Pending</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'View'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />

          <Card>
            <Skeleton loading={loading} active>
              <Col lg={18} md={24} sm={24}>
                <Descriptions size="default">
                  <Descriptions.Item label={'Request'}>{customers.action}</Descriptions.Item>
                </Descriptions>
              </Col>
              <Col lg={18} md={24} sm={24}>
                <Descriptions size="default">
                  <Descriptions.Item label={'Request By'}>
                    {customers.requestedBy}
                  </Descriptions.Item>
                </Descriptions>
              </Col>
              <Col lg={18} md={24} sm={24}>
                <Descriptions size="default">
                  <Descriptions.Item label={'Requested On'}>
                    {customers.requestedOn ? dayjs(customers.requestedOn).format(DATE_FORMAT) : ''}
                  </Descriptions.Item>
                </Descriptions>
              </Col>
              <div className="detail-table table-responsive">
                <table className="table table-bordered" style={{ backgroundColor: 'white' }}>
                  <thead className="ant-table-thead">
                    <tr>
                      <th scope="col">{'Fields'}</th>
                      {oldValues && <th scope="col">Old Values</th>}
                      {customers.oldData && customers.newData ? (
                        <th scope="col">{'New Values'}</th>
                      ) : (
                        <th scope="col">{'Values'}</th>
                      )}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Fullname</td>
                      {oldValues && <td>{oldValues ? oldValues.name : ''}</td>}
                      <td>{newValues ? newValues.name : ''}</td>
                    </tr>
                    <tr>
                      <td>Username</td>
                      {oldValues && <td>{oldValues ? oldValues.shortName : ''}</td>}
                      <td>{newValues ? newValues.shortName : ''}</td>
                    </tr>

                    <tr>
                      <td>Code</td>
                      {oldValues && <td>{oldValues ? oldValues.code : ''}</td>}
                      <td>{newValues ? newValues.code : ''}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      {oldValues && <td>{oldValues ? oldValues.email : ''}</td>}
                      <td>{newValues ? newValues.email : ''}</td>
                    </tr>
                    <tr>
                      <td>Address</td>
                      {oldValues && <td>{oldValues ? oldValues.address : ''}</td>}
                      <td>{newValues ? newValues.address : ''}</td>
                    </tr>
                    <tr>
                      <td>{'Status'}</td>
                      {oldValues && <td>{oldValues ? oldValues.active : ''}</td>}
                      <td>{newValues ? newValues.active : ''}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <Button
                type="primary"
                className="btn-custom-field m-1"
                onClick={() => ApproveModel(approveModalProps)}
              >
                {'Approve'}
              </Button>
              )}
              <Link to="/customers/pending" className="ant-btn no-underline m-1">
                {'Cancel'}
              </Link>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default PendingDetail;
