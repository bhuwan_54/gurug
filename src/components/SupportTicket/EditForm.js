import React, { useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Input, Button, Breadcrumb, Spin, DatePicker, Affix } from 'antd';

import moment from 'moment';
import Message from '../Common/Message';
import { DATE_FORMAT_VIEW, DATE_SERVER_FORMAT } from '../../constants/appConfig';
import { HomeOutlined } from '@ant-design/icons';
import history from '../../utils/history';
import ApproveModel from '../Common/Modal/ApproveModal';
import ImageUploadManually from '../Common/ImageUpload/ImageUploadManually';

const FormItem = Form.Item;
const customers = {
  id: 1,
  fullName: 'Sam Shrestha',
  mobileNumber: '9860088222',
  emailId: 'sam@gmail.com',
  homeAddress: 'Home',
  workAddress: 'Work',
  dob: '2020-02-19',
  deviceUUID: '123456',
  deviceMAC: '123456',
  photo: 'www.imageurl.com',
  status: 'REGISTERED',
  active: true,
};
const EditForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();
  const {
    // customers,
    loading,
    errors,
    fetchCustomerByIdentifier,
    updateCustomer,
    updateCustomerStatus,
    cleanCustomerProps,
  } = props;

  const  {getFieldValue} = form;
  const onFinish = values => {
    Object.keys(values).map(key => {
      if (values && moment.isMoment(values[key])) {
        values[key] = moment(values[key], DATE_SERVER_FORMAT)
          .startOf('days')
          .format(DATE_SERVER_FORMAT);
      }
    });
    updateCustomer(values);
  };
  const onFinishFailed = errorInfo => {
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 4 },
      sm: { span: 12, offset: 4 },
      xs: { span: 24, offset: 0 },
    },
  };

  const handleInitialValue = values => {
    if (values) {
      Object.keys(values).map(key => {
        if (key === 'dob') {
          values[key] = values[key] && moment(values[key], DATE_FORMAT_VIEW);
        }
      });
      return values;
    }
  };

  const handleBack = () => {
    history.push('/customers');
  };

  const handleCancel = () => {
    form.resetFields();
  };
  const modalProps={
    title: customers.active ? 'Are you sure want to disable ?':'Are you sure want to activate ?',
    apiUrlKey: customers.active ? 'disable':'activate',
    width: '400px',
    id:id,
    approveEntity: updateCustomerStatus
  };

  const verifyModalProps={
    title: 'Verify the customer record ?',
    apiUrlKey: 'verify',
    width: '400px',
    id:id,
    approveEntity: updateCustomerStatus
  };

  const customerPhotoPreview = getFieldValue('photoPerson');

  const customerPhotoSource = customers
    ? 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    : '';

  useEffect(() => {
    // form.setFieldsValue({
    //   id: id,
    // });
    fetchCustomerByIdentifier(id);
  }, []);

  return (
    <Spin spinning={loading} >
      <div className="container-fluid no-breadcrumb page-dashboard">
        <QueueAnim type="bottom" className="ui-animate">
          <article className="article" id="components-form-demo-advanced-search">
            <Row type="flex" justify="space-between">
              <Col xl={16} lg={16} md={24} xs={24} sm={24}>
                <h4 className="article-title">Update Customer</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined />
                    Dashboard
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    {' '}
                    <Link to={'/customers'}>Customers </Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Update</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />
            <div className="box box-default">
              <Spin spinning={loading} size={'large'} delay={300} tip={'loading...'}>
                <div className="box-body p-5">
                  <section className="form-v1-container">
                    <Form
                      form={form}
                      {...formItemLayout}
                      name="updateForm"
                      initialValues={customers && handleInitialValue(customers)}
                      onFinish={onFinish}
                      onFinishFailed={onFinishFailed}
                      className="gurug-form"
                      layout="horizontal"
                    >
                      <Row gutter={18}>
                        <Row justify="end" gutter={[8,16]}>
                          <Col xl={5} lg={5} md={12} sm={18} xs={18}>
                            <Affix offsetTop={10}>
                              <Button type="dashed" shape="round" block onClick={() => ApproveModel(modalProps)}>
                                { customers && customers.active ? 'Disable':'Activate' }
                              </Button>
                            </Affix>
                          </Col>
                          {
                            !customers.verified &&  <Col xl={5} lg={5} md={12} sm={18} xs={18}>
                              <Affix offsetTop={10}>
                                <Button type="dashed" shape="round" block onClick={() => ApproveModel(verifyModalProps)}>
                                  Verify
                                </Button>
                              </Affix>
                            </Col>
                          }
                        </Row>

                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'fullName'}
                              label="Full Name"
                              rules={[{ required: true, message: 'Full name is required.' }]}
                            >
                              <Input />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'emailId'}
                              label={'Email'}
                              rules={[
                                { required: true, message: 'Email is required' },
                                {
                                  type: 'email',
                                  message: 'Please enter valid email address.',
                                },
                              ]}
                            >
                              <Input />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'mobileNumber'}
                              rules={[{ required: true, message: 'Mobile number is required.' }]}
                              label="Mobile Number"
                            >
                              <Input />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem name={'homeAddress'} label="Home Address">
                              <Input />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem name={'workAddress'} label="Work Address">
                              <Input />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              name={'dob'}
                              label="Date of Birth"
                              rules={[{ required: true, message: 'Date of birth is required.' }]}
                            >
                              <DatePicker format={DATE_FORMAT_VIEW} />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem name={'deviceUUID'} label="Device UUID">
                              <Input />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem name={'deviceMAC'} label="Device Mac Address">
                              <Input />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem name={'active'} label="Active">
                              <Input />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              name={'personPhoto'}
                              label="Person Photo"
                              rules={[
                                {
                                  required: true,
                                  message: 'Person photo is required',
                                },
                              ]}
                            >
                              <ImageUploadManually
                                {...props}
                                fileName="personPhoto"
                                acceptType="image/jpeg,image/png"
                                placeholder="Person photo"
                                fileType="picture"
                                sizeOfFile="512000"
                                form={form}
                              />
                              {customerPhotoPreview !== undefined && (
                                <div>
                                  <img
                                    src={`${customerPhotoSource}?${Date.now()}`}
                                    className="img-thumbnail m-1"
                                    alt="Vechile photo"
                                  />
                                </div>
                              )}
                            </FormItem>
                          </Col>

                          <Col xl={16} lg={24} md={24} sm={24}>
                            <Affix offsetBottom={30}>
                              <FormItem {...submitFormLayout}>
                                <Button
                                  className="btn-custom-primary mr-1"
                                  shape="round"
                                  loading={loading}
                                  htmlType="submit"
                                  size={'large'}
                                >
                                  Update
                                </Button>

                                <Button
                                  className=" mr-1"
                                  shape="round"
                                  danger
                                  onClick={handleCancel}
                                  size={'large'}
                                >
                                  Cancel
                                </Button>
                                <Button
                                  className=" mr-1"
                                  shape="round"
                                  onClick={handleBack}
                                  size={'large'}
                                >
                                  Back
                                </Button>
                              </FormItem>
                            </Affix>
                          </Col>
                        </Row>
                      </Row>
                    </Form>
                  </section>
                </div>
              </Spin>
            </div>
          </article>
        </QueueAnim>
      </div>
    </Spin>
  );
};

export default EditForm;
