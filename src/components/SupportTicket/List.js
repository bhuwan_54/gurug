import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Card, Col, Form, Input, Row, Table, Tag } from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import { PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';

import Message from '../Common/Message';
import { getSortingOrder, searchItemLayout } from '../../utils/commonUtil';
import moment from 'moment';

const Search = Input.Search;

export const statusColor = {
  ASSIGNED: '#1890ff',
  CLOSED: '#66BB6A',
  OPEN: '#ffc53d',
};

const List = props => {
  const [form] = Form.useForm();

  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const { validateFields } = form;

  const {
    fetchSupportTicketWithCriteria,
    supportTickets,
    errors,
    pagination,
    loading,
    cleanSupportTicketProps,
  } = props;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchSupportTicketWithCriteria({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = page.pageNumber || 1;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchSupportTicketWithCriteria(fieldsValue);
    });
  };
  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      align:'center',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Ticket Type',
      dataIndex: 'ticketType',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.ticketType}`}</div>;
      },
    },
    {
      title: 'User Type',
      dataIndex: 'userType',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Ticket Id',
      dataIndex: 'ticketId',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      align: 'left',
      sorter: true,
      render: status => (
        <span>
            <Tag color={statusColor[`${status}`]}>
              {status}
            </Tag>
      </span>
      ),
    },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{moment(record.createdOn).format('DD MMM YYYY HH:mm:ss')}</div>;
      },
    },
    {
      title: 'Modified On',
      dataIndex: 'modifiedOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{moment(record.modifiedOn).format('DD MMM YYYY HH:mm:ss')}</div>;
      },
    },
    {
      title: 'Action',
      key: 'operation',
      align:'center',
      render: (text, record) => {
        return (
          <Link to={`/supports/${record.id}/detail`} title="View">
            <Button shape="circle"  icon={<EyeOutlined  style={{ fontSize: '16px' }}/>}  style={{margin:'5px'}}/>
          </Link>
        );
      },
    },
  ];

  useEffect(() => {
    fetchSupportTicketWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });
    return () => {
      cleanSupportTicketProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title"> Support Ticket</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Support Ticket</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <Row>
                <Col {...searchItemLayout} className="search-form mb-2">
                  <Form name={'search-form'} form={form}>
                    <Form.Item
                      name="searchParameter"
                    >
                      <Search
                        placeholder="Please enter value"
                        enterButton="Search"
                        onSearch={onFinish}
                      />
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
              <div className="box box-default box-ant-table-v1">
                <div className="table-responsive">
                  <Table
                    bordered
                    size={'small'}
                    columns={columns}
                    rowKey={record => record.id}
                    dataSource={supportTickets instanceof Array ? supportTickets : []}
                    pagination={{
                      total: pagination.totalData,
                      showSizeChanger: true,
                      current: pagination.current,
                    }}
                    loading={false}
                    onChange={handleTableChange}
                    scroll={{ x: 720 }}
                  />
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
