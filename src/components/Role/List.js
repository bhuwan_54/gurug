import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form, Checkbox} from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import moment from 'moment';

import { PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';
import Message from '../Common/Message';
import { searchItemLayout, getSortingOrder } from '../../utils/commonUtil';

const Search = Input.Search;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [form] = Form.useForm();

  const { roles, fetchRoleWithCriteria, errors, pagination, loading, cleanRoleProps } = props;
  const { validateFields, getFieldValue, getFieldDecorator } = form;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchRoleWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = page.pageNumber || 1;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchRoleWithCriteria(fieldsValue);
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      align:'center',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Role Name',
      dataIndex: 'name',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.name}`}</div>;
      },
    },
    {
      title: 'Active',
      align: 'center',
      render: (text, record) => {
        return <div> <Checkbox
          checked={record.active} /></div>;
      },
    },
    {
      title: 'Created On',
      align: 'left',
      sorter: true,
      render: record => {
        return (
          <div>
            {record.createdOn &&  moment(record.createdOn).format('DD MMM YYYY HH:mm:ss')
            }
          </div>
        );
      },
    },
    {
      title: 'Modified On',
      align: 'left',
      sorter: true,
      render: record => {
        return (
          <div>
            {' '}
            { record.modifiedOn && moment(record.modifiedOn).format('DD MMM YYYY HH:mm:ss')
            }
          </div>
        );
      },
    },
    {
      title: 'Action',
      key: 'operation',
      align:'center',
      render: (text, record) => {
        return (
          <Link to={`/roles/${record.id}/detail`} title="View">
            <EyeOutlined style={{ fontSize: '22px', color: '#08c' }} />
          </Link>
        );
      },
    },
  ];

  useEffect(() => {
    fetchRoleWithCriteria({ pageSize: PAGE_SIZE, pageNumber:1 });
    return () => {
      cleanRoleProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Role List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Roles</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="search-form mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>

                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size="middle"
                      bordered
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={roles instanceof Array ? roles : []}
                      pagination={{
                        total: pagination.total,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
