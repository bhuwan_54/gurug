import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Breadcrumb, Card, Skeleton, Divider, Typography, Affix, Checkbox } from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import { ENTITY_VALUE, ROLE_ENTITY } from '../../constants/appConfig';

const FormItem = Form.Item;
const { Title } = Typography;


const Detail = props => {
  const { id } = useParams();

  const { roles, errors, loading, fetchRoleByIdentifier, cleanRoleProps } = props;

  const permissions = roles && roles.permissions;
  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 8 },
      sm: { span: 12, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  useEffect(() => {
    fetchRoleByIdentifier(id);
    return () => {
      cleanRoleProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Role View'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/roles'}>{'roles'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'View'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />
          <Card className="detail-view">
            <Skeleton loading={loading} active>
              <Form>
                <Row>
                  <Row justify={'gutter'}>
                    <Col xl={16} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Name">
                        {roles?.roleName}
                      </FormItem>
                    </Col>

                    <Col xl={16} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label={'Active'}>
                        {roles && roles.active ? 'Yes' : 'No'}
                      </FormItem>
                    </Col>

                    <Col xl={16} lg={12} md={18} sm={24}>
                      <div className={'permission-detail'}>
                      <FormItem {...formItemLayout} label={'Permission'}>
                        {permissions && permissions.map((obj) => <Col span={6}>
                            <Checkbox key={obj.id} checked> {ROLE_ENTITY[`${obj.name}`]}</Checkbox>
                          </Col>

                        )}
                      </FormItem>
                      </div>
                    </Col>
                  </Row>

                  <Col xl={16} lg={24} md={24} sm={24}>
                    <Affix offsetBottom={30}>
                      <FormItem {...submitFormLayout} className="mt-3 ">
                        <Link
                          className="ant-btn btn-custom-primary ant-btn-round ant-btn-lg no-underline  mr-2"
                          to={`/roles/${id}/edit`}
                          style={{ color: 'white' }}
                          shape="round"
                        >
                          {/*<Icon type="edit" /> */}
                          {'Edit'}
                        </Link>

                        <Link to="/roles" className="ant-btn ant-btn-round ant-btn-lg no-underline">
                          {'Cancel'}
                        </Link>
                      </FormItem>
                    </Affix>
                  </Col>
                </Row>
              </Form>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default Detail;
