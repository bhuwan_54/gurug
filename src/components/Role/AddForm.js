import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Button,
  Checkbox,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  Skeleton,
  Typography, Input,
} from 'antd';
import { HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import { ROLE_ENTITY,  ENTITY_VALUE } from '../../constants/appConfig';

const FormItem = Form.Item;
const { Title } = Typography;
const { Option } = Select;

const AddForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const {roles, loading, errors,permissions, userTypes, fetchRoleByIdentifier, fetchPermission,fetchUserType, addRole, cleanRoleProps } = props;
  const [active, setActive] = useState(false);
  const [defaultPermission, setDefaultPermission] = useState();

  const getPermissionValue = (id) => {
    let valuesEdited = [];

    id && id.map(value => {
      valuesEdited.push(
        { value: value.value, label: ROLE_ENTITY[value.label] },
      );

    });
    return valuesEdited;
  };

  const onFinish = values => {
    values.permissions = defaultPermission.map(({ id }) => id)
      delete values.DRIVER;
      values.active = active;
      addRole(values);
  };

  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const handleUserTypeChange = (identifier) => {
    fetchRoleByIdentifier(identifier);
  };
  const handleActive = () => {
    setActive(!active)
  };

  const preDefinedRoles = [];

  const handleRoleChange = (value) => {
    if(defaultPermission.some(role => role.name === value.name)){
      const filterPermission = defaultPermission && defaultPermission.filter(role => role.name !== value.name)
     setDefaultPermission(filterPermission)
    } else {
      setDefaultPermission([...defaultPermission, value])
    }
  };

  const permissionItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 14 },
      lg: { span: 14 },
      md: { span: 12 },
      sm: { span: 12 },
      xs: { span: 12 },
    },
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 0 },
      lg: { span: 24, offset: 0 },
      md: { span: 12, offset: 4 },
      sm: { span: 12, offset: 4 },
      xs: { span: 24, offset: 0 },
    },
  };

  const activeformItemLayout = {
    labelCol: {
      xl: { span: 4 },
      lg: { span: 4 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 8 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };

  useEffect(() => {
    fetchPermission()
    // form.setFieldsValue({
    //   id: id,
    // });
    fetchUserType();
    return () => {
      cleanRoleProps();
    };
  }, []);

 // setDefaultPermission

  useEffect(() => {
    setDefaultPermission(roles && roles.permissions)
    return () => {
     // cleanRoleProps();
    };
  }, [roles.permissions]);

  const getPermissionArray = () => {
    let fields = [];
    permissions && Object.keys(permissions).map(key => {
      fields.push(
        <Form.Item key={key} name={key} label={ ENTITY_VALUE[key]}>
              <Row>
                {
                  permissions && permissions[key].map(value =>
                    <Col key={value.id} span={8}>
                      <Checkbox checked={defaultPermission instanceof Array && defaultPermission.some(role => role.name === value.name) } onChange={() => handleRoleChange(value)} value={value.id} style={{ lineHeight: '32px', marginLeft:'10px' }}>
                        { ROLE_ENTITY[value.name]}
                      </Checkbox>
                    </Col>
                  )
                }
              </Row>
        </Form.Item>
      )
    });
    return fields;
  };

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Add New Role</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined/>
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/roles'}>Roles</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Add</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors}/>

          <div className="box box-default">
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    name="updateForm"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    form={form}
                  >
                    <Row gutter={18}>
                      <Row>
                        <Col {...permissionItemLayout} xl={22} lg={22} md={20} sm={24}>
                          <FormItem
                            name={'roleId'}
                            rules={[{ required: true, message: 'User  type is required.' }]}
                            label="User Type:"
                          >
                            <Select
                              showSearch
                              placeholder="Select user type"
                              optionFilterProp="children"
                              onChange={handleUserTypeChange}
                              filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
                                0
                              }
                            >

                              {userTypes instanceof Array &&
                              userTypes.map(d => <Option key={d.id}>{d.name}</Option>)}
                            </Select>
                          </FormItem>
                        </Col>
                        <Col xl={18} lg={18} md={18} sm={24}>
                          <FormItem
                            {...activeformItemLayout}
                            name={'active'}
                            label="Status"
                          >
                            <Checkbox checked={active} onChange={handleActive}/>
                          </FormItem>
                        </Col>
                      </Row>

                      <Title level={4}>Role Management</Title>
                      <Divider/>
                      {
                        roles && roles.permissions &&
                        <Row className={'permission-roles'}>
                          <Skeleton active loading={loading} paragraph={{ rows: 4 }}>

                            {

                              getPermissionArray()
                            }
                          </Skeleton>
                        </Row>
                      }
                      <Col xl={16} lg={24} md={24} sm={24}>
                        <FormItem {...submitFormLayout}>
                          <Button
                            className="btn-custom-primary mr-1"
                            shape="round"
                            htmlType="submit"
                            size={'large'}
                          >
                            Add
                          </Button>

                          <Link
                            to="/roles"
                            className="ant-btn ant-btn-round ant-btn-lg no-underline mt-3"
                          >
                            {'Cancel'}
                          </Link>
                        </FormItem>
                      </Col>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
  );
};

export default AddForm;
