import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  DatePicker,
  InputNumber,
  Checkbox,
  Typography,
  Affix,
} from 'antd';

import Message from '../Common/Message';
import { DATE_FORMAT_VIEW, ENTITY_VALUE, ROLE_ENTITY } from '../../constants/appConfig';
import { isEmpty } from '../../utils/commonUtil';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import { format } from 'echarts/src/export';

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const { Title } = Typography;
const { Option } = Select;

const EditForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const [permission, setPermission] = useState([]);
  const { roles, permissions, loading, errors, fetchRoleByIdentifier, updateRole, cleanRoleProps, fetchPermission } = props;

  const { setFieldsValue, setFields, validateFields, getFieldValue } = form;
  const [active, setActive] = useState(roles && roles.active);
  const handleInitialPermission = (value) => {
    let perm = []
    value && value.map(obj =>
      perm.push(obj.id)
    );
    return perm
  };

  const onFinish = values => {
    validateFields().then(values => {
      values['roleName'] = roles && roles.roleName;
      values['roleId'] = id;
      values['active'] = active;
      values['permissions'] = permission.length > 0 ? permission : handleInitialPermission(roles.permissions);
      updateRole(values);
    });
  };

  const handleRoleChecked = (key, value) => {
    roles.permissions && roles.permissions.map((obj) => {
        return obj.id === value.toString();
      },
    );
  };

  const onFinishFailed = errorInfo => {
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 8 },
      lg: { span: 8 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const formItemLayout2 = {
    labelCol: {
      xl: { span: 8 },
      lg: { span: 8 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 16 },
      lg: { span: 16 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 5 },
      lg: { span: 24, offset: 5 },
      md: { span: 12, offset: 0 },
      sm: { span: 12, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };


  const getDriverPermissionOption = (permissions) => {
    let options = [];
    permissions && permissions.map((obj) => {
      options.push({ label: ROLE_ENTITY[`${obj.name}`], value: obj.id });
    });
    return options;
  };

  const getDriverPermissionDefaultValue = permissions => {
    let defaultValue = [];
    permissions && permissions.map((obj) => {
      defaultValue.push(obj.id);
    });
    return defaultValue;
  };

  const handlePermissionChange = (values) => {
    setPermission(values);
  };

  const handleActive = () => {
    setActive(!active)
  };

  useEffect(() => {
    form.setFieldsValue({
      id: id,
    });
    fetchRoleByIdentifier(id);
    fetchPermission();
    return () => {
      cleanRoleProps();
    };
  }, []);

  useEffect(() => {
    form.setFieldsValue({
      id: id,
      name: roles?.name,
      active: roles?.active,
    });
    return () => {
      //roles
    };
  }, [roles]);

  return (
    <div className="container-fluid no-breadcrumb">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Update Role</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined/>
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/roles'}>Roles</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Update</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors}/>

          <div className="box box-default">
            <Spin spinning={loading} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    name="updateForm"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                    form={form}
                  >
                    <Row gutter={18}>
                      <Col xl={16} lg={12} md={18} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          name={'roleName'}
                          label="Name"
                        >
                          {
                            roles && roles.roleName
                          }
                        </FormItem>
                      </Col>
                      <Col xl={16} lg={12} md={18} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          name={'active'}
                          label="Status"
                        >
                          <Checkbox checked={active} onChange={handleActive}/>
                        </FormItem>
                      </Col>
                    </Row>
                    <Row>
                      <Col xl={16} lg={12} md={18} sm={24}>
                        <label style={{ marginRight: '20px' }}><strong> Manage Driver Permission:</strong> </label>
                        <Checkbox.Group
                          options={permissions && getDriverPermissionOption(permissions.DRIVER)}
                          defaultValue={roles && getDriverPermissionDefaultValue(roles.permissions)}
                          onChange={handlePermissionChange}
                        />

                      </Col>

                    </Row>
                    <Row>
                      <FormItem {...submitFormLayout}>
                        <Affix offsetBottom={30}>
                          <Button
                            className="btn-custom-primary mr-1"
                            shape="round"
                            htmlType="submit"
                            size={'large'}
                          >
                            Update
                          </Button>

                          <Link
                            to="/roles"
                            className="ant-btn ant-btn-round ant-btn-lg no-underline mt-3"
                          >
                            {'Cancel'}
                          </Link>
                        </Affix>
                      </FormItem>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
  );
};

export default EditForm;
