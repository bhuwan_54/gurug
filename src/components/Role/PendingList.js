import React, { useEffect } from 'react';
import { Table, Breadcrumb, Row, Col, Form, Card, Input } from 'antd';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import { searchItemLayout } from '../../utils/commonUtil';

const Search = Input.Search;
const columns = [
  {
    title: 'S.N',
    key: 'index',
    render: (text, record, index) => index + 1,
  },
  {
    title: 'Role Name',
    dataIndex: 'fullName',
    sorter: true,
  },
  {
    title: 'Username',
    dataIndex: 'username',
  },
  {
    title: 'Mobile Number',
    dataIndex: 'mobileNumber',
  },
  {
    title: 'Email',
    dataIndex: 'emailId',
  },
  {
    title: 'City',
    dataIndex: 'city',
  },
  {
    title: 'Action',
    key: 'operation',
    render: (text, record) => {
      return (
        <Link to={`/roles/${record.id}/detail`} title="View">
          <EyeOutlined style={{ fontSize: '22px', color: '#08c' }} />
        </Link>
      );
    },
  },
];

const PendingList = props => {
  const [form] = Form.useForm();
  const handleTableChange = (pagination, filters, sorter) => {
    this.props.fetchPendingRole({
      pageLimit: pagination.pageSize,
      pageNumber: pagination.current,
      sortField: sorter.field,
      sortOrder: sorter.order,
      ...filters,
    });
  };

  const { roles, errors, page, loading, fetchPendingRole, cleanRoleProps } = props;
  const { validateFields } = form;

  const onFinish = fieldsValue => {
    fieldsValue.searchParameter = fieldsValue['searchParameter'];
    fieldsValue.pageSize = page.pageSize;
    fieldsValue.sortParameter = page.sortParameter;
    fieldsValue.sortOrder = page.sortOrder;
    fetchPendingRole(fieldsValue);
    console.log('heree in sear', fieldsValue);
  };

  useEffect(() => {
    fetchPendingRole();
    return () => {
      cleanRoleProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section" key="1">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={16} lg={16} md={24} xs={24} sm={24}>
                <h4 className="article-title">Pending list</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined />
                    Dashboard
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    {' '}
                    <Link to={'/roles'}>roles</Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Pending</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>
            <Message error={errors} />
            <Card>
              <Row>
                <Col {...searchItemLayout} className="search-form mb-2">
                  <Form name={'search-form'} form={form}>
                    <Form.Item
                      name="searchParameter"
                      rules={[{ required: true, message: 'Please enter the search value' }]}
                    >
                      <Search
                        placeholder="Please enter value"
                        enterButton="Search"
                        onSearch={onFinish}
                      />
                    </Form.Item>
                  </Form>
                </Col>
              </Row>

              <div className="box box-default box-ant-table-v1">
                <div className="table-responsive">
                  <Table
                    columns={columns}
                    rowKey={record => record.id}
                    dataSource={roles instanceof Array ? roles : []}
                    pagination={roles.pagination}
                    loading={loading}
                    onChange={handleTableChange}
                  />
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default PendingList;
