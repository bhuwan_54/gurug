import React from 'react';
import { Tag, Tooltip } from 'antd';

const Section = props => {
  const { dashboards } = props;

  const customerInfo = dashboards.filter(
    obj =>
      obj.key === 'customer_total' ||
      obj.key === 'customer_total_active' ||
      obj.key === 'customer_total_refer' ||
      obj.key === 'customer_total_refer_active'
  );
  const driverInfo = dashboards.filter(
    obj =>
      obj.key === 'driver_total' ||
      obj.key === 'driver_pending_count' ||
      obj.key === 'driver_total_active' ||
      obj.key === 'driver_total_refer' ||
      obj.key === 'driver_total_refer_active'
  );
  const tripInfo = dashboards.filter(
    obj => obj.key === 'trip_average_distance' || obj.key === 'trip_total_count'
  );
  return (
    <div className="row">
      <div className="col-xl-3 mb-4">
        <div className="number-card-v1">
          <div className="card-top">
            <Tooltip title={'Total Customer'}>
              {' '}
              <span className="h5">Total: </span>{' '}
              <span style={{ marginBottom: '20px' }}>
                <strong className={'h3'}>{customerInfo.length > 0 && customerInfo[0]['value']}</strong>
              </span>
            </Tooltip>
          </div>
          <div className="card-info">
            <span>Customer</span>
          </div>
          <div className="card-bottom">
            <Tooltip title={'Total Active Customer'}>
              {' '}
              <span className="h4">Active </span>{' '}
              <span>
                <strong className='h3'>{customerInfo.length > 0 && customerInfo[1]['value']}</strong>
              </span>
            </Tooltip>
            {/*<Tooltip title={'Total Refer Customer'}>*/}
            {/*  {' '}*/}
            {/*  <span className="h4">Refered </span>{' '}*/}
            {/*  <span className="h4">*/}
            {/*    <Tag color="#f50">{customerInfo.length > 0 && customerInfo[2]['value']}</Tag>*/}
            {/*  </span>*/}
            {/*</Tooltip>*/}
            {/*<Tooltip title={'Total Refer Active Customer'}>*/}
            {/*  {' '}*/}
            {/*  <span className="h4">Refered Active </span>{' '}*/}
            {/*  <span className="h4">*/}
            {/*    <Tag color="#f50">{customerInfo.length > 0 && customerInfo[3]['value']}</Tag>*/}
            {/*  </span>*/}
            {/*</Tooltip>*/}
          </div>
        </div>
      </div>
      <div className="col-xl-3 mb-4">
        <div className="number-card-v1">
          <div className="card-top">
            <Tooltip title={'Total Driver Count'}>
              {' '}
              <span className="h5">Total: </span>{' '}
              <span>
                <strong className="h3">{driverInfo.length > 0 && driverInfo[1]['value']}</strong>
              </span>
            </Tooltip>
          </div>
          <div className="card-info">
            <span>Drivers</span>
          </div>
          <div className="card-bottom">
            <Tooltip title={'Total Active Driver'}>
              {' '}
              <span className="h4">Active </span>{' '}
              <span>
                <strong className={'h2'}>{driverInfo.length > 0 && driverInfo[2]['value']}</strong>
              </span>
            </Tooltip>
            <br/>
            <Tooltip title={'Total Pending Driver'}>
              {' '}
              <span className="h4"> {'  '} Pending </span>{' '}
              <span>
                <strong className={'h2'}>{driverInfo.length > 0 && driverInfo[1]['value']}</strong>
              </span>
            </Tooltip>
          </div>
        </div>
      </div>

      <div className="col-xl-3 mb-4">
        <div className="number-card-v1">
          <div className="card-top">
            {/*<Icon type="shopping-cart" className="text-info" />*/}
            <Tooltip title={'Total Trip Count'}>
              {' '}
              <span className="h5">Total: </span>{' '}
              <span className="h5">
                <strong className={'h2'}>{tripInfo.length > 0 && tripInfo[1]['value']}</strong>
              </span>
            </Tooltip>
          </div>
          <div className="card-info">
            <span>TRIP INFO </span>
          </div>
          <div className="card-bottom">
            <Tooltip title={'Average  Trip Distance'}>
              {' '}
              <span className="h4"> Average: </span>{' '}
              <span className="h5">
                <Tag color="purple">{tripInfo.length > 0 && tripInfo[0]['value']} Km</Tag>
              </span>
            </Tooltip>
          </div>
        </div>
      </div>

      <div className="col-xl-3 mb-4">
        <div className="number-card-v1">
          <div className="card-top">
            {/*<Icon type="shopping-cart" className="text-info" />*/}
            <Tooltip title={'Total Customer Refer / Active Refer customer'}>
              {' '}
              <span className="h4">Customer: </span>{' '}
              <span className={'h5'}>
                <strong className={'h5'}>{customerInfo.length > 0 && customerInfo[2]['value']} / {customerInfo.length > 0 && customerInfo[3]['value']} </strong>
              </span>
            </Tooltip>
          </div>
          <div className="card-info">
            <span>Refered </span>
          </div>
          <div className="card-bottom">
            <Tooltip title={'Total Refered Driver / Active Refered Driver'}>
              {' '}
              <span className="h4"> Driver: </span>{' '}
              <span className="h5">
                <strong className={'h5'}> {driverInfo.length > 0 && driverInfo[2]['value']} / {driverInfo.length > 0 && driverInfo[3]['value']}</strong>
              </span>
            </Tooltip>
          </div>
        </div>
      </div>

      {/*<div className="col-xl-3 mb-4">*/}
      {/*  <div className="number-card-v1">*/}
      {/*    <div className="card-top">*/}
      {/*      <span>55<span className="h5">%</span></span>*/}
      {/*    </div>*/}
      {/*    <div className="card-info">*/}
      {/*      <span>Growth</span>*/}
      {/*    </div>*/}
      {/*    <div className="card-bottom">*/}
      {/*      /!*<Icon type="rocket" className="text-warning" />*!/*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</div>*/}
    </div>
  );
};

export default Section;
