import React, { useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import MainChart from './MainChart';
import NumberCards from './NumberCards';
import ProjectTable from './ProjectTable';
import Message from '../Common/Message';

const Dashboard = props => {
  const { dashboards, errors, loading } = props;

  useEffect(() => {
    props.fetchDashboadStats();
    return () => {
      //todo
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <Message error={errors} />
        <h4 className="article-title">Dashboard</h4>
        <div key="2">
          {' '}
          <NumberCards dashboards={dashboards} />{' '}
        </div>
        <div key="1">
          {' '}
          <MainChart />{' '}
        </div>
        <div key="3">
          {' '}
          {/*<ProjectTable />{' '}*/}
        </div>
      </QueueAnim>
    </div>
  );
};

export default Dashboard;
