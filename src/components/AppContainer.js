import React,{Fragment, Component} from 'react';
import loadable from 'react-loadable';
import LoadingComponent from 'components/Loading';

import { withRouter, Switch, Route } from 'react-router-dom';

// Import custom components
import PrivateRoute from '../routes/PrivateRoute';
import RestrictRoute from '../routes/RestrictRoute';
import PublicRoute from '../routes/PublicRoute';
import NotFound from '../containers/Exception/NotFoundContainer';
import Forbidden from './Exception/Forbidden';
import InternalServer from '../containers/Exception/InternalServerContainer';

let AsyncAppLayout = loadable({
  loader: () => import('components/Layout/AppLayout/'),
  loading: LoadingComponent
})
// let AsyncException = loadable({
//   loader: () => import('routes/exception/'),
//   loading: LoadingComponent
// })

let AsyncLogin = loadable({
  loader: () => import('containers/Auth/LoginContainer'),
  loading: LoadingComponent
})

let AsyncReset = loadable({
  loader: () => import('containers/Auth/ResetContainer'),
  loading: LoadingComponent
})

let AsyncDriver = loadable({
  loader: () => import('containers/Drivers/'),
  loading: LoadingComponent
})


let AsyncDashboard = loadable({
  loader: () => import('containers/Dashboard/'),
  loading: LoadingComponent
})


class AppContainer extends Component {
  render() {
    const { match, location } = this.props;
    // const isRoot = location.pathname === '/' ? true : false;
    // if (isRoot) {
    //   return ( <Redirect to={'/user/login'}/> );
    // }

    return (
      <Fragment>
      <Switch>
        <Route exact path="/" component={AsyncLogin} />
        <Route path="/reset"  component={AsyncReset} />

        <PublicRoute path="/dashboard" layout={AsyncAppLayout} component={AsyncDashboard} />
        <PublicRoute path="/drivers" layout={AsyncAppLayout} component={AsyncDriver} />
         <PublicRoute path="/roles" layout={AsyncAppLayout} component={AsyncDashboard} />
        <PublicRoute path="/customers" layout={AsyncAppLayout} component={AsyncDashboard} />

        <PublicRoute path="/profile" layout={AsyncAppLayout} component={AsyncDashboard} /> */}

        <PublicRoute path="/403" layout={AsyncAppLayout} component={Forbidden} />

        <Route path="/500" layout={AsyncAppLayout} component={InternalServer} />
        <Route path="/404" layout={AsyncAppLayout}  component={NotFound} />
        <Route component={NotFound} />
      </Switch>
    </Fragment>
    );
  }
}

export default AppContainer;
