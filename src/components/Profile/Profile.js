import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Spin, Row, Card, Col, Breadcrumb, Descriptions, Avatar, Tabs } from 'antd';
import QueueAnim from 'rc-queue-anim';
import { HomeOutlined, UserOutlined } from '@ant-design/icons';

import Message from '../Common/Message';

const FormItem = Form.Item;
const { TabPane } = Tabs;

const ProfileForm = (props) => {
  const [form] = Form.useForm();
  const [tabKey, setTabKey] = useState('1');
  const { loading, errors, updateProfile,fetchProfile,users } = props;

  const { getFieldValue } = form;

  useEffect(() => {
    fetchProfile();
    return () => {
    };
  }, []);

  const onFinish = values => {
    updateProfile(values);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 4 },
      sm: { span: 12, offset: 4 },
      xs: { span: 24, offset: 0 },
    },
  };

  const changeTabPanel = (key) => {
    setTabKey(key);
  };

  return (
    <Spin spinning={loading} delay={1000} size={'large'}>
      <div className="container-fluid no-breadcrumb page-dashboard">
        <QueueAnim type="bottom" className="ui-animate">
          <article className="article" id="components-form-demo-advanced-search">
            <Row type="flex" justify="space-between">
              <Col xl={16} lg={16} md={24} xs={24} sm={24}>
                <h4 className="article-title">Profile</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined/>
                    Dashboard
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Profile</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>
            <Message error={errors}/>
            <section className="form-v1-container">
              <Card>
                <>
                  <Tabs defaultActiveKey={tabKey} tabPosition={'left'} onChange={changeTabPanel}>
                    <TabPane tab="Personal Information" key="1">
                          <h4 style={{marginLeft:'140px'}}>Personal Information</h4>
                      <Row justify={'center'}>
                        <Col xl={14} lg={14} md={12} sm={18} xs={24} {...formItemLayout}>
                          <Form.Item label="UserName">{users?.fullName }</Form.Item>
                          <Form.Item label="Mobile">{users?.mobileNumber}</Form.Item>
                          <Form.Item label="Email">{users?.emailId}</Form.Item>
                          <Form.Item label="Maker"> {users?.maker}
                          </Form.Item>

                        </Col>
                        <Col xl={6} lg={6} md={12} sm={18} xs={24}>
                          <Avatar size={80} icon={<UserOutlined/>}/>

                        </Col>

                      </Row>

                    </TabPane>
                    <TabPane tab="Change Password" key="3">
                      <h4>Change Password</h4>
                      <Form
                        {...formItemLayout}
                        form={form}
                        name="normal_login"
                        className="login-form"
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                      >
                        <Col xl={18} lg={18} md={18} sm={24}>
                          <FormItem
                            name="oldPassword"
                            label="Current Password"
                            rules={[
                              {
                                required: true,
                                message: 'Please input your current password',
                              },
                            ]}
                            hasFeedback
                          >
                            <Input.Password/>
                          </FormItem>
                        </Col>
                        <Col xl={18} lg={18} md={18} sm={24}>
                          <FormItem
                            name="newPassword"
                            label="New Password"
                            rules={[
                              {
                                required: true,
                                message: 'Please input new password',
                              },
                            ]}
                            hasFeedback
                          >
                            <Input.Password/>
                          </FormItem>
                        </Col>
                        <Col xl={18} lg={18} md={18} sm={24}>
                          <FormItem
                            name="confirmPassword"
                            label="Confirm Password"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                              {
                                required: true,
                                message: 'Please confirm your password!',
                              },
                              ({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if (!value || getFieldValue('newPassword') === value) {
                                    return Promise.resolve();
                                  }

                                  return Promise.reject('The two passwords that you entered do not match!');
                                },
                              }),
                            ]}
                          >
                            <Input.Password/>
                          </FormItem>
                        </Col>
                        <Col>
                          <FormItem   {...submitFormLayout}
                          >
                            <Button
                              type="primary"
                              htmlType="submit"
                              loading={loading}
                              className="btn-cta"
                            >
                              Submit
                            </Button>
                          </FormItem>
                        </Col>

                      </Form> </TabPane>
                  </Tabs>,


                </>
              </Card>
            </section>
          </article>
        </QueueAnim>
      </div>
    </Spin>
  );
};

export default ProfileForm;
