import React, { useState, useEffect } from 'react';
import { Upload, Button, Form, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

message.config({
  duration: 4,
  maxCount: 1,
});

const ImageUploadManually = props => {
  // const [form] = Form.useForm();

  const [selectedFileList, setSelectedFileList] = useState([]);
  const [showFileUpload, setShowFileUpload] = useState(true);
  const [showUploadButton, setShowUploadButton] = useState(true);
  const [imageUrl, setImageUrl] = useState();

  const { fileName, acceptType, placeholder, fileType, imageSrc } = props;

  useEffect(() => {
  }, [showUploadButton]);

  const getFileType = fileList => {
    let fileTypes = [];
    fileList &&
    fileList.map(item => {
      switch (item) {
        case 'image/jpeg':
          fileTypes.push('jpeg image');
          break;
        case 'image/png':
          fileTypes.push('png image');
          break;
        case 'text/plain':
          fileTypes.push('text file');
          break;
        case 'text/csv':
          fileTypes.push('csv file');
          break;
        case 'application/pkix-cert':
          fileTypes.push('crt file');
          break;
        case 'application/vnd.ms-excel':
          fileTypes.push('excel file');
          break;
        case 'application/pdf':
          fileTypes.push('pdf file');
          break;
        case 'application/vnd.android.package-archive':
          fileTypes.push('apk file');
          break;
        case 'video/mp4':
          fileTypes.push('mp4 video');
          break;
        default:
          fileTypes = 'unknown file type';
      }
    });
    return fileTypes;
  };

  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  const fileProps = {
    onChange: async ({ file, fileList }) => {
      // Only to show one recent uploaded files, and old ones will be replaced by the new
      fileList = fileList.slice(-1);

      setSelectedFileList({ [fileName]: fileList });
      if (file.status === undefined) {
        props.form.setFieldsValue({ [fileName]: fileList });
      }
    },

    onRemove: () => {
      setSelectedFileList({ selectedFileList });
      delete selectedFileList[fileName];
      setShowFileUpload(true);
      props.form.setFieldsValue({ [fileName]: undefined });
      props.form.validateFields([fileName], { force: true });
      return selectedFileList;
    },

    beforeUpload: (file, fileList) => {
      let fileType = acceptType.split(',');
      if (fileType.includes(file.type)) {
        if (file.size <= props.sizeOfFile) {
          setShowFileUpload(true);
          getBase64(file, imageUrl => setImageUrl(imageUrl));
          setSelectedFileList({ [fileName]: file });
          setShowUploadButton(false);
          return false;
        } else {
          message.error(`The file that you attempt to upload ${
            file.name
          } is large size.The file size must be less than ${props.sizeOfFile / 1024} KB.`);
          // props.form.setFields({
          //   [fileName]: {
          //     errors: [
          //       new Error(
          //         `The file that you attempt to upload ${
          //           file.name
          //         } is large size.The file size must be less than ${props.sizeOfFile / 1024} KB.`
          //       ),
          //     ],
          //   },
          // });
          setShowFileUpload(false);
        }
      } else {
        message.error(`The file that you attempt to upload  ${
          file.name
        } is not supported. Only ${getFileType(acceptType.split(','))}
           are accepted.`);

        // props.form.setFields({
        //   [fileName]: {
        //     errors: [
        //       new Error(`The file that you attempt to upload  ${
        //         file.name
        //       } is not supported. Only ${getFileType(acceptType.split(','))}
        //    are accepted.`),
        //     ],
        //   },
        // });


        setShowFileUpload(false);
      }
    },
    selectedFileList: selectedFileList[fileName],
  };
  //
  // useEffect(() => {
  //   props.form.setFieldsValue({
  //     [fileName]: selectedFileList[fileName],
  //   });
  // }, [selectedFileList]);

  const uploadButton = (
    showUploadButton ?
      <div>
        <div className="ant-upload-text">Image</div>
      </div>
      : ''
  );

  return (
    <Upload
      {...fileProps}
      name={fileName}
      id={fileName}
      fileList={selectedFileList[fileName]}
      accept={acceptType}
      listType={fileType}
      showUploadList={false}
      className="avatar-uploader"
      // showUploadList={showFileUpload}
    >
      {/*<UploadOutlined /> {placeholder}*/}
      {imageUrl || imageSrc !== undefined ?
        <img src={imageUrl === undefined ? imageSrc : imageUrl} alt="avatar" style={{ width: '100%' }}/> : uploadButton}

    </Upload>
  );
};

export default ImageUploadManually;
