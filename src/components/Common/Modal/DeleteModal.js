import React from 'react';
import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const DeleteModel = props => {
  Modal.confirm({
    title: props.title || 'Are you sure want to delete?',
    width: props.width || 320,
    icon: <ExclamationCircleOutlined/>,
    cancelText: 'No',
    okText: 'Yes',
    onOk() {
      props.deleteEntity(props.id)
        .then((res) => {
        if(res.status === 200){
          props.fetchEntity(props.data)
        }
      })
    },
  });
};

export default DeleteModel;
