import React from 'react';
import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const ApproveModel = props => {
  Modal.confirm({
    title: props.title,
    width: props.width,
    icon: <ExclamationCircleOutlined/>,
    cancelText: 'No',
    okText: 'Yes',
    onOk() {
      props.approveEntity({ id: props.id, apiUrlKey: props.apiUrlKey });
    },
  });
};

export default ApproveModel;
