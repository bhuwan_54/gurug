import React, {  useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import {  Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Button, Breadcrumb, Spin, Select, Input, InputNumber, DatePicker, Affix, Divider } from 'antd';

import Message from '../Common/Message';
import { HomeOutlined } from '@ant-design/icons';

const FormItem = Form.Item;
const { Option } = Select;

const AddForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const {
    commissions,
    profiles,
    loading,
    errors,
    addDriverCommission,
    fetchCommissionProfile,
    fetchCommissionType,
    commissionType,
    fetchDriverCommissionWithCriteria,
    cleanDriverCommissionsProps,
  } = props;

  const onFinish = values => {
    let formData = [];
    const goldProfile = {
      commissionType: values.goldCommissionType,
      commissionProfile:values.goldCommissionProfile,
      value: values.goldValue
    };
    const silverProfile = {
      commissionType: values.silverCommissionType,
      commissionProfile:values.silverCommissionProfile,
      value: values.silverValue
    };
    const thirdPartyProfile = {
      commissionType: values.thirdCommissionType,
      commissionProfile:values.thirdCommissionProfile,
      value: values.thirdValue
    };
    formData.push(goldProfile);
    formData.push(silverProfile);
    formData.push(thirdPartyProfile);
    addDriverCommission(formData);
  };

  const onFinishFailed = errorInfo => {};

  const handleCancel = () => {
    form.resetFields();
  };

  const handleGoldTypeChange = () => {
    form.resetFields(['goldValue'])
  };
  const handleSilverTypeChange = () => {
    form.resetFields(['silverValue'])
  };
  const handleThirdPartyTypeChange = () => {
    form.resetFields(['thirdValue'])
  };

  const filterGoldDriver = commissions && commissions.filter(commission => commission.commissionProfile === 'GOLD_DRIVER')[0];
  const filterSilverDriver = commissions && commissions.filter(commission => commission.commissionProfile === 'SILVER_DRIVER')[0];

  const filterThirdParty = commissions && commissions.filter(commission => commission.commissionProfile === 'THIRD_PARTY')[0];
  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 9 },
      lg: { span: 24, offset: 9 },
      md: { span: 12, offset: 4 },
      sm: { span: 12, offset: 4 },
      xs: { span: 24, offset: 0 },
    },
  };

  useEffect(() => {
    fetchCommissionType();
    fetchCommissionProfile();
    fetchDriverCommissionWithCriteria();
    return () => {
      cleanDriverCommissionsProps();
    };
  }, []);

  useEffect(() => {
     form.setFieldsValue({'thirdCommissionProfile': filterThirdParty && filterThirdParty.commissionProfile, thirdValue: filterThirdParty &&  filterThirdParty.value, thirdCommissionType:filterThirdParty && filterThirdParty.commissionType});
     form.setFieldsValue({'goldCommissionProfile': filterGoldDriver && filterGoldDriver.commissionProfile, goldValue: filterGoldDriver &&  filterGoldDriver.value, goldCommissionType:filterGoldDriver && filterGoldDriver.commissionType});
     form.setFieldsValue({'silverCommissionProfile': filterSilverDriver && filterSilverDriver.commissionProfile, silverValue: filterSilverDriver &&  filterSilverDriver.value, silverCommissionType:filterSilverDriver && filterSilverDriver.commissionType});
     form.setFieldsValue({'silverCommissionProfile': 'SILVER'});
    form.setFieldsValue({'goldCommissionProfile': 'GOLD' });


  }, [filterThirdParty,filterGoldDriver,filterSilverDriver]);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title"> Add New Commissions</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/commissions'}>Commissions</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>New</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />

          <div className="box box-default">
            <Spin spinning={loading} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="">
                  <Form
                    {...formItemLayout}
                    name="updateForm"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                    form={form}
                  >
                    <Col span={18}>
                      <Divider orientation="left" plain>
                        Gold Driver
                      </Divider>
                      <Col xl={14} lg={14} md={24} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          label="Commission Profile"
                          name={'goldCommissionProfile'}
                          rules={[{ required: true, message: 'Commission profile is required.' }]}
                        >
                          <Select
                            defaultValue={'GOLD'}
                            value={'GOLD'}
                            showSearch
                            disabled
                            placeholder="Select"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {profiles &&
                            profiles.map(el => <Option value={el.value}>{el.value}</Option>)}
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'goldCommissionType'}
                          rules={[{ required: true, message: 'Commission type is required.' }]}
                          label="Commission Type"
                        >
                          <Select
                            showSearch
                            placeholder="Select commission type"
                            optionFilterProp="children"
                            onChange={handleGoldTypeChange}
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {commissionType &&
                              commissionType.map(val => (
                                <Option value={val.value}>{val.value}</Option>
                              ))}
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'goldValue'}
                          initialValues={filterGoldDriver && filterGoldDriver.value}
                          rules={[
                            {
                              required: true,
                              message: 'Gold commission value is required.',
                            },
                            ({ getFieldValue }) => ({
                              validator(rule, value) {
                                if (value && getFieldValue('goldCommissionType') === 'PERCENTAGE' && parseInt(value) > 100) {
                                  return Promise.reject('Percentage must not be greater than 100');
                                }
                                return Promise.resolve();
                              },
                            }),
                          ]}
                          label="Commission Value"
                        >
                          <InputNumber placeholder={'value'} />
                        </FormItem>
                      </Col>
                    </Col>

                    <Col span={18}>
                      <Divider orientation="left" plain>
                        Silver Driver
                      </Divider>
                      <Col xl={14} lg={14} md={24} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          label="Commission Profile"
                          name={ 'silverCommissionProfile'}
                          initialValues={'SILVER'}
                          rules={[{ required: true, message: 'Commission profile is required.' }]}
                        >
                          <Select
                            showSearch
                            defaultValue={'SILVER'}
                            value={'SILVER'}
                            disabled
                            placeholder="Select"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {profiles &&
                            profiles.map(el => <Option value={el.value}>{el.value}</Option>)}
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={ 'silverCommissionType'}
                          rules={[{ required: true, message: 'Commission type is required.' }]}
                          label="Commission Type"
                        >
                          <Select
                            showSearch
                            placeholder="Select commission type"
                            optionFilterProp="children"
                            onChange={handleSilverTypeChange}
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {commissionType &&
                            commissionType.map(val => (
                              <Option value={val.value}>{val.value}</Option>
                            ))}
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={'silverValue'}
                          rules={[
                            {
                              required: true,
                              message: 'Silver commission value is required.',
                            },
                            ({ getFieldValue }) => ({
                              validator(rule, value) {
                                if (value && getFieldValue('silverCommissionType') === 'PERCENTAGE' && parseInt(value) > 100) {
                                  return Promise.reject('Percentage must not be greater than 100');
                                }
                                return Promise.resolve();
                              },
                            }),
                          ]}
                          label="Commission Value"
                        >
                          <Input placeholder={'value'} />
                        </FormItem>
                      </Col>
                    </Col>

                    <Col span={18}>
                      <Divider orientation="left" plain> Third Party
                      </Divider>
                      <Col xl={14} lg={14} md={24} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          label="Commission Profile"
                          name={['thirdCommissionProfile']}
                          rules={[{ required: true, message: 'Commission profile is required.' }]}
                        >
                          <Select
                            showSearch
                            placeholder="Select"
                            defaultValue={'THIRD_PARTY'}
                            value={'THIRD_PARTY'}
                            disabled
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {profiles &&
                            profiles.map(el => <Option value={el.value}>{el.value}</Option>)}
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={['thirdCommissionType']}
                          rules={[{ required: true, message: 'Commission type is required.' }]}
                          label="Commission Type"
                        >
                          <Select
                            showSearch
                            placeholder="Select commission type"
                            optionFilterProp="children"
                            onChange={handleThirdPartyTypeChange}
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {commissionType &&
                            commissionType.map(val => (
                              <Option value={val.value}>{val.value}</Option>
                            ))}
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={14} lg={14} md={18} sm={24}>
                        <FormItem
                          name={['thirdValue']}
                          rules={[
                            {
                              required: true,
                              message: 'Commission value is required.',
                            },
                            ({ getFieldValue }) => ({
                              validator(rule, value) {
                                if (value && getFieldValue('thirdCommissionType') === 'PERCENTAGE' && parseInt(value) > 100) {
                                  return Promise.reject('Percentage must not be greater than 100');
                                }
                                return Promise.resolve();
                              },
                            }),
                          ]}
                          label="Commission Value"
                        >
                          <Input placeholder={'value'} />
                        </FormItem>
                      </Col>
                      <Col xl={16} lg={24} md={24} sm={24}>
                        <Affix offsetBottom={30}>
                          <FormItem {...submitFormLayout}>
                            <Button
                              className="btn-custom-primary mr-1"
                              shape="round"
                              loading={loading}
                              htmlType="submit"
                              size={'large'}
                            >
                              Add
                            </Button>
                            <Button
                              className=" mr-1"
                              shape="round"
                              onClick={handleCancel}
                              size={'large'}
                              danger
                            >
                              Cancel
                            </Button>

                            <Link
                              to="/dashboard"
                              className="ant-btn ant-btn-round ant-btn-lg no-underline mt-3"
                            >
                              {'Back'}
                            </Link>
                          </FormItem>
                        </Affix>
                      </Col>
                    </Col>

                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
  );
};

export default AddForm;
