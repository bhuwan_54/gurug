import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form } from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import moment from 'moment';

import { PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';
import Message from '../Common/Message';
import { searchItemLayout, getSortingOrder } from '../../utils/commonUtil';

const Search = Input.Search;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [form] = Form.useForm();

  const { driverCommissions, fetchDriverCommissionWithCriteria, errors, pagination, loading, cleanDriverCommissionsProps } = props;
  const { validateFields, getFieldValue, getFieldDecorator } = form;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchDriverCommissionWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = page.pageNumber ? page.pageNumber : PAGE_NUMBER;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchDriverCommissionWithCriteria(fieldsValue);
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Commission Profile',
      dataIndex: 'commission',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.commissionProfile}`}</div>;
      },
    },
    {
      title: 'Commission Type',
      dataIndex: 'commissionType',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.commissionType}`}</div>;
      },
    },
    {
      title: 'Value',
      dataIndex: 'value',
      align: 'left',
    },
    // {
    //   title: 'Created By',
    //   dataIndex: 'createdBy',
    //   align: 'left',
    //   sorter: true,
    // },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      sorter: true,
      render: record => {
        return (
          <div>
            {' '}
            {moment(record.createdOn).format('YYYY-MM-DD') // display
            }
          </div>
        );
      },
    },
    {
      title: 'Modified On',
      dataIndex: 'modifiedOn',
      align: 'left',
      sorter: true,
      render: record => {
        return (
          <div>
            {' '}
            {moment(record.modifiedOn).format('YYYY-MM-DD') // display
            }
          </div>
        );
      },
    },
    // {
    //   title: 'Action',
    //   key: 'operation',
    //   render: (text, record) => {
    //     return (
    //       <Link to={`/drivers/${record.id}/detail`} title="View">
    //         <EyeOutlined style={{ fontSize: '22px', color: '#08c' }} />
    //       </Link>
    //     );
    //   },
    // },
  ];

  useEffect(() => {
    fetchDriverCommissionWithCriteria({ pageSize: PAGE_SIZE, pageNumber: PAGE_NUMBER });
    return () => {
      cleanDriverCommissionsProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Commission List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined/>
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Commissions</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="search-form mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size="middle"
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={driverCommissions instanceof Array ? driverCommissions : []}
                      pagination={{
                        total: pagination.total,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
