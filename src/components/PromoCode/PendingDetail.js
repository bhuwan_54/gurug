import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Button, Breadcrumb, Card, Descriptions, Skeleton } from 'antd';
import dayjs from 'dayjs';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import ApproveModel from '../Common/Modal/ApproveModal';
import { DATE_FORMAT } from '../../constants/appConfig';
import Message from '../Common/Message';

const PendingDetail = props => {
  const { id } = useParams();

  const {
    promoCodes,
    errors,
    loading,
    fetchPendingDetailByIdentifier,
    approveRequest,
    cleanPromoCodeProps,
  } = props;

  const approveModalProps = {
    title: 'Arrpove Request?',
    width: '380px',
    id: promoCodes.id,
    approveEntity: approveRequest,
  };

  const newValues = promoCodes ? promoCodes.newData : '';
  const oldValues = promoCodes ? promoCodes.oldData : '';

  const oldActive = oldValues ? oldValues.active : null;
  const newActive = newValues ? newValues.active : null;

  useEffect(() => {
    fetchPendingDetailByIdentifier(id);
    return () => {
      cleanPromoCodeProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Pending Detail'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/promoCodes'}>Promo Codes</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/promoCodes/drafts'}>Pending</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'View'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />

          <Card>
            <Skeleton loading={loading} active>
              <Col lg={18} md={24} sm={24}>
                <Descriptions size="default">
                  <Descriptions.Item label={'Request'}>{promoCodes.action}</Descriptions.Item>
                </Descriptions>
              </Col>
              <Col lg={18} md={24} sm={24}>
                <Descriptions size="default">
                  <Descriptions.Item label={'Request By'}>
                    {promoCodes.requestedBy}
                  </Descriptions.Item>
                </Descriptions>
              </Col>
              <Col lg={18} md={24} sm={24}>
                <Descriptions size="default">
                  <Descriptions.Item label={'Requested On'}>
                    {promoCodes.requestedOn
                      ? dayjs(promoCodes.requestedOn).format(DATE_FORMAT)
                      : ''}
                  </Descriptions.Item>
                </Descriptions>
              </Col>
              <div className="detail-table table-responsive"></div>
              <Button
                type="primary"
                className="btn-custom-field m-1"
                onClick={() => ApproveModel(approveModalProps)}
              >
                {'Approve'}
              </Button>
              )}
              <Link to="/promoCodes/pending" className="ant-btn no-underline m-1">
                {'Cancel'}
              </Link>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default PendingDetail;
