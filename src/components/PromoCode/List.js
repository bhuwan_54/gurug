import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import {
  Row,
  Col,
  Input,
  Table,
  Breadcrumb,
  Card,
  Form,
  Button,
  Affix,
  Skeleton,
  Modal,
  Select,
  Tag,
  DatePicker, InputNumber,
} from 'antd';
import { EditOutlined, HomeOutlined, EyeOutlined,CheckOutlined,CloseSquareOutlined } from '@ant-design/icons';
import moment from 'moment';

import {
  colorStatus,
  DATE_FORMAT,
  DATE_FORMAT_VIEW,
  DATE_FORMAT_VIEW2,
  DATE_SERVER,
  PAGE_NUMBER,
  PAGE_SIZE,
} from '../../constants/appConfig';
import Message from '../Common/Message';
import { searchItemLayout, getSortingOrder } from '../../utils/commonUtil';
import ApproveModel from '../Common/Modal/ApproveModal';
import { Link } from 'react-router-dom';

const Search = Input.Search;
const FormItem = Form.Item;
const { Option } = Select;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [selectedPromo, setSelectedPromo] = useState();
  const [promo, setPromo] = useState();
  const [viewModalVisible, setViewModalVisible] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);

  const [form] = Form.useForm();
  const {
    promoCodes,
    fetchPromoCodeWithCriteria,
    errors,
    pagination,
    loading,
    updatePromoCodes,
    updatePromoCodeStatus,
    cleanPromoCodeProps,
    promoCodeSet
  } = props;
  const { validateFields, getFieldValue, getFieldDecorator } = form;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchPromoCodeWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = PAGE_SIZE;
      fieldsValue.pageNumber = 1;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchPromoCodeWithCriteria(fieldsValue);
    });
  };

  const handleInitialValue = (promoCodes) => {
    promoCodes instanceof Array && promoCodes.map(value => {
      Object.keys(value).map(key => {
        if (key === 'effectiveFrom' || key === 'effectiveTo') {
          value[key] = value[key] && moment(value[key], DATE_FORMAT_VIEW);
        }
      });
      return value;
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      align:'center',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Name',
      dataIndex: 'name',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Code',
      dataIndex: 'code',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Details',
      dataIndex: 'details',
      align: 'left',
    },
    {
      title: 'Discount Amount Bike',
      dataIndex: 'discountAmountBike',
      align: 'left',
    },
    {
      title: 'Discount Amount Car',
      dataIndex: 'discountAmountCar',
      align: 'left',
    },
    {
      title: 'Status',
      key: 'disabled',
      dataIndex: 'disabled',
      render: (text, record) => {
        return <Tag color={record && record.disabled ? '#1890ff' : '#ff4d4f'}>
          {record && record.disabled ? 'Disabled' : 'Activated'}
        </Tag>
      },
    },
    {
      title: 'Effective From',
      dataIndex: 'effectiveFrom',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{moment(record.effectiveFrom).format('DD MMM YYYY HH:mm:ss')}</div>;
      },
    },
    {
      title: 'Effective To',
      dataIndex: 'effectiveTo',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{moment(record.effectiveTo).format('DD MMM YYYY HH:mm:ss')}</div>;
      },
    },
    {
      title: 'Action',
      key: 'operation',
      align: 'center',
      fixed: 'right',
      width: 80,
      render: (text, record) => {
        return (
          <div>
            <Link to={`/promoCodes/${record.id}/detail`} title="View">
              <Button shape="circle"  icon={<EyeOutlined  style={{ fontSize: '16px' }}/>}  style={{margin:'5px'}}/>
            </Link>
            <Link to={`/promoCodes/${record.id}/edit`} title="Edit">
              <Button shape="circle" icon={<EditOutlined  style={{ fontSize: '16px' }}/>} />
            </Link>
            <Button title={!record.disabled ? 'disable':'activate'} icon={record.disabled ? <CheckOutlined style={{ fontSize: '16px' }}/> : <CloseSquareOutlined style={{ fontSize: '16px' }}/>} shape="circle"  onClick={() => ApproveModel({
              title: !record.disabled ? 'Are you sure want to disable ?':'Are you sure want to activate ?',
              apiUrlKey: !record.disabled ? 'disable':'activate',
              width: '400px',
              id:record.id,
              approveEntity: updatePromoCodeStatus
            })}>
            </Button>
          </div>
        );
      },
    },
  ];

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 18 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };

  const showEditModal = async (id) => {
    console.log('id',id)
    setSelectedPromo(id);
    const values = promoCodes.map(val => {
      if(val.id === id){
        return val
      }
    });
    await setPromo(values);
    console.log(values,'values')
    console.log(promo,'promo')
    promo.effectiveFrom = moment(promo.effectiveFrom);
    promo.effectiveTo = moment(promo.effectiveTo);

    form.setFieldsValue(promo);
    // promoCodeSet(promo)
    setEditModalVisible(true);
  };

  const hideEditModal = () => {
    if (editModalVisible) {
      setEditModalVisible(false);
    }
  };

  const showViewModal = (id) => {
    setPromo(promoCodes.filter(val => val.id === id)[0]);
    setViewModalVisible(true);
  };

  const hideViewModal = () => {
    if (viewModalVisible) {
      setViewModalVisible(false);
    }
  };

  const onFinishEdit = () => {
    // values.id = selectedPromo;
    Object.keys(promo).map(key => {
      if (promo && moment.isMoment(promo[key])) {
        promo[key] = moment(promo[key], DATE_SERVER)
          .startOf('days')
          .format(DATE_SERVER);
      }
    });
    updatePromoCodes(promo);
  };

  const modalProps = {
    apiUrlKey: promo && promo.disabled ? 'activate' : 'disable',
    id: selectedPromo,
  };

  const updateStatus = () => {
    updatePromoCodeStatus(modalProps);
    hideEditModal();
  };

  useEffect(() => {
    fetchPromoCodeWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });

    return () => {
      //cleanPromoCodeProps()
    };
  }, []);

  useEffect(() => {
    // fetchPromoCodeWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });

    return () => {
      // cleanPromoCodeProps();
    };
  }, [selectedPromo]);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Promo Codes List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined/>
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Promo Codes</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="search-form mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size="small"
                      bordered
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={promoCodes instanceof Array ? promoCodes : []}
                      pagination={{
                        total: pagination.totalData,
                        showSizeChanger: true,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
