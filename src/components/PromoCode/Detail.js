import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Breadcrumb,
  Card,
  Skeleton,
  Divider,
  Typography,
  Affix,
  Input,
  Select,
  DatePicker,
  InputNumber,
  Button,
} from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import moment from 'moment';

import Message from '../Common/Message';
import {DATE_FORMAT, READ_ABLE_DATE_FORMAT} from '../../constants/appConfig';

const FormItem = Form.Item;
const { Title } = Typography;
let promo ;
const Detail = props => {
  const { id } = useParams();
  const [form] = Form.useForm();
  const { errors, promoCodes, loading, fetchPromoCodeByIdentifier, cleanPromoCodeProps } = props;

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 8 },
      sm: { span: 12, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  useEffect(() => {
    fetchPromoCodeByIdentifier(id);
    return () => {
      cleanPromoCodeProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Promo Code View'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/promoCodes'}>{'Promo Code'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'View'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />
          <Card className="detail-view">
            <Skeleton loading={false} active>
              <Form {...formItemLayout} className="gurug-form" layout="horizontal">
                <Row>
                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem label="Name">{promoCodes?.name}</FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem label={'Code'}>{promoCodes?.code} </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem label="Details">{promoCodes?.details} </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem label="Discount Type">{promoCodes?.discountType}</FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem label="Effective From">{moment(promoCodes?.effectiveFrom).format(READ_ABLE_DATE_FORMAT)} </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem label="Effective To">{moment(promoCodes?.effectiveTo).format(READ_ABLE_DATE_FORMAT)} </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem label="Discount Amount Bike">
                      {promoCodes?.discountAmountBike}
                    </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem label="Discount Amount Car">{promoCodes?.discountAmountCar}</FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem label="Created On">{moment(promoCodes?.createdOn).format(READ_ABLE_DATE_FORMAT)} </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem label="Modified On">{moment(promoCodes?.modifiedOn).format(READ_ABLE_DATE_FORMAT)} </FormItem>
                  </Col>

                  <Col xl={16} lg={24} md={24} sm={24}>
                    <Affix offsetBottom={30}>
                      <FormItem {...submitFormLayout}>
                        <Link
                          className="ant-btn btn-custom-primary ant-btn-round ant-btn-lg no-underline  mr-2"
                          to={`/promoCodes/${id}/edit`}
                          style={{ color: 'white' }}
                          shape="round"
                        >
                          {/*<Icon type="edit" /> */}
                          {'Edit'}
                        </Link>

                        <Link
                          to="/promoCodes"
                          className="ant-btn ant-btn-round ant-btn-lg no-underline mt-3"
                        >
                          {'Cancel'}
                        </Link>
                      </FormItem>
                    </Affix>
                  </Col>
                </Row>
              </Form>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default Detail;
