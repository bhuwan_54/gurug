import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Select,
  DatePicker,
  InputNumber,
  Affix,
} from 'antd';
import moment from 'moment';

import Message from '../Common/Message';
import { DATE_FORMAT_VIEW2, DATE_SERVER_FORMAT } from '../../constants/appConfig';
import { HomeOutlined } from '@ant-design/icons';
import history from '../../utils/history';
import { disabledPastDate } from '../../utils/commonUtil';

const FormItem = Form.Item;
const { Option } = Select;

const AddForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const [buttonLoading, setButttonLoading] = useState(false);
  const { promoCodes, loading, errors, addPromoCode, cleanPromoCodeProps } = props;

  const { validateFields, getFieldValue } = form;

  const onFinish = values => {
    values.effectiveFrom = moment(values.effectiveFrom).format(DATE_SERVER_FORMAT);
    values.effectiveTo = moment(values.effectiveTo).format(DATE_SERVER_FORMAT);
    addPromoCode(values);
  };
  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 18 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 18, offset: 0 },
      sm: { span: 18, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  const handleCancel = () => {
    form.resetFields();
    cleanPromoCodeProps();
  };
  const handleBack = () => {
    history.push('/promoCodes');
  };

  useEffect(() => {
    form.setFieldsValue({
      id: id,
    });
    return () => {
      cleanPromoCodeProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Add New Promo Code</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/promoCodes'}>Promo Codes</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Add</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />

          <div className="box box-default">
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    {...formItemLayout}
                    name="updateForm"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                    form={form}
                  >
                    <Row>
                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'name'}
                          label="Name"
                          rules={[{ required: true, message: 'Name is required.' },{max:50,message: 'Please enter less than 50 character.'}]}
                        >
                          <Input />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'code'}
                          label={'Code'}
                          rules={[{ required: true, message: 'Code is required' },{max:20, message: 'Please enter less than 20 character.'}]}
                        >
                          <Input />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'details'}
                          rules={[{ required: true, message: 'Detail is required.' },{max:150 , message: 'Please enter less than 150 character.'}]}
                          label="Details"
                        >
                          <Input.TextArea />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'discountType'}
                          rules={[{ required: true, message: 'Discount type is required.' }]}
                          label="Discount Type"
                        >
                          <Select
                            showSearch
                            placeholder="Select discount type"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            <Option value="FLAT">Flat</Option>
                            <Option value="PERCENTAGE">Percentage</Option>
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'effectiveFrom'}
                          label="Effective From"
                          rules={[{ required: true, message: 'Effective from date is required.' }]}
                        >
                          <DatePicker
                            disabledDate={disabledPastDate}
                            className={'date-with-time'}
                            showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
                            format={DATE_FORMAT_VIEW2} />
                        </FormItem>
                      </Col>
                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'effectiveTo'}
                          label="Effective To"
                          rules={[{ required: true, message: 'Effective to date is required.' },
                            ({ getFieldValue }) => ({
                              validator(rule, value) {
                                if (!value || moment(getFieldValue('effectiveFrom')) < moment(value)) {
                                  return Promise.resolve();
                                }
                                return Promise.reject('Please select the time greater than Effective From.');
                              },
                            }),
                          ]}
                        >
                          <DatePicker
                            disabledDate={disabledPastDate}
                            className={'date-with-time'}
                            showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
                                            format={DATE_FORMAT_VIEW2} />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          label="Discount For Bike"
                          name={'discountAmountBike'}
                          rules={[{ required: true, message: 'Discount for bike is required.' }]}
                        >
                          <InputNumber />
                        </FormItem>
                      </Col>
                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          name={'discountAmountCar'}
                          label="Discount For Car"
                          rules={[{ required: true, message: 'Discount for car is required.' }]}
                        >
                          <InputNumber />
                        </FormItem>
                      </Col>

                      <Col xl={16} lg={24} md={24} sm={24}>
                        <FormItem {...submitFormLayout}>
                          <Button
                            className="btn-custom-primary mr-1"
                            shape="round"
                            htmlType="submit"
                            size={'large'}
                            loading={loading}
                          >
                            Add
                          </Button>

                          <Button
                            className=" mr-1"
                            shape="round"
                            onClick={handleCancel}
                            size={'large'}
                            danger
                          >
                            Cancel
                          </Button>
                          <Button
                            className=" mr-1"
                            shape="round"
                            onClick={handleBack}
                            size={'large'}
                          >
                            Back
                          </Button>
                        </FormItem>
                      </Col>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
  );
};

export default AddForm;
