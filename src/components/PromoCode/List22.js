import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import {
  Row,
  Col,
  Input,
  Table,
  Breadcrumb,
  Card,
  Form,
  Button,
  Affix,
  Skeleton,
  Modal,
  Select,
  DatePicker, InputNumber,
} from 'antd';
import { EditOutlined, HomeOutlined, EyeOutlined } from '@ant-design/icons';
import moment from 'moment';

import { DATE_FORMAT_VIEW, DATE_SERVER_FORMAT, PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';
import Message from '../Common/Message';
import { searchItemLayout, getSortingOrder } from '../../utils/commonUtil';
import ApproveModel from '../Common/Modal/ApproveModal';

const Search = Input.Search;
const FormItem = Form.Item;
const { Option } = Select;

let promo;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [selectedPromo, setSelectedPromo] = useState();
  const [viewModalVisible, setViewModalVisible] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);

  const [form] = Form.useForm();
  const {
    promoCodes,
    fetchPromoCodeWithCriteria,
    errors,
    pagination,
    loading,
    updatePromoCodes,
    updatePromoCodeStatus,
    cleanPromoCodeProps,
  } = props;
  const { validateFields, getFieldValue, getFieldDecorator } = form;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchPromoCodeWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = 1;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchPromoCodeWithCriteria(fieldsValue);
    });
  };

  const handleInitialValue = (promoCodes) => {
    promoCodes instanceof Array && promoCodes.map(value => {
      Object.keys(value).map(key => {
        if (key === 'effectiveFrom' || key === 'effectiveTo') {
          value[key] = value[key] && moment(value[key], DATE_FORMAT_VIEW);
        }
      });
      return value;
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Name',
      dataIndex: 'name',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Code',
      dataIndex: 'code',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Details',
      dataIndex: 'details',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Discount Amount Bike',
      dataIndex: 'discountAmountBike',
      align: 'left',
    },
    {
      title: 'Discount Amount Car',
      dataIndex: 'discountAmountCar',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Effective From',
      dataIndex: 'effectiveFrom',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{moment(record.effectiveFrom).format('YYYY-MM-DD')}</div>;
      },
    },
    {
      title: 'Effective To',
      dataIndex: 'effectiveTo',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{moment(record.effectiveTo).format('YYYY-MM-DD')}</div>;
      },
    },
    {
      title: 'Action',
      key: 'operation',
      align: 'center',
      fixed: 'right',
      render: (text, record) => {
        return (
          <div>
            <Button shape="circle" icon={<EyeOutlined style={{ fontSize: '16px' }}/>} style={{ margin: '5px' }}
                    onClick={() => showViewModal(record.id)}/>
            <Button shape="circle" icon={<EditOutlined style={{ fontSize: '16px' }}/>}
                    onClick={() => showEditModal(record.id)}/>
          </div>
        );
      },
    },
  ];
  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 18 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };

  const showEditModal = (id) => {
    setSelectedPromo(id);
    promo = promoCodes.filter(val => val.id === id)[0];
    promo.effectiveFrom = moment(promo.effectiveFrom);
    promo.effectiveTo = moment(promo.effectiveTo);
    form.setFieldsValue(promo);

    setEditModalVisible(true);
  };

  const hideEditModal = () => {
    if (editModalVisible) {
      setEditModalVisible(false);
      promo = undefined;
      setSelectedPromo();
    }
  };

  const showViewModal = (id) => {
    promo = promoCodes.filter(val => val.id === id)[0];
    setViewModalVisible(true);
  };

  const hideViewModal = () => {
    if (viewModalVisible) {
      setViewModalVisible(false);
      promo = undefined;

    }
  };

  const onFinishEdit = () => {
    // values.id = selectedPromo;
    Object.keys(promo).map(key => {
      if (promo && moment.isMoment(promo[key])) {
        promo[key] = moment(promo[key], DATE_SERVER_FORMAT)
          .startOf('days')
          .format(DATE_SERVER_FORMAT);
      }
    });
    updatePromoCodes(promo);
  };

  const modalProps = {
    apiUrlKey: promo && promo.disabled ? 'activate' : 'disable',
    id: selectedPromo,
  };

  const updateStatus = () => {
    updatePromoCodeStatus(modalProps);
    hideEditModal();
  };

  useEffect(() => {
    fetchPromoCodeWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });

    return () => {
      cleanPromoCodeProps();
    };
  }, []);

  useEffect(() => {
    // fetchPromoCodeWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });

    return () => {
      // cleanPromoCodeProps();
    };
  }, [selectedPromo]);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Promo Codes List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined/>
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Promo Codes</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="search-form mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                        rules={[{ required: true, message: 'Please enter the search value' }]}
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>

                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size="middle"
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={promoCodes instanceof Array ? promoCodes : []}
                      pagination={{
                        total: pagination.totalData,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
      <Modal
        title="View Promo Code"
        visible={viewModalVisible}
        width={'720px'}
        destroyOnClose
        onOk={hideViewModal}
        onCancel={hideViewModal}
        cancelText="Cancel"
      >
        <Form {...formItemLayout} className="gurug-form" layout="horizontal">
          <Row>
            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem label="Name">{promo?.name}</FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem label={'Code'}>{promo?.code} </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem label="Details">{promo?.details} </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem label="Discount Type">{promo?.discountType}</FormItem>
            </Col>

            <Col xl={12} lg={12} md={24} sm={24}>
              <FormItem label="Effective From">{promo?.effectiveFrom} </FormItem>
            </Col>

            <Col xl={12} lg={12} md={24} sm={24}>
              <FormItem label="Effective To">{promo?.effectiveTo} </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem label="Discount Amount Bike">
                {promo?.discountAmountBike}
              </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem label="Discount Amount Car">{promo?.discountAmountCar}</FormItem>
            </Col>
            <Col xl={12} lg={12} md={24} sm={24}>
              <FormItem label="Created On">{promo?.createdOn} </FormItem>
            </Col>

            <Col xl={12} lg={12} md={24} sm={24}>
              <FormItem label="Modified On">{promo?.modifiedOn} </FormItem>
            </Col>
          </Row>
        </Form>

      </Modal>

      <Modal
        title="Edit Promo Code"
        visible={editModalVisible}
        onOk={onFinishEdit}
        width={'720px'}
        destroyOnClose
        onCancel={hideEditModal}
        cancelText="Cancel"
        footer={[
          <Button key="back" onClick={hideEditModal}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" loading={loading} onClick={onFinishEdit}>
            Submit
          </Button>,
        ]}
      >
        <Form
          {...formItemLayout}
          name="updateForm"
          // initialValues={promo && handleInitialValue(promo)}
          onFinish={onFinishEdit}
          className="gurug-form"
          layout="horizontal"
          form={form}
        >
          <Row justify="end" gutter={[8, 16]}>
            <Col xl={5} lg={5} md={12} sm={18} xs={18}>
              <Affix offsetTop={10}>
                <Button type="dashed" shape="round" block onClick={() => updateStatus()}>
                  { selectedPromo ?  promo && promo.disable ? 'Activate' : 'Disable' : ""}
                </Button>
              </Affix>
            </Col>
            {/*{*/}
            {/*  !promoCodes.verified &&  <Col xl={5} lg={5} md={12} sm={18} xs={18}>*/}
            {/*    <Affix offsetTop={10}>*/}
            {/*      <Button type="dashed" shape="round" block onClick={() => ApproveModel(verifyModalProps)}>*/}
            {/*        Verify*/}
            {/*      </Button>*/}
            {/*    </Affix>*/}
            {/*  </Col>*/}
            {/*}*/}
          </Row>

          <Row>
            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                name={'name'}
                label="Name"
                rules={[{ required: true, message: 'Name is required.' }, {
                  max: 50,
                  message: 'Please enter less than 50 character.',
                }]}
              >
                <Input/>
              </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                name={'code'}
                label={'Code'}
                rules={[{ required: true, message: 'Code is required' }, {
                  max: 20,
                  message: 'Please enter less than 20 character.',
                }]}
              >
                <Input/>
              </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                name={'details'}
                rules={[{ required: true, message: 'Detail is required.' }, {
                  max: 150,
                  message: 'Please enter less than 150 character.',
                }]}
                label="Details"
              >
                <Input.TextArea/>
              </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                name={'discountType'}
                rules={[{ required: true, message: 'Discount type is required.' }]}
                label="Discount Type"
              >
                <Select
                  showSearch
                  placeholder="Select Discount type"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  <Option value="FLAT">Flat</Option>
                  <Option value="PERCENTAGE">Percentage</Option>
                </Select>
              </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                name={'effectiveFrom'}
                label="Effective From"
                rules={[{ required: true, message: 'Effective from date is required.' }]}
              >
                <DatePicker format={DATE_FORMAT_VIEW}/>
              </FormItem>
            </Col>
            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem name={'effectiveTo'} label="Effective To">
                <DatePicker format={DATE_FORMAT_VIEW}/>
              </FormItem>
            </Col>

            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                {...formItemLayout}
                label="Discount Amount Bike"
                name={'discountAmountBike'}
                rules={[{ required: true, message: 'Discount amount bike is required.' }]}
              >
                <InputNumber/>
              </FormItem>
            </Col>
            <Col xl={12} lg={12} md={18} sm={24}>
              <FormItem
                {...formItemLayout}
                name={'discountAmountCar'}
                label="Discount Amount Car"
                rules={[{ required: true, message: 'Discount amount car is required.' }]}
              >
                <InputNumber/>
              </FormItem>
            </Col>
          </Row>
        </Form>

      </Modal>
    </div>
  );
};

export default List;
