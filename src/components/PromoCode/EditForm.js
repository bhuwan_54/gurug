import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  DatePicker,
  InputNumber,
  Checkbox,
  Typography,
  Affix,
} from 'antd';
import dayjs from 'dayjs';
import { DownloadOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import { DATE_FORMAT_VIEW, DATE_SERVER_FORMAT } from '../../constants/appConfig';
import { disabledPastDate, isEmpty } from '../../utils/commonUtil';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';
import moment from 'moment';
import history from '../../utils/history';
import ApproveModel from '../Common/Modal/ApproveModal';

const FormItem = Form.Item;
const { Title } = Typography;
const { Option } = Select;


const EditForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const [buttonLoading, setButttonLoading] = useState(false);
  const {
    loading,
    promoCodes,
    errors,
    updatePromoCodes,
    updatePromoCodeStatus,
    fetchPromoCodeByIdentifier,
    cleanPromoCodeProps,
  } = props;

  const { setFieldsValue, setFields, validateFields, getFieldValue } = form;

  const onFinish = values => {
    values.id = id;
    Object.keys(values).map(key => {
      if (values && moment.isMoment(values[key])) {
        values[key] = moment(values[key], DATE_SERVER_FORMAT).format(DATE_SERVER_FORMAT);
      }
    });
    updatePromoCodes(values);
  };
  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 18 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 18, offset: 0 },
      sm: { span: 18, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };
  const modalProps = {
    apiUrlKey: promoCodes && promoCodes.disabled ? 'activate' : 'disable',
    id: id,
  };

  const handleCancel = () => {
    form.resetFields();
    cleanPromoCodeProps();
  };
  const handleBack = () => {
    history.push('/promoCodes');
  };

  useEffect(() => {
   fetchPromoCodeByIdentifier(id);
    return () => {
      cleanPromoCodeProps();
    };
  }, []);

  useEffect(() => {
    form.setFieldsValue({
      name: promoCodes.name,
      code: promoCodes.code,
      details: promoCodes.details,
      discountType: promoCodes.discountType,
      discountAmountBike: promoCodes.discountAmountBike,
      discountAmountCar: promoCodes.discountAmountCar,
      effectiveFrom: promoCodes.effectiveFrom ? moment(promoCodes.effectiveFrom) : null,
      effectiveTo: promoCodes.effectiveTo ? moment(promoCodes.effectiveTo) : null,
      createdOn: promoCodes.createdOn ? moment(promoCodes.createdOn) : null,
      modifiedOn: promoCodes.modifiedOn ? moment(promoCodes.modifiedOn) : null,
    });
    setFieldsValue({promoCodes});
  }, [promoCodes.name]);

  return (
    <Spin spinning={loading} size={'large'} delay={300} tip={'loading...'}>
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Update Promo Code</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/promoCodes'}>Promo Codes</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Update</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />

          <div className="box box-default">
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    {...formItemLayout}
                    name="updateForm"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                    form={form}
                  >
                    <Row justify="end" gutter={[8, 16]}>
                      <Col xl={5} lg={5} md={12} sm={18} xs={18}>
                        <Affix offsetTop={10}>
                          <Button type="dashed" shape="round" block onClick={() => updatePromoCodeStatus(modalProps)}>
                            {promoCodes && promoCodes.disabled ? 'Activate' : 'Disable'}
                          </Button>
                        </Affix>
                      </Col>
                      {/*{*/}
                      {/*  !promoCodes.verified &&  <Col xl={5} lg={5} md={12} sm={18} xs={18}>*/}
                      {/*    <Affix offsetTop={10}>*/}
                      {/*      <Button type="dashed" shape="round" block onClick={() => ApproveModel(verifyModalProps)}>*/}
                      {/*        Verify*/}
                      {/*      </Button>*/}
                      {/*    </Affix>*/}
                      {/*  </Col>*/}
                      {/*}*/}
                    </Row>
                    <Row>
                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'name'}
                          label="Name"
                          rules={[{ required: true, message: 'Name is required.' },{max:50,message: 'Please enter less than 50 character.'}]}
                        >
                          <Input />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'code'}
                          label={'Code'}
                          rules={[{ required: true, message: 'Code is required' },{max:20, message: 'Please enter less than 20 character.'}]}
                        >
                          <Input />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'details'}
                          rules={[{ required: true, message: 'Detail is required.' },{max:150 , message: 'Please enter less than 150 character.'}]}
                          label="Details"
                        >
                          <Input.TextArea />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'discountType'}
                          rules={[{ required: true, message: 'Discount type is required.' }]}
                          label="Discount Type"
                        >
                          <Select
                            showSearch
                            placeholder="Select Discount type"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            <Option value="FLAT">Flat</Option>
                            <Option value="PERCENTAGE">Percentage</Option>
                          </Select>
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          name={'effectiveFrom'}
                          label="Effective From"
                          rules={[{ required: true, message: 'Effective from date is required.' }]}
                        >
                          <DatePicker
                            disabledDate={disabledPastDate}
                            showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
                            format={DATE_FORMAT_VIEW} />
                        </FormItem>
                      </Col>
                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem name={'effectiveTo'}
                                  rules={[{ required: true, message: 'Effective to date is required.' }]}
                                  label="Effective To">
                          <DatePicker
                            disabledDate={disabledPastDate}
                            showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
                            format={DATE_FORMAT_VIEW} />
                        </FormItem>
                      </Col>

                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          label="Discount Amount Bike"
                          name={'discountAmountBike'}
                          rules={[{ required: true, message: 'Discount amount bike is required.' }]}
                        >
                          <InputNumber />
                        </FormItem>
                      </Col>
                      <Col xl={12} lg={12} md={18} sm={24}>
                        <FormItem
                          {...formItemLayout}
                          name={'discountAmountCar'}
                          label="Discount Amount Car"
                          rules={[{ required: true, message: 'Discount amount car is required.' }]}
                        >
                          <InputNumber />
                        </FormItem>
                      </Col>

                      <Col xl={16} lg={24} md={24} sm={24}>
                        <Affix offsetBottom={30}>
                          <FormItem {...submitFormLayout}>
                            <Button
                              className="btn-custom-primary mr-1"
                              shape="round"
                              htmlType="submit"
                              size={'large'}
                              loading={loading}
                            >
                              Update
                            </Button>

                            <Button
                              className=" mr-1"
                              shape="round"
                              onClick={handleCancel}
                              size={'large'}
                              danger
                            >
                              Cancel
                            </Button>
                            <Button
                              className=" mr-1"
                              shape="round"
                              onClick={handleBack}
                              size={'large'}
                            >
                              Back
                            </Button>
                          </FormItem>
                        </Affix>
                      </Col>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
    </Spin>
  );
};

export default EditForm;
