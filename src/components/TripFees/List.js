import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form, Tag, Button, Radio } from 'antd';
import { EyeOutlined, HomeOutlined, EditOutlined } from '@ant-design/icons';
import moment from 'moment';

import { PAGE_SIZE, colorStatus, PAGE_NUMBER } from '../../constants/appConfig';
import Message from '../Common/Message';
import { searchItemLayout, getSortingOrder } from '../../utils/commonUtil';

const Search = Input.Search;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [reviewOf, setReviewOf] = useState('bike');

  const [form] = Form.useForm();

  const {  fetchTripFeeWithCriteria, errors, pagination,tripFees, loading, cleanTripFeeProps } = props;
  const { validateFields } = form;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchTripFeeWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      rideType: reviewOf,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = PAGE_NUMBER ;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchTripFeeWithCriteria(fieldsValue);
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      align:'center',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Base Fare',
      dataIndex: 'baseFare',
      align: 'left',
      render: (text, record) => {
        return <div>{`Rs ${record.baseFare}`}</div>;
      },
    },
    {
      title: 'Per Kilometer',
      dataIndex: 'perKilometer',
      align: 'left',
      render: (text, record) => {
        return <div>{`Rs ${record.perKilometer}`}</div>;
      },
    },
    {
      title: 'Per Minute',
      dataIndex: 'perMinute',
      align: 'left',
      render: (text, record) => {
        return <div>{`Rs ${record.perMinute}`}</div>;
      },
    },
    {
      title: 'Minimum Fare',
      dataIndex: 'minimumFare',
      align: 'left',
      render: (text, record) => {
        return <div>{`Rs ${record.minimumFare}`}</div>;
      },
    },

    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{record.createdOn && moment(record.createdOn).format('YYYY-MM-DD')}</div>;
      },
    },
    {
      title: 'Modified On',
      dataIndex: 'modifiedOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{record.modifiedOn && moment(record.modifiedOn).format('YYYY-MM-DD')}</div>;
      },    },
    // {
    //   title: 'Action',
    //   key: 'operation',
    //   render: (text, record) => {
    //     return (
    //       <div>
    //       <Link to={`/drivers/${record.id}/detail`} title="View">
    //         <Button shape="circle"  icon={<EyeOutlined  style={{ fontSize: '16px' }}/>}  style={{margin:'5px'}}/>
    //       </Link>
    //         <Link to={`/drivers/${record.id}/edit`} title="Edit">
    //           <Button shape="circle" icon={<EditOutlined  style={{ fontSize: '16px' }}/>} />
    //         </Link>
    //         </div>
    //     );
    //   },
    // },
  ];

  const handleModeChange = e => {
    const mode = e.target.value;
    setReviewOf(mode);
    fetchTripFeeWithCriteria({pageNumber:PAGE_NUMBER, pageSize: PAGE_SIZE, rideType: mode})
  };

  useEffect(() => {
    fetchTripFeeWithCriteria({ pageSize: PAGE_SIZE, pageNumber: PAGE_NUMBER, rideType:'bike' });
    return () => {
      cleanTripFeeProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Trip Fee List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Trip Fees</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <div>
                <Row>
                  <Col>
                    <Radio.Group onChange={handleModeChange} value={reviewOf} style={{ marginBottom: 8 }} >
                      <Radio.Button value="bike">Bike</Radio.Button>
                      <Radio.Button value="car">Car</Radio.Button>
                    </Radio.Group>
                  </Col>

                  <Col {...searchItemLayout} className="search-form mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      size="middle"
                      bordered
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={[tripFees] instanceof Array ? [tripFees] : []}
                      pagination={{
                        total: pagination.totalData,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
