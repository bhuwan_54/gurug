import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Breadcrumb, Card, Skeleton, Divider, Typography, Affix } from 'antd';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';

const FormItem = Form.Item;
const { Title } = Typography;

// const drivers = {
//   id: 1,
//   fullName: 'Ashish Shrestha',
//   mobileNumber: '9808997878',
//   emailId: 'email@email.com',
//   city: 'Kathmandu',
//   streetNumber: '123',
//   vehicleType: 'Bike',
//   vehicleCC: 250,
//   vehicleBrandModel: 'ABC',
//   vehicleLicensePlate: 'BA 2 PA 9876',
//   expiryDate: null,
//   issueDate: '2020-02-20',
//   identityFrontPhoto: 'driver/1DRIVER_IDENTITY_FRONT',
//   identityBackPhoto: 'driver/1DRIVER_IDENTITY_BACK',
//   photoPerson: 'driver/1DRIVER_PHOTO',
//   photoVehicleFront: 'driver/1DRIVER_VEHICLE_FRONT',
//   photoVehicleBack: 'driver/1DRIVER_VEHICLE_BACK',
//   photoVehicleSide: 'driver/1DRIVER_VEHICLE_SIDE',
//   deviceUid: '123456',
//   deviceMac: '123456',
//   createdBy: 'admin@gmail.com',
//   updatedBy: null,
//   verifiedBy: null,
//   active: true,
//   verified: true,
// };

const Detail = props => {
  const { id } = useParams();

  const { drivers, errors, loading, fetchDriverByIdentifier, cleanDriverProps } = props;

  const driverIdFrontPhotoSource = drivers ? drivers.identityFrontPhoto : '';
  const driverIdBackPhotoSource = drivers ? drivers.identityBackPhoto : '';
  const driverPhotoSource = drivers ? drivers.photoVehicleBack : '';

  const vehicleFrontPhotoSource = drivers ? drivers.photoVehicleFront : '';
  const vehicleBackPhotoSource = drivers ? drivers.photoVehicleBack : '';
  const vehicleSidePhotoSource = drivers ? drivers.photoVehicleSide : '';

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 8 },
      sm: { span: 12, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  useEffect(() => {
    fetchDriverByIdentifier(id);
    return () => {
      cleanDriverProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">{'Driver View'}</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined/>
                  <Link to={'/dashboard'}>{'Dashboard'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/drivers'}>{'Drivers'}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{'View'}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors}/>
          <Card className="detail-view">
            <Skeleton loading={false} active>
              <Form>
                <Row>
                  <Title level={4}>Personal Information</Title>
                  <Divider/>
                  <Row>
                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Name">
                        {drivers ? drivers.fullName : ''}
                      </FormItem>
                    </Col>

                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label={'Email ID'}>
                        {drivers ? drivers.emailId : ''}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Mobile Number">
                        {drivers ? drivers.mobileNumber : ''}
                      </FormItem>
                    </Col>

                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Date of Birth">
                        {drivers ? drivers.dob : ''}
                      </FormItem>
                    </Col>
                  </Row>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="Person Photo">
                      <div>
                        <img
                          src={`${driverPhotoSource}?${Date.now()}`}
                          className="img-thumbnail m-1"
                          alt="Vechile"
                        />
                      </div>
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem {...formItemLayout} label="Licence Number">
                      {drivers ? drivers.licenceNumber : ''}
                    </FormItem>
                  </Col>

                  <Row>
                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Street Number">
                        {drivers ? drivers.streetNumber : ''}
                      </FormItem>
                    </Col>

                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="City">
                        {drivers ? drivers.city : ''}
                      </FormItem>
                    </Col>
                  </Row>

                  <Title level={4}>Vehicle Information</Title>
                  <Divider/>
                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle Type">
                      {drivers ? drivers.vehicleType : ''}
                    </FormItem>
                  </Col>

                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle CC">
                      {drivers ? drivers.vehicleCC : ''}
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle Brand Model">
                      {drivers ? drivers.vehicleBrandModel : ''}
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={18} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle License Plate">
                      {drivers ? drivers.vehicleLicensePlate : ''}
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle Front Photo">
                      <div>
                        <img
                          src={`${vehicleFrontPhotoSource}?${Date.now()}`}
                          className="img-thumbnail m-1"
                          alt="Vechile"
                        />
                      </div>
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle Side Photo">
                      <div>
                        <img
                          src={`${vehicleSidePhotoSource}?${Date.now()}`}
                          className="img-thumbnail m-1"
                          alt="Vechile"
                        />
                      </div>
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="Vehicle Back Photo">
                      <div>
                        <img
                          src={`${vehicleBackPhotoSource}?${Date.now()}`}
                          className="img-thumbnail m-1"
                          alt="Vechile"
                        />
                      </div>
                    </FormItem>
                  </Col>

                  <Title level={4}>Identification</Title>
                  <Divider/>
                  <Row>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem {...formItemLayout} label="Issue Date">
                        {drivers ? drivers.issueDate : ''}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem {...formItemLayout} label="Expiry Date">
                        {drivers ? drivers.expiryDate : ''}
                      </FormItem>
                    </Col>
                  </Row>

                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="ID Front Photo">
                      <div>
                        <img
                          src={`${driverIdFrontPhotoSource}?${Date.now()}`}
                          className="img-thumbnail m-1"
                          alt="Vechile"
                        />
                      </div>
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="ID Back Photo">
                      <div>
                        <img
                          src={`${driverIdBackPhotoSource}?${Date.now()}`}
                          className="img-thumbnail m-1"
                          alt="Vechile"
                        />
                      </div>
                    </FormItem>
                  </Col>

                  <Title level={4}>Others</Title>
                  <Divider/>
                  <Row>
                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Device UID">
                        {drivers ? drivers.deviceUid : ''}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Device MAC">
                        {drivers ? drivers.deviceMac : ''}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col xl={12} lg={12} md={18} sm={24}>
                      <FormItem {...formItemLayout} label="Created By">
                        {drivers ? drivers.createdBy : ''}
                      </FormItem>
                    </Col>
                    <Col xl={12} lg={12} md={24} sm={24}>
                      <FormItem {...formItemLayout} label="Updated By">
                        {drivers ? drivers.updatedBy : ''}
                      </FormItem>
                    </Col>
                  </Row>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="Status">
                      {drivers.active}
                    </FormItem>
                  </Col>
                  <Col xl={12} lg={12} md={24} sm={24}>
                    <FormItem {...formItemLayout} label="Verified">
                      {drivers.verified}
                    </FormItem>
                  </Col>

                  <Col xl={16} lg={24} md={24} sm={24}>
                    <Affix offsetBottom={30}>
                      <FormItem {...submitFormLayout} className="mt-3 ">
                        <Link
                          className="ant-btn btn-custom-primary ant-btn-round ant-btn-lg no-underline  mr-2"
                          to={`/drivers/${id}/edit`}
                          style={{ color: 'white' }}
                          shape="round"
                        >
                          {/*<Icon type="edit" /> */}
                          {'Edit'}
                        </Link>

                        <Link
                          to="/drivers"
                          className="ant-btn ant-btn-round ant-btn-lg no-underline"
                        >
                          {'Back'}
                        </Link>
                      </FormItem>
                    </Affix>
                  </Col>
                </Row>
              </Form>
            </Skeleton>
          </Card>
        </article>
      </QueueAnim>
    </div>
  );
};

export default Detail;
