import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form, Button, Modal } from 'antd';
import { searchItemLayout } from '../../utils/commonUtil';
import { EyeOutlined, HomeOutlined, DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons';

import { PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';
import Message from '../Common/Message';
import DeleteModel from '../Common/Modal/DeleteModal';
import moment from 'moment';

const Search = Input.Search;

const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [form] = Form.useForm();
  const { validateFields, getFieldVale } = form;

  const {fetchGeofencingWithCriteria,  errors,  pagination, loading, geoFencings, deleteGeofenceById, cleanGeofencingProps } = props;

  const locale = {
    emptyText: 'No data',
  };
  const handleTableChange = (pagination, filters, sorter) => {
    console.log(pagination,'pagination')
    fetchGeofencingWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: sorter.order,
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortOrder: sorter.order,
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber = PAGE_NUMBER ;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;
      fetchGeofencingWithCriteria(fieldsValue);
    });
  };

  const handleDelete = async id  => {
    await deleteGeofenceById(id).then(response => {
      if(response.status === 200){
        fetchGeofencingWithCriteria({pageSize: PAGE_SIZE ,pageNumber: PAGE_NUMBER})
      }
    })
  };

  const DeleteModel = props => {
    Modal.confirm({
      title: props.title || 'Are you sure want to delete?',
      width: props.width || 320,
      icon: <ExclamationCircleOutlined/>,
      cancelText: 'No',
      okText: 'Yes',
      onOk() {
        handleDelete(props.id)
      },
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      align:'center',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Location',
      dataIndex: 'location',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.location}`}</div>;
      },
    },
    {
      title: 'Latitude',
      dataIndex: 'lat',
      align: 'left',
      render: (text, record) => {
        return <div>{`${record ? record.lat :''}`}</div>;
      },
    },
    {
      title: 'Longitude',
      dataIndex: 'lng',
      align: 'left',
      render: (text, record) => {
        return <div>{`${record ? record.lng :''}`}</div>;
      },
    },
    {
      title: 'Radius',
      dataIndex: 'radius',
      align: 'left',
      render: (text, record) => {
        return <span><span>{`${record ? record.radius :''}`}</span> <span style={{fontSize:'11px'}}>(K.M)</span> </span>;
      },
    },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <span>{`${record ? moment(record.createdOn).format('DD MMM YYYY HH:mm:ss') :''}`}</span>;
      },
    },
    {
      title: 'Modified On',
      dataIndex: 'modifiedOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <span>{`${record ? moment(record.modifiedOn).format('DD MMM YYYY HH:mm:ss') :''}`}</span>;
      },
    },
    {
      title: 'Action',
      key: 'operation',
      align:'center',
      render: (text, record) => {
        return (
          <div>
          <Link to={`/geofencings/${record.id}/detail`} title="View">
            <Button shape="circle"  icon={<EyeOutlined  style={{ fontSize: '16px' }}/>}  style={{margin:'5px'}}/>
          </Link>
          <Button shape="circle" onClick={() => DeleteModel({id:record.id, deleteEntity: deleteGeofenceById, fetchEntity:fetchGeofencingWithCriteria , data:{ pageSize: PAGE_SIZE ,pageNumber: PAGE_NUMBER}})} icon={<DeleteOutlined style={{ fontSize: '16px' }}/>}/>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    fetchGeofencingWithCriteria({ pageSize: PAGE_SIZE ,pageNumber: PAGE_NUMBER});
    return () => {
      cleanGeofencingProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">GeoFencings List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>GeoFencings</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <div>
                <Row>
                  <Col {...searchItemLayout} className="mb-2">
                    <Form name={'search-form'} form={form}>
                      <Form.Item
                        name="searchParameter"
                      >
                        <Search
                          placeholder="Please enter value"
                          enterButton="Search"
                          onSearch={onFinish}
                        />
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      columns={columns}
                      bordered
                      size={'middle'}
                      rowKey={record => record.id}
                      dataSource={geoFencings}
                      pagination={{
                        total: pagination.totalData,
                        showSizeChanger: true,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
