import React, { Fragment, useEffect, useState, useRef, useCallback } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link } from 'react-router-dom';
import {
  Row,
  Col,
  Breadcrumb,
  Card,
  Slider,
  Descriptions,
  Divider,
  Button,
  Form,
  Input,
  Select, message,
} from 'antd';
import { GoogleMap, useLoadScript, Circle } from '@react-google-maps/api';
import { HomeOutlined } from '@ant-design/icons';

import Message from '../Common/Message';
import { isEmpty } from '../../utils/commonUtil';
import history from '../../utils/history';

const FormItem = Form.Item;

// {"center":{"lat":2.222682,"lng":2.222682},"radius":100,"id":4,"location":"kathmandu-2"}

const Geofencing = props => {
  const { geoFencings, errors, loading, addGeoFencing, cleanGeofencingProps } = props;

  const polygonRef = useRef(null);
  const listenersRef = useRef([]);

  const [property, setProperty] = useState({
    /**
     * Center of the circle,
     */
    center: null,
    /**
     * Radius of the circle,
     * stored in meters
     * @type {Number}
     */
    radius: null,
  });

  const [hiddenForm, setHiddenForm] = useState(false);

  const [form] = Form.useForm();
  const { setFieldsValue, resetFields } = form;

  const formItemLayout = {
    labelCol: {
      xl: { span: 4 },
      lg: { span: 4 },
      md: { span: 24 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 8 },
      lg: { span: 8 },
      md: { span: 24 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };

  const radiusLayout = {
    labelCol: {
      xl: { span: 24 },
      lg: { span: 24 },
      md: { span: 24 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 24 },
      lg: { span: 24 },
      md: { span: 24 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
  };

  /**
   * Handle radius slider
   *
   * @param {Object} value - It has the new slider value
   */
  const handleSlide = value => {
    let { center, radius } = property;

    // Convert radius in meters
    radius = value * 1000;
    setProperty({ radius: radius, center:property.center });
    setFieldsValue({
      [`radius`]: radius,
    });
  };

  /**
   * Handle map clicking
   *
   * @param  event
   */
  const handleMapClick = event => {
    let { center, radius } = property;

    // Set center and radius
    center = { lat: event.latLng.lat(), lng: event.latLng.lng() };

    // 100 km in meters
    radius = radius ? radius : 20000;

    setProperty({
      center: center, radius: radius
    });
    resetFields([`radius`]);
  };

  /**
   * Handle drag end change
   *
   * @param  event
   */
  const handleCircleDragEnd = event => {
    let { center, radius } = property;

    // Set center and radius
    center = { lat: event.latLng.lat(), lng: event.latLng.lng() };

    setProperty({
      center: center, radius: radius
    });
  };

  /**
   * Is the circle already radius and center?
   *
   * @returns {boolean}
   */
  const hasCircle = () => {
    const { center, radius } = property;
    return radius !== null && center !== null;
  };

  const { center, radius } = property;
  const radiusKM = radius && radius / 1000;
  const marks = {
    0: '0.5 Km',
    100: '100Km',
  };
  const circleLatitude = hasCircle() && center ? center.lat : 'N/A';
  const circleLongitude = hasCircle() && center ? center.lng : 'N/A';
  const isSliderEnabled = !hasCircle();

  const regionOptions = { fillOpacity: 0.1, strokeWidth: 1, strokeOpacity: 0.2 };

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: `${process.env.REACT_APP_GOOGLE_API_KEY}`,
    preventGoogleFontsLoading: true,
  });

  const onFinish = (values) => {
    let formData = property;
    formData['location'] = values.location;
    property.radius === null && property.center === null ?
      (message.error('Please select on map to set geofencings')) : addGeoFencing(formData)
  };

  const handleBack = () => {
    history.push('/geofencings');
  };

  useEffect(() => {
    form.setFieldsValue({
      radius: property.radius,
      center: property.center,
    });
  }, [property]);

  useEffect(() => {
    return () => {
      cleanGeofencingProps();
      setProperty({
        center: null,
        radius: null,
      })
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} sm={24} xs={24}>
                <h4 className="article-title">{'Geofencing'}</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined/>
                    <Link to={'/dashboard'}>{'Dashboard'}</Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    {' '}
                    <Link to={'/geofencings'}>{'Geofencing'}</Link>
                  </Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Row gutter={24}>
              <Col span={24}>
                <Card className={'geofencing-card'}>
                  <Descriptions layout="horizontal">
                    <Descriptions.Item label={'Radius'}>
                      {' '}
                      <strong>
                        {' '}
                        {radiusKM && radiusKM.toFixed(2)}
                        {''}
                        {'KM'}
                      </strong>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Laltitude'}>
                      <strong> {circleLatitude}</strong>
                    </Descriptions.Item>
                    <Descriptions.Item label={'Longitude'}>
                      <strong> {circleLongitude}</strong>
                    </Descriptions.Item>
                  </Descriptions>
                  <Col span={16} offset={4}>
                    <Slider
                      value={parseInt(radiusKM)}
                      marks={marks}
                      step={0.5}
                      max={100}
                      min={0.5}
                      onChange={handleSlide}
                      disabled={isSliderEnabled}
                    />
                  </Col>
                  <Divider dashed={true} style={{ margin: '7px 0' }}/>
                  {isLoaded && (
                    <GoogleMap
                      mapContainerStyle={{
                        height: '70vh',
                        width: '100%',
                      }}
                      id="geofencing-map"
                      center={{ lat: 27.702477, lng: 85.31531 }}
                      zoom={10}
                      onClick={handleMapClick}
                    >
                      <Circle
                        draggable={true}
                        center={center}
                        radius={radius}
                        options={regionOptions}
                        onDragEnd={handleCircleDragEnd}
                      />
                    </GoogleMap>
                  )}
                  <Form onFinish={onFinish} form={form} className="driver-geofencing-form">
                    <FormItem
                      style={{ marginTop: '20px' }}
                      {...formItemLayout}
                      name={'location'}
                      label={'Location'}
                      rules={[
                        {
                          required: true,
                          message: 'Location is required',
                        },
                      ]}
                    >
                      <Input placeholder={'location'}/>
                    </FormItem>
                    {hiddenForm && (
                      <FormItem
                        {...radiusLayout}
                        name={'radius'}
                        Label={'Radius'}
                      >
                        <Input type="hidden"/>
                      </FormItem>
                    )}
                    {hiddenForm && (
                      <FormItem
                        {...radiusLayout}
                        name={'id'}
                      >
                        <Input type="hidden"/>
                      </FormItem>
                    )}
                    {hiddenForm && (
                      <FormItem name={'center'}>
                        <Input type="hidden"/>
                      </FormItem>
                    )}
                    <div className={'mt-2 ml-5'}>
                      <Button
                        htmlType="submit"
                        loading={loading}
                        className="btn-custom-primary mr-1"
                        shape="round"
                        size={'large'}
                      >
                        Add
                      </Button>

                      <Button className=" mr-1" shape="round" onClick={handleBack} size={'large'}>
                        Back
                      </Button>
                    </div>
                  </Form>
                </Card>
              </Col>
            </Row>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default Geofencing;
