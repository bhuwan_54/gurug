import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  DatePicker,
  InputNumber,
  Checkbox,
  Typography,
  Affix,
} from 'antd';

import Message from '../Common/Message';
import { DATE_FORMAT_VIEW } from '../../constants/appConfig';
import { isEmpty } from '../../utils/commonUtil';
import ImageUploadManually from '../Common/ImageUpload/ImageUploadManually';
import { HomeOutlined } from '@ant-design/icons';

const FormItem = Form.Item;
const { Title } = Typography;
const { Option } = Select;

const customers = {
  id: 1,
  fullName: 'Ashish Shrestha',
  mobileNumber: '9808997878',
  emailId: 'email@email.com',
  city: 'Kathmandu',
  streetNumber: '123',
  vehicleType: 'Bike',
  dob: '',
  vehicleCC: 250,
  vehicleBrandModel: 'ABC',
  vehicleLicensePlate: 'BA 2 PA 9876',
  expiryDate: null,
  issueDate: '2020-02-20',
  identityFrontPhoto: 'Customer/1DRIVER_IDENTITY_FRONT',
  identityBackPhoto: 'driver/1DRIVER_IDENTITY_BACK',
  photoPerson: 'driver/1DRIVER_PHOTO',
  photoVehicleFront: 'driver/1DRIVER_VEHICLE_FRONT',
  photoVehicleBack: 'driver/1DRIVER_VEHICLE_BACK',
  photoVehicleSide: 'driver/1DRIVER_VEHICLE_SIDE',
  deviceUid: '123456',
  deviceMac: '123456',
  createdBy: 'admin@gmail.com',
  updatedBy: null,
  verifiedBy: null,
  active: true,
  verified: true,
};

const AddForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();

  const { loading, errors, fetchCustomerByIdentifier, updateCustomer, cleanCustomerProps } = props;

  const { validateFields, getFieldValue } = form;

  const onFinish = values => {
    updateCustomer(values);
  };
  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 4 },
      sm: { span: 12, offset: 4 },
      xs: { span: 24, offset: 0 },
    },
  };

  const driverPhotoPreview = getFieldValue('photoPerson');

  const driverIDFrontPhotoPreview = getFieldValue('identityFrontPhoto');
  const driverIDBackPhotoPreview = getFieldValue('identityBackPhoto');

  const driverPhotoSource = customers ? customers.photoPerson : '';
  const driverIdFrontPhotoSource = customers ? customers.identityFrontPhoto : '';
  const driverIdBackPhotoSource = customers ? customers.identityBackPhoto : '';

  useEffect(() => {
    form.setFieldsValue({
      id: id,
    });
    fetchCustomerByIdentifier(id);
    return () => {
      cleanCustomerProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <article className="article" id="components-form-demo-advanced-search">
          <Row type="flex" justify="space-between">
            <Col xl={16} lg={16} md={24} xs={24} sm={24}>
              <h4 className="article-title">Add Customers</h4>
            </Col>
            <Col>
              <Breadcrumb separator="/">
                <Breadcrumb.Item>
                  {' '}
                  <HomeOutlined />
                  Dashboard
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {' '}
                  <Link to={'/customers'}>Customers</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Update</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>

          <Message error={errors} />

          <div className="box box-default">
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    {...formItemLayout}
                    name="updateForm"
                    initialValues={customers}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    className="gurug-form"
                    layout="horizontal"
                    form={form}
                  >
                    <Row gutter={18}>
                      <Row>
                        <Title level={4}>Personal Information</Title>
                        <Divider />
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'fullName'}
                              label="Full Name"
                              rules={[{ required: true, message: 'Full name is required.' }]}
                            >
                              <Input />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'emailId'}
                              label={'Email'}
                              rules={[
                                { required: true, message: 'Email is required' },
                                {
                                  type: 'email',
                                  message: 'Please enter valid email address.',
                                },
                              ]}
                            >
                              <Input />
                            </FormItem>
                          </Col>
                        </Row>
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'mobileNumber'}
                              rules={[{ required: true, message: 'Mobile number is required.' }]}
                              label="Mobile Number"
                            >
                              <Input />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'dob'}
                              label="Date of Birth"
                              rules={[{ required: true, message: 'Date of birth is required.' }]}
                            >
                              <DatePicker format={DATE_FORMAT_VIEW} />
                            </FormItem>
                          </Col>
                        </Row>

                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'streetNumber'}
                              label="Street Number"
                              rules={[{ required: true, message: 'Street number is required.' }]}
                            >
                              <InputNumber />
                            </FormItem>
                          </Col>

                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              name={'city'}
                              label="City"
                              rules={[{ required: true, message: 'City is required.' }]}
                            >
                              <Input />
                            </FormItem>
                          </Col>
                        </Row>
                        <Row>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              name={'personPhoto'}
                              rules={[
                                {
                                  required: isEmpty(customers),
                                  message: 'Person photo is required',
                                },
                              ]}
                              label="Person Photo"
                            >
                              <ImageUploadManually
                                {...props}
                                fileName="personPhoto"
                                acceptType="image/jpeg,image/png"
                                placeholder="ID front photo"
                                fileType="picture"
                                sizeOfFile="512000"
                              />

                              {driverPhotoPreview !== undefined && (
                                <div>
                                  <img
                                    src={`${driverPhotoSource}?${Date.now()}`}
                                    className="thumbnail"
                                    alt="Person photo"
                                  />
                                </div>
                              )}
                            </FormItem>
                          </Col>
                        </Row>

                        <Title level={4}>Vehicle Information</Title>
                        <Divider />
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'vehicleType'}
                            rules={[{ required: true, message: 'Vehicle type is required.' }]}
                            label="Vehicle Type"
                          >
                            <Select
                              showSearch
                              placeholder="Select vehicle type"
                              optionFilterProp="children"
                              filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
                                0
                              }
                            >
                              <Option value="Bike">Bike</Option>
                              <Option value="Taxi">Taxi</Option>
                            </Select>
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'vehicleCC'}
                            label="Vehicle CC"
                            rules={[{ required: true, message: 'Vehicle CC is required.' }]}
                          >
                            <InputNumber />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'vehicleBrandModel'}
                            label="Vehicle Brand Model"
                            rules={[{ required: true, message: 'Vehicle brand is required.' }]}
                          >
                            <Input />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem name={'vehicleLicensePlate'} label="Vehicle License Plate">
                            <Input />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem name={'photoVehicleFront'} label="Vehicle Front Photo">
                            <ImageUploadManually
                              {...props}
                              fileName="photoVehicleFront"
                              acceptType="image/jpeg,image/png"
                              placeholder="Vehicle front photo"
                              fileType="picture"
                              sizeOfFile="512000"
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${driverPhotoSource}?${Date.now()}`}
                                  className="thumbnail"
                                  alt="Person photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem name={'photoVehicleBack'} label="Vehicle Back Photo">
                            <ImageUploadManually
                              {...props}
                              fileName="photoVehicleBack"
                              acceptType="image/jpeg,image/png"
                              placeholder="Vehicle back photo"
                              fileType="picture"
                              sizeOfFile="512000"
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${driverPhotoSource}?${Date.now()}`}
                                  className="thumbnail"
                                  alt="Person photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Title level={4}>Identification </Title>
                        <Divider />
                        <Row>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              name={'issueDate'}
                              label="Issue Date"
                              rules={[{ required: true, message: 'ID issue date is required.' }]}
                            >
                              <DatePicker format={DATE_FORMAT_VIEW} />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem name={'expiryDate'} label="Expiry Date">
                              <DatePicker format={DATE_FORMAT_VIEW} />
                            </FormItem>
                          </Col>
                        </Row>

                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            label="ID Front Photo"
                            name={'identityFrontPhoto'}
                            rules={[
                              {
                                required: isEmpty(drivers),
                                message: 'Id front photo is required',
                              },
                            ]}
                          >
                            <ImageUploadManually
                              {...props}
                              fileName="identityFrontPhoto"
                              acceptType="image/jpeg,image/png"
                              placeholder="ID front photo"
                              fileType="picture"
                              sizeOfFile="512000"
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${driverPhotoSource}?${Date.now()}`}
                                  className="thumbnail"
                                  alt="Person photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            name={'identityBackPhoto'}
                            label="ID Back Photo"
                            rules={[
                              {
                                required: isEmpty(drivers),
                                message: 'Id back photo is required',
                              },
                            ]}
                          >
                            <ImageUploadManually
                              {...props}
                              fileName="identityBackPhoto"
                              acceptType="image/jpeg,image/png"
                              placeholder="ID back photo"
                              fileType="picture"
                              sizeOfFile="512000"
                            />
                            {driverPhotoPreview !== undefined && (
                              <div>
                                <img
                                  src={`${driverPhotoSource}?${Date.now()}`}
                                  className="thumbnail"
                                  alt="Person photo"
                                />
                              </div>
                            )}
                          </FormItem>
                        </Col>
                        <Title level={4}>Others </Title>
                        <Divider />
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              {...formItemLayout}
                              label="Device UID"
                              name={'deviceUid'}
                              rules={[{ required: true, message: 'Device UID is required.' }]}
                            >
                              <InputNumber />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              {...formItemLayout}
                              name={'deviceMac'}
                              label="Device MAc"
                              rules={[{ required: true, message: 'Device MAC is required.' }]}
                            >
                              <InputNumber />
                            </FormItem>
                          </Col>
                        </Row>
                        <Row>
                          <Col xl={12} lg={12} md={18} sm={24}>
                            <FormItem
                              {...formItemLayout}
                              name={'createdBy'}
                              rules={[{ required: true, message: 'Created by is required.' }]}
                              label="Created By"
                            >
                              <Input />
                            </FormItem>
                          </Col>
                          <Col xl={12} lg={12} md={24} sm={24}>
                            <FormItem
                              {...formItemLayout}
                              label="Updated By"
                              name={'updatedBy'}
                              rules={[{ required: true, message: 'Updated by is required.' }]}
                            >
                              <Input />
                            </FormItem>
                          </Col>
                        </Row>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            {...formItemLayout}
                            label="Status"
                            name={'active'}
                            valuePropName="checked"
                          >
                            <Checkbox />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem
                            {...formItemLayout}
                            name={'verified'}
                            label="Verified"
                            rules={[{ required: true, message: 'Verified is required.' }]}
                          >
                            <Input />
                          </FormItem>
                        </Col>

                        <Col xl={16} lg={24} md={24} sm={24}>
                          <Affix offsetBottom={30}>
                            <FormItem {...submitFormLayout}>
                              <Button
                                className="btn-custom-primary mr-1"
                                shape="round"
                                htmlType="submit"
                                size={'large'}
                              >
                                Update
                              </Button>

                              <Link
                                to="/customers"
                                className="ant-btn ant-btn-round ant-btn-lg no-underline mt-3"
                              >
                                {'Cancel'}
                              </Link>
                            </FormItem>
                          </Affix>
                        </Col>
                      </Row>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>
          </div>
        </article>
      </QueueAnim>
    </div>
  );
};

export default AddForm;
