import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Card, Col, Form, Input, Row, Table, Tag } from 'antd';
import { EyeOutlined, HomeOutlined, EditOutlined } from '@ant-design/icons';
import { PAGE_SIZE, PAGE_NUMBER, DATE_FORMAT, READ_ABLE_DATE_FORMAT } from '../../constants/appConfig';

import Message from '../Common/Message';
import { getSortingOrder, searchItemLayout } from '../../utils/commonUtil';
import moment from 'moment';

const Search = Input.Search;

const List = props => {
  const [form] = Form.useForm();

  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const { validateFields } = form;

  const {
    fetchCustomerWithCriteria,
    customers,
    errors,
    pagination,
    loading,
    cleanCustomerProps,
  } = props;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchCustomerWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.current || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      fieldsValue.pageNumber =  PAGE_NUMBER;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;

      fetchCustomerWithCriteria(fieldsValue);
    });
  };

 const colorStatusCustomer = {
   OTP_PENDING:'#1890ff',
   KYC_PENDING:'#ffc53d',
   REGISTERED:'#66BB6A'
  }
  const columns = [
    {
      title: 'S.N',
      key: 'index',
      align: 'center',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Fullname',
      dataIndex: 'fullName',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.fullName}`}</div>;
      },
    },
    {
      title: 'Mobile Number',
      dataIndex: 'mobileNumber',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Email ID',
      dataIndex: 'emailId',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      render: status => (
        <span>
            <Tag color={colorStatusCustomer[`${status}`]}>
              {status}
            </Tag>
      </span>
      ),
    },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div style={{fontSize:'13px'}}>{moment(record.createdOn).format(READ_ABLE_DATE_FORMAT)}
        </div>;
      },
    },
    {
      title: 'Modified On',
      dataIndex: 'modifiedOn',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div style={{fontSize:'13px'}}>{moment(record.modifiedOn).format(READ_ABLE_DATE_FORMAT)} <br/>
        </div>;
      },
    },
    {
      title: 'Action',
      key: 'operation',
      align:'center',
      render: (text, record) => {
        return (
          <div>
            <Link to={`/customers/${record.id}/detail`} title="View">
              <Button shape="circle" icon={<EyeOutlined style={{ fontSize: '16px' }}/>} style={{ margin: '5px' }}/>
            </Link>
            <Link to={`/customers/${record.id}/edit`} title="Edit">
              <Button shape="circle" icon={<EditOutlined style={{ fontSize: '16px' }}/>}/>
            </Link>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    fetchCustomerWithCriteria({ pageSize: PAGE_SIZE, pageNumber: 1 });
    return () => {
      cleanCustomerProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Customer List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined/>
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Customers</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Card>
              <Row>
                <Col {...searchItemLayout} className="mb-2">
                  <Form name={'search-form'} form={form}>
                    <Form.Item
                      name="searchParameter"
                    >
                      <Search
                        placeholder="Please enter value"
                        enterButton="Search"
                        onSearch={onFinish}
                      />
                    </Form.Item>
                  </Form>
                </Col>
              </Row>

              <div className="box box-default box-ant-table-v1">
                <div className="table-responsive">
                  <Table
                    columns={columns}
                    bordered
                    size='middle'
                    rowKey={record => record.id}
                    dataSource={customers instanceof Array ? customers : []}
                    pagination={{
                      total: pagination.totalData,
                      showSizeChanger: true,
                      current: pagination.current,
                    }}
                    loading={loading}
                    onChange={handleTableChange}
                    scroll={{ x: 720 }}
                  />
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
