import React, { useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, useParams } from 'react-router-dom';
import { Row, Col, Form, Input, Button, Breadcrumb, Spin, DatePicker, Affix, Checkbox } from 'antd';

import moment from 'moment';
import Message from '../Common/Message';
import { DATE_FORMAT_VIEW, DATE_SERVER } from '../../constants/appConfig';
import { HomeOutlined } from '@ant-design/icons';
import history from '../../utils/history';
import ApproveModel from '../Common/Modal/ApproveModal';
import ImageUploadManually from '../Common/ImageUpload/ImageUploadManually';
import { disabledFutureDate } from '../../utils/commonUtil';

const FormItem = Form.Item;

const EditForm = props => {
  const [form] = Form.useForm();
  const { id } = useParams();
  const {
    customers,
    loading,
    errors,
    fetchCustomerByIdentifier,
    updateCustomer,
    updateCustomerStatus,
    cleanCustomerProps,
  } = props;

  const { getFieldValue } = form;

  const onFinish = values => {
    values.id = id;
    Object.keys(values).forEach(key => values[key] === undefined && delete values[key]);

    const formData = new FormData();
    if (values) {
      Object.keys(values).map(key => {
        formData.append(
          key,
          Array.isArray(values[key])
            ? values[key][0].originFileObj
            : moment.isMoment(values[key])
            ? moment(values[key]).format(DATE_SERVER)
            : values[key]
            ? values[key]
            : ' '
        );
      });
    }
    updateCustomer(formData);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 10 },
      lg: { span: 10 },
      md: { span: 10 },
      sm: { span: 12 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 12 },
      lg: { span: 12 },
      md: { span: 16 },
      sm: { span: 18 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };

  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 8 },
      lg: { span: 24, offset: 8 },
      md: { span: 12, offset: 4 },
      sm: { span: 12, offset: 4 },
      xs: { span: 24, offset: 0 },
    },
  };

  const handleInitialValue = values => {
    if (values) {
      Object.keys(values).map(key => {
        if (key === 'dob') {
          values[key] = values[key] && moment(values[key], DATE_FORMAT_VIEW);
        }
      });
      return values;
    }
  };

  const handleBack = () => {
    history.push('/customers');
  };

  const handleCancel = () => {
    form.resetFields();
  };

  const modalProps = {
    title: customers.active
      ? 'Are you sure want to deactivate ?'
      : 'Are you sure want to activate ?',
    apiUrlKey: customers.active ? 'disable' : 'reactivate',
    width: '380px',
    id: id,
    approveEntity: updateCustomerStatus,
  };

  const verifyModalProps = {
    title: 'Verify the customer record ?',
    apiUrlKey: 'verify',
    width: '400px',
    id: id,
    approveEntity: updateCustomerStatus,
  };

  const customerPhotoPreview = getFieldValue('photoPerson');

  const customerPhotoSource = customers ? customers.photo : '';

  useEffect(() => {
    fetchCustomerByIdentifier(id);
    return () => {
      cleanCustomerProps();
    };
  }, []);

  useEffect(() => {
    form.setFieldsValue({
      id: customers.id,
      fullName: customers.fullName,
      emailId: customers.emailId,
      mobileNumber: customers.mobileNumber,
      homeAddress: customers.homeAddress,
      workAddress: customers.workAddress,
      deviceUUID: customers.deviceUUID,
      deviceMAC: customers.deviceMAC,
      active: customers.active,
      dob: customers.dob ? moment(customers.dob) : null,
    });
  }, [customers.fullName]);

  return (
    <Spin spinning={loading} size={'large'} delay={300} tip={'loading...'}>
      <div className="container-fluid no-breadcrumb page-dashboard">
        <QueueAnim type="bottom" className="ui-animate">
          <article className="article" id="components-form-demo-advanced-search">
            <Row type="flex" justify="space-between">
              <Col xl={16} lg={16} md={24} xs={24} sm={24}>
                <h4 className="article-title">Update Customer</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined />
                    Dashboard
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    {' '}
                    <Link to={'/customers'}>Customers </Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Update</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />
            <div className="box box-default">
              <div className="box-body p-5">
                <section className="form-v1-container">
                  <Form
                    form={form}
                    {...formItemLayout}
                    name="updateForm"
                    // initialValues={customers}
                    onFinish={onFinish}
                    className="gurug-form"
                    layout="horizontal"
                  >
                    <Row gutter={18}>
                      <Row justify="end" gutter={[8, 16]}>
                        <Col xl={5} lg={5} md={12} sm={18} xs={18}>
                          <Affix offsetTop={10}>
                            <Button
                              type="dashed"
                              shape="round"
                              block
                              onClick={() => ApproveModel(modalProps)}
                            >
                              {customers && customers.active ? 'Deactivate' : 'Activate'}
                            </Button>
                          </Affix>
                        </Col>
                      </Row>

                      <Row>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'fullName'}
                            label="Full Name"
                            rules={[
                              { required: true, message: 'Full name is required.' },
                              { max: 50, message: 'Please enter less than 50 characters.' },
                            ]}
                          >
                            <Input />
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'emailId'}
                            label={'Email'}
                            rules={[
                              { required: true, message: 'Email is required' },
                              {
                                type: 'email',
                                message: 'Please enter valid email address.',
                              },
                              {
                                max: 30,
                                message: 'Please enter less than 30 character.',
                              },
                            ]}
                          >
                            <Input />
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'mobileNumber'}
                            rules={[
                              { required: true, message: 'Mobile number is required.' },
                              { max: 30, message: 'Please enter less than 30 character.' },
                            ]}
                            label="Mobile Number"
                          >
                            <Input />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'homeAddress'}
                            label="Home Address"
                          >
                            <Input readOnly disabled />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'workAddress'}
                            label="Work Address"
                          >
                            <Input readOnly disabled/>
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem
                            name={'dob'}
                            label="Date of Birth"
                            rules={[{ required: true, message: 'Date of birth is required.' }]}
                          >
                            <DatePicker
                              format={DATE_FORMAT_VIEW}
                              disabledDate={disabledFutureDate}
                            />
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem name={'deviceUUID'} label="Device UUID">
                            <Input.Password readOnly />
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem name={'deviceMAC'} label="Device Mac Address">
                            <Input.Password readOnly />
                          </FormItem>
                        </Col>

                        <Col xl={12} lg={12} md={18} sm={24}>
                          <FormItem label="Status" name={'active'} valuePropName="checked">
                            {customers?.status}{' '}
                          </FormItem>
                        </Col>
                        <Col xl={12} lg={12} md={24} sm={24}>
                          <FormItem name={'photo'} label="Person Photo">
                            <ImageUploadManually
                              {...props}
                              fileName="photo"
                              acceptType="image/jpeg,image/png"
                              placeholder="Person photo"
                              fileType="picture-card"
                              sizeOfFile="512000"
                              form={form}
                              imageSrc={`${customers.photo}?${Date.now()}`}
                            />
                          </FormItem>
                        </Col>

                        <Col xl={16} lg={24} md={24} sm={24}>
                          <Affix offsetBottom={30}>
                            <FormItem {...submitFormLayout}>
                              <Button
                                className="btn-custom-primary mr-1"
                                shape="round"
                                loading={loading}
                                htmlType="submit"
                                size={'large'}
                              >
                                Update
                              </Button>

                              <Button
                                className=" mr-1"
                                shape="round"
                                danger
                                onClick={handleCancel}
                                size={'large'}
                              >
                                Cancel
                              </Button>
                              <Button
                                className=" mr-1"
                                shape="round"
                                onClick={handleBack}
                                size={'large'}
                              >
                                Back
                              </Button>
                            </FormItem>
                          </Affix>
                        </Col>
                      </Row>
                    </Row>
                  </Form>
                </section>
              </div>
            </div>
          </article>
        </QueueAnim>
      </div>
    </Spin>
  );
};

export default EditForm;
