import React, { useState, useEffect } from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter, Link, useParams } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Breadcrumb,
  Spin,
  Divider,
  Select,
  DatePicker,
  InputNumber,
  Checkbox,
  Typography,
  Steps,
  Affix,
  Tabs,
} from 'antd';
import moment from 'moment';

import Message from '../Common/Message';
import { DATE_FORMAT_VIEW, DATE_SERVER_FORMAT, DATE_SERVER} from '../../constants/appConfig';
import { isEmpty, disabledFutureDate, disabledPastDate } from '../../utils/commonUtil';
import ImageUploadManually from '../Common/ImageUpload/ImageUploadManually';
import { HomeOutlined } from '@ant-design/icons';
import ApproveModel from '../Common/Modal/ApproveModal';

const FormItem = Form.Item;
const { Title } = Typography;
const { Option } = Select;

const { TabPane } = Tabs;

const AddForm = props => {
  const [form] = Form.useForm();
  const [keyValue, setKeyValue] = useState(1);
  const [vehicleType, setVehicleType] = useState(props.drivers && props.drivers.vehicleType || 'Bike');

  const { drivers, securityQuestion, loading, errors, fetchSecurityQuestion, addDriver, cleanDriverProps } = props;

  const { validateFields, getFieldValue } = form;

  const onFinish = values => {
    Object.keys(values).forEach(key => values[key] === undefined && delete values[key]);
    const formData = new FormData();
    if (values) {
      Object.keys(values).map(key => {
        formData.append(
          key,
          Array.isArray(values[key])
            ? values[key][0].originFileObj
            : moment.isMoment(values[key])
            ? moment(values[key]).format(DATE_SERVER)
            : values[key] ? values[key] : '',
        );
      });
    }
    addDriver(formData);
  };
  const onFinishFailed = errorInfo => {
    console.error('Failed:', errorInfo);
  };

  const formItemLayout = {
    labelCol: {
      xl: { span: 24 },
      lg: { span: 24 },
      md: { span: 24 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    wrapperCol: {
      xl: { span: 24 },
      lg: { span: 24 },
      md: { span: 24 },
      sm: { span: 24 },
      xs: { span: 24 },
    },
    labelAlign: 'left',
  };
  const submitFormLayout = {
    wrapperCol: {
      xl: { span: 24, offset: 4 },
      lg: { span: 24, offset: 4 },
      md: { span: 12, offset: 0 },
      sm: { span: 12, offset: 0 },
      xs: { span: 24, offset: 0 },
    },
  };

  const handleCancel = () => {
    form.resetFields();
  };

  const handleNextChange = () => {
    validateFields().then(values => {
      setKeyValue(keyValue + 1);
    });
  };

  const handlePreviousChange = () => {
    validateFields().then(values => {
      setKeyValue(keyValue - 1);
    });
  };

  const handleTabChange = (key) => {
    setKeyValue(key);
  };

  const handleVechileType = (value) => {
    setVehicleType(value);
  };

  useEffect(() => {
    fetchSecurityQuestion();
    return () => {
      cleanDriverProps();
    };
  }, []);

  return (
    <Spin spinning={loading} size={'large'} delay={300} tip={'loading...'}>
      <div className="container-fluid no-breadcrumb page-dashboard">
        <QueueAnim type="bottom" className="ui-animate">
          <article className="article" id="components-form-demo-advanced-search">
            <Row type="flex" justify="space-between">
              <Col xl={16} lg={16} md={24} xs={24} sm={24}>
                <h4 className="article-title">Add New Driver</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    {' '}
                    <HomeOutlined/>
                    Dashboard
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    {' '}
                    <Link to={'/drivers'}>Drivers</Link>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>New</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>
            <Spin spinning={false} size={'large'} delay={300} tip={'loading...'}>
              <div className="box-body edit-driver p-5">
                <section className="form-v1-container">
                  <Form
                    {...formItemLayout}
                    name="updateForm"
                    onFinish={onFinish}
                    className="gurug-form-newDesign"
                    layout="horizontal"
                    form={form}
                  >
                    <div className="box box-default" style={{ padding: '20px' }}>
                      <Row gutter={18}>
                        <Tabs defaultActiveKey={'1'} tabPosition={'left'} activeKey={keyValue.toString()}
                        >
                          <TabPane tab="Personal Details" key={'1'}>
                            <Row gutter={14}>
                              <Col xl={12} lg={12} md={18} sm={24}>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'fullName'}
                                    label="Full Name"
                                    rules={[{ required: true, message: 'Full name is required.' }, {
                                      max: 50,
                                      message: 'Please enter the less than 50 characters only.',
                                    }]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>

                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'emailId'}
                                    label={'Email'}
                                    rules={[
                                      { required: true, message: 'Email is required' },
                                      {
                                        type: 'email',
                                        message: 'Please enter valid email address.',
                                      },
                                      {
                                        max: 30,
                                        message: 'Please enter less than 30 character',
                                      },
                                    ]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'mobileNumber'}
                                    rules={[{ required: true, message: 'Mobile number is required.' }]}
                                    label="Mobile Number"
                                  >
                                    <InputNumber/>
                                  </FormItem>
                                </Col>

                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'streetNumber'}
                                    label="Street Number"
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'dob'}
                                    label="Date Of Birth"
                                    rules={[
                                      { required: true, message: 'Date of birth is required.' },
                                    ]}
                                  >
                                    <DatePicker
                                      format={DATE_FORMAT_VIEW}
                                      disabledDate={disabledFutureDate}
                                    />
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'city'}
                                    label="City"
                                    rules={[{ required: true, message: 'City is required.' }, {
                                      max: 20,
                                      message: 'Please enter less than 20 character',
                                    }]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>
                              </Col>
                              <Col xl={12} lg={12} md={24} sm={24}>
                                <div className={'image-label'}>
                                  <FormItem
                                    name={'personPhoto'}
                                    className={'image-label-lower'}
                                    label="Person Photo"
                                    style={{ padding: 0 }}
                                  >
                                    <ImageUploadManually
                                      {...props}
                                      name='avatar'
                                      fileName="personPhoto"
                                      acceptType="image/jpeg,image/png"
                                      placeholder="Person photo"
                                      fileType="picture-card"
                                      sizeOfFile="512000"
                                      form={form}
                                      imageSrc={drivers.photoPerson}
                                    />
                                  </FormItem>
                                </div>
                              </Col>
                            </Row>


                          </TabPane>

                          <TabPane tab="Driving License" key={'2'}>
                            <Row>
                              <Col xl={10} lg={10} md={18} sm={24}>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    label="Licence Number"
                                    name={'licenceNumber'}
                                    rules={[{ required: true, message: 'Licence number is required.' }, {
                                      max: 20,
                                      message: 'Please enter less than 20 character',
                                    }]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>

                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'issueDate'}
                                    label="Issue Date"
                                    rules={[{ required: true, message: 'ID issue date is required.' }]}
                                  >
                                    <DatePicker format={DATE_FORMAT_VIEW} disabledDate={disabledFutureDate}/>
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem name={'expiryDate'} label="Expiry Date"
                                            rules={[{ required: true, message: 'ID issue date is required.' }]}
                                  >
                                    <DatePicker format={DATE_FORMAT_VIEW} disabledDate={disabledPastDate}/>
                                  </FormItem>
                                </Col>
                              </Col>
                              <Col xl={12} lg={12} md={24} sm={24}>
                                <Row gutter={14} justify={'center'} className={'id-label'}>
                                  <Col xl={12} lg={12} md={24} sm={24}>
                                    <FormItem
                                      label="ID Front Photo"
                                      name={'identityFrontPhoto'}
                                      className={'image-label-lower'}
                                      rules={[
                                        {
                                          required: drivers.identityFrontPhoto === undefined,
                                          message: 'Id front photo is required',
                                        },
                                      ]}
                                    >
                                      <ImageUploadManually
                                        {...props}
                                        name='avatar'
                                        fileName="identityFrontPhoto"
                                        acceptType="image/jpeg,image/png"
                                        placeholder="ID front photo"
                                        fileType="picture-card"
                                        sizeOfFile="512000"
                                        form={form}
                                        imageSrc={drivers.identityFrontPhoto}
                                      />
                                    </FormItem>
                                  </Col>
                                  <Col xl={12} lg={12} md={24} sm={24}>
                                    <FormItem
                                      name={'identityBackPhoto'}
                                      label="ID Back Photo"
                                      className={'image-label-lower'}
                                      rules={[
                                        {
                                          required: drivers.identityBackPhoto === undefined,
                                          message: 'Id back photo is required',
                                        },
                                      ]}
                                    >
                                      <ImageUploadManually
                                        {...props}
                                        name='avatar'
                                        fileName="identityBackPhoto"
                                        acceptType="image/jpeg,image/png"
                                        placeholder="ID back photo"
                                        fileType="picture-card"
                                        sizeOfFile="512000"
                                        form={form}
                                        imageSrc={drivers.identityBackPhoto}
                                      />
                                    </FormItem>
                                  </Col>

                                </Row>
                              </Col>
                            </Row>
                          </TabPane>

                          <TabPane tab="Vehicle Details" key={'3'}>
                            <Row gutter={18}>
                              <Col xl={10} lg={10} md={18} sm={24}>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'vehicleType'}
                                    rules={[{ required: true, message: 'Vehicle type is required.' }]}
                                    label="Vehicle Type"
                                  >
                                    <Select
                                      showSearch
                                      onChange={handleVechileType}
                                      placeholder="Select vehicle type"
                                      optionFilterProp="children"
                                      filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
                                        0
                                      }
                                    >
                                      <Option value="Bike">Bike</Option>
                                      <Option value="Car">Car</Option>
                                    </Select>
                                  </FormItem>
                                </Col>

                                {vehicleType === 'Bike' &&
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'vehicleCC'}
                                    label="Vehicle CC"
                                    rules={[{ required: true, message: 'Vehicle CC is required.' }]}
                                  >
                                    <InputNumber/>
                                  </FormItem>
                                </Col>
                                }
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'vehicleBrandModel'}
                                    label="Vehicle Brand Model"
                                    rules={[{ required: true, message: 'Vehicle brand is required.' }, {
                                      max: 20,
                                      message: 'Please enter less than 20 charaters.',
                                    }]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>

                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem name={'vehicleLicensePlate'} label="Vehicle License Plate"
                                            rules={[{
                                              required: true,
                                              message: 'Vehicle license plate is required.',
                                            }, { max: 30, message: 'Please enter less than 30 charaters.' }]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>
                              </Col>

                              <Col xl={14} lg={12} md={24} sm={24}>
                                <Row gutter={16} justify={'center'} className={'id-label'}>
                                  <Col xl={12} lg={12} md={24} sm={24}>
                                    <FormItem name={'photoVehicleFront'} className={'image-label-lower'}
                                              label="Vehicle Front">
                                      <ImageUploadManually
                                        {...props}
                                        name={'avatar'}
                                        fileName="photoVehicleFront"
                                        acceptType="image/jpeg,image/png"
                                        placeholder="Vehicle front photo"
                                        fileType="picture-card"
                                        sizeOfFile="512000"
                                        form={form}
                                        imageSrc={drivers.photoVehicleFront}
                                      />
                                    </FormItem>
                                  </Col>
                                  <Col xl={12} lg={12} md={24} sm={24}>

                                    <FormItem name={'photoVehicleBack'} className={'image-label-lower'}
                                              label="Vehicle Back">
                                      <ImageUploadManually
                                        {...props}
                                        name={'avatar'}
                                        fileName="photoVehicleBack"
                                        acceptType="image/jpeg,image/png"
                                        placeholder="Vehicle back photo"
                                        fileType="picture-card"
                                        sizeOfFile="512000"
                                        form={form}
                                        imageSrc={drivers.photoVehicleBack}
                                      />
                                    </FormItem>

                                  </Col>

                                  <Col xl={12} lg={12} md={24} sm={24}>

                                    <FormItem name={'photoVehicleSide'} className={'image-label-lower'}
                                              label="Vehicle Side">
                                      <ImageUploadManually
                                        {...props}
                                        name={'avatar'}
                                        fileName="photoVehicleSide"
                                        acceptType="image/jpeg,image/png"
                                        placeholder="Vehicle side photo"
                                        fileType="picture-card"
                                        sizeOfFile="512000"
                                        form={form}
                                        imageSrc={drivers.photoVehicleSide}
                                      />
                                    </FormItem>
                                  </Col>
                                </Row>

                              </Col>

                            </Row>
                          </TabPane>
                          <TabPane tab="Others Details" key={'4'}>

                            <Row gutter={14}>
                              <Col xl={12} lg={12} md={18} sm={24}>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    label="Device UID"
                                    name={'deviceUuid'}
                                    rules={[{ required: true, message: 'Device UUID is required.' }]}
                                  >
                                    <Input/>

                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'deviceMac'}
                                    label="Device MAC"
                                    rules={[{ required: true, message: 'Device MAC is required.' }]}
                                  >
                                    <Input/>

                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    name={'password'}
                                    rules={[{ required: true, message: 'Password is required.' }]}
                                    label="Password"
                                  >
                                    <Input.Password/>
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    label="Security Question"
                                    name={'securityQuestionId'}
                                    rules={[{ required: true, message: 'Security question is required.' }]}
                                  >
                                    <Select
                                      showSearch
                                      placeholder='Select security question'
                                      optionFilterProp="children"
                                      filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                      }
                                    >
                                      {securityQuestion &&
                                      securityQuestion.map(val => (
                                        <Option key={val.id} value={val.id}>{val.question}</Option>
                                      ))}
                                    </Select>
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    label="Security Question Answer"
                                    name={'securityQuestionAnswer'}
                                    rules={[{ required: true, message: 'Security question qnswer is required.' }]}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>
                                <Col xl={24} lg={24} md={24} sm={24}>
                                  <FormItem
                                    label="Referred By"
                                    name={'referredBy'}
                                  >
                                    <Input/>
                                  </FormItem>
                                </Col>
                              </Col>
                            </Row>
                          </TabPane>

                        </Tabs>
                      </Row>
                    </div>
                    <Row className={'next-btn-step-form'}>

                      <FormItem {...submitFormLayout}>
                        {/*<Col xl={3} lg={3} md={6} sm={8}>*/}
                        {
                          keyValue < 4 &&
                          <Button
                            className="btn-custom-primary mr-1"
                            onClick={() => handleNextChange()}
                            size={'large'}
                          >
                            Next
                          </Button>
                        }
                        {/*</Col>*/}
                        {/*<Col xl={3} lg={3} md={6} sm={8}>*/}

                        {
                          keyValue === 4 &&
                          <Button
                            className="btn-custom-primary mr-1"
                            htmlType="submit"
                            size={'large'}
                            loading={loading}
                            //onClick={() =>onFinish()}
                          >
                            Save
                          </Button>
                        }
                        {/*</Col>*/}
                        {/*<Col xl={3} lg={3} md={6} sm={8}>*/}
                        {
                          keyValue !== 1 &&
                          <Button
                            className="btn-custom-primary mr-1"
                            loading={loading}
                            onClick={() => handlePreviousChange()}
                            size={'large'}
                          >
                            Previous
                          </Button>
                        }
                        <Link
                          to="/drivers"
                          className="ant-btn ant-btn-round ant-btn-lg no-underline"
                        >
                          {'Back'}
                        </Link>
                      </FormItem>
                    </Row>
                  </Form>
                </section>
              </div>
            </Spin>

          </article>
        </QueueAnim>
      </div>
    </Spin>
  );
};

export default AddForm;
