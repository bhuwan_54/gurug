import React, { useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Row, Col, Input, Table, Breadcrumb, Card, Form, Button, Radio, Modal } from 'antd';
import { getSortingOrder, searchItemLayout } from '../../utils/commonUtil';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import { PAGE_NUMBER, PAGE_SIZE , READ_ABLE_DATE_FORMAT} from '../../constants/appConfig';
import Message from '../Common/Message';
import moment from 'moment';

const Search = Input.Search;
const FormItem = Form.Item;
const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [reviewOf, setReviewOf] = useState('driver');
  // const [viewQuestionModal, setViewQuestionModal] = useState(false);
  const [form] = Form.useForm();
  const { validateFields } = form;

  const { fetchReviewWithCriteria, errors, reviews, pagination, loading, cleanReviewProps } = props;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchReviewWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      type:reviewOf,
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    validateFields().then(fieldsValue => {
      fieldsValue.pageSize = page.pageSize;
      // fieldsValue.pageNumber = page.pageNumber ? page.pageNumber : PAGE_NUMBER;
      fieldsValue.pageNumber = PAGE_NUMBER;
      fieldsValue.sortParameter = page.sortParameter;
      fieldsValue.sortOrder = page.sortOrder;
      fieldsValue.type = reviewOf;
      fetchReviewWithCriteria(fieldsValue);
    });
  };

  const handleQuestionAdd = values => {
    validateFields().then(values => {
      console.log(values, 'values');
      //
    });
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      align:'center',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Reviewed By',
      dataIndex: 'reviewedBy',
      align: 'left',
      render: (text, record) => {
        return <div>{`${record.reviewedBy}`}</div>;
      },
    },
    {
      title: 'Question',
      dataIndex: 'question',
      align: 'left',
      render: (text, record) => {
        return <div>{`${record.question}`}</div>;
      },
    },
    {
      title: 'Answer',
      dataIndex: 'answer',
      align: 'left',
      render: (text, record) => {
        return <div>{`${record.answer}`}</div>;      },
    },
    {
      title: 'Trip ID',
      dataIndex: 'tripId',
      align: 'left',
    },
    {
      title: 'Trip ID',
      dataIndex: 'tripCode',
      align: 'left',
    },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      render: (text, record) => {
        return  <div>{record.createdOn && moment(record.createdOn).format(READ_ABLE_DATE_FORMAT)}</div>;
      },
    },
  ];

  const handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  const handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  useEffect(() => {
    fetchReviewWithCriteria({ pageSize: PAGE_SIZE, pageNumber: PAGE_NUMBER });
    return () => {
      cleanReviewProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Review List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item> Review </Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors} />

            <Card>
              <div>
                <Row justify={'space-between'}>
                  {/*<Col>*/}
                  {/*  <Radio.Group*/}
                  {/*    onChange={handleModeChange}*/}
                  {/*    value={reviewOf}*/}
                  {/*    style={{ marginBottom: 8 }}*/}
                  {/*  >*/}
                  {/*    <Radio.Button value="driver">Driver</Radio.Button>*/}
                  {/*    <Radio.Button value="customer">Customer</Radio.Button>*/}
                  {/*  </Radio.Group>*/}
                  {/*</Col>*/}
                  {/*<Col>*/}
                  {/*  <Button type={'dashed'} onClick={setViewQuestionModal(true)}>*/}
                  {/*    Add Question{' '}*/}
                  {/*  </Button>*/}
                  {/*</Col>*/}

                  <Col {...searchItemLayout} className="mb-2">
                    <Form name={'searchParameter'}>
                      <Search
                        placeholder="Please enter value"
                        enterButton="Search"
                        onSearch={onFinish}
                      />
                    </Form>
                  </Col>
                </Row>

                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      columns={columns}
                      size={'middle'}
                      bordered
                      rowKey={record => record.id}
                      dataSource={reviews instanceof Array ? reviews : []}
                      pagination={{
                        total: pagination.total,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
                {/*<Modal*/}
                {/*  title="Add Review Question"*/}
                {/*  visible={viewQuestionModal}*/}
                {/*  // onOk={handleOk}*/}
                {/*  onCancel={handleCancel}*/}
                {/*  footer={null}*/}
                {/*>*/}
                {/*  /!*<Form*!/*/}
                {/*  /!*  name="updateForm"*!/*/}
                {/*  /!*  onFinish={handleQuestionAdd}*!/*/}
                {/*  /!*  className="gurug-form"*!/*/}
                {/*  /!*  layout="horizontal"*!/*/}
                {/*  /!*  form={form}*!/*/}
                {/*  /!*>*!/*/}
                {/*  /!*  <Row>*!/*/}
                {/*  /!*    <Col>*!/*/}
                {/*  /!*      <FormItem*!/*/}
                {/*  /!*        name={'question'}*!/*/}
                {/*  /!*        rules={[{ required: true, message: 'Question value is required.' }]}*!/*/}
                {/*  /!*        label="Question"*!/*/}
                {/*  /!*      >*!/*/}
                {/*  /!*        <Input placeholder={'value'} />*!/*/}
                {/*  /!*      </FormItem>*!/*/}
                {/*  /!*    </Col>*!/*/}
                {/*  /!*    <Col span={24}>*!/*/}
                {/*  /!*      <FormItem*!/*/}
                {/*  /!*        name={'question'}*!/*/}
                {/*  /!*        rules={[{ required: true, message: 'Question value is required.' }]}*!/*/}
                {/*  /!*        label="Question"*!/*/}
                {/*  /!*      >*!/*/}
                {/*  /!*        <Input placeholder={'value'} />*!/*/}
                {/*  /!*      </FormItem>*!/*/}
                {/*  /!*    </Col>*!/*/}

                {/*  /!*    <Col span= {24}>*!/*/}
                {/*  /!*      <FormItem*!/*/}
                {/*  /!*        label="Created By"*!/*/}
                {/*  /!*        name={'createdBy'}*!/*/}
                {/*  /!*        rules={[{ required: true, message: 'Created by is required.' }]}*!/*/}
                {/*  /!*      >*!/*/}
                {/*  /!*        <Input placeholder={'value'} />*!/*/}
                {/*  /!*      </FormItem>*!/*/}
                {/*  /!*    </Col>*!/*/}

                {/*  /!*    <Col xl={16} lg={24} md={24} sm={24}>*!/*/}
                {/*  /!*        <FormItem>*!/*/}
                {/*  /!*          <Button*!/*/}
                {/*  /!*            className="btn-custom-primary mr-1"*!/*/}
                {/*  /!*            shape="round"*!/*/}
                {/*  /!*            loading={loading}*!/*/}
                {/*  /!*            htmlType="submit"*!/*/}
                {/*  /!*          >*!/*/}
                {/*  /!*            Add*!/*/}
                {/*  /!*          </Button>*!/*/}
                {/*  /!*          <Button*!/*/}
                {/*  /!*            className=" mr-1"*!/*/}
                {/*  /!*            shape="round"*!/*/}
                {/*  /!*            onClick={handleCancel}*!/*/}
                {/*  /!*          >*!/*/}
                {/*  /!*            Cancel*!/*/}
                {/*  /!*          </Button>*!/*/}
                {/*  /!*        </FormItem>*!/*/}
                {/*  /!*    </Col>*!/*/}
                {/*  /!*  </Row>*!/*/}
                {/*  /!*</Form>*!/*/}
                {/*</Modal>*/}
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
