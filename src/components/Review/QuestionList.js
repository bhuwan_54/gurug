import React, { Fragment, useEffect, useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Input, Table, Breadcrumb, Card, Form, Button, Radio } from 'antd';
import { getSortingOrder, searchItemLayout } from '../../utils/commonUtil';
import { EyeOutlined, HomeOutlined } from '@ant-design/icons';

import { PAGE_NUMBER, PAGE_SIZE } from '../../constants/appConfig';
import Message from '../Common/Message';

const Search = Input.Search;
const List = props => {
  const [page, setPage] = useState({ pageSize: PAGE_SIZE });
  const [reviewOf, setReviewOf] = useState('all');
  const [form] = Form.useForm();

  const {
    fetchReviewQuestionWithCriteria,
    errors,
    reviews,
    pagination,
    loading,
    cleanReviewProps,
  } = props;

  const handleTableChange = (pagination, filters, sorter) => {
    fetchReviewQuestionWithCriteria({
      searchParameter: form.getFieldValue('searchParameter'),
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
      ...filters,
    });
    setPage({
      pageSize: pagination.pageSize,
      pageNumber: pagination.pageNumber || PAGE_NUMBER,
      sortParameter: sorter.field,
      sortType: getSortingOrder(sorter.order),
    });
  };

  const onFinish = fieldsValue => {
    fieldsValue.searchParameter = fieldsValue['searchParameter'];
    fieldsValue.pageSize = page.pageSize;
    fieldsValue.pageNumber = page.pageNumber ? page.pageNumber : PAGE_NUMBER;
    fieldsValue.sortParameter = page.sortParameter;
    fieldsValue.sortOrder = page.sortOrder;
    fetchReviewQuestionWithCriteria(fieldsValue);
  };

  const columns = [
    {
      title: 'S.N',
      key: 'index',
      width: '6%',
      render: (text, record, index) => {
        return (PAGE_NUMBER - 1) * PAGE_SIZE + index + 1;
        // return (pagination.current - 1) * pagination.pageSize + index + 1;
      },
    },
    {
      title: 'Name',
      dataIndex: 'fullName',
      align: 'left',
      sorter: true,
      render: (text, record) => {
        return <div>{`${record.fullName}`}</div>;
      },
    },
    {
      title: 'Identity',
      dataIndex: 'mobileNumber',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Email ID',
      dataIndex: 'emailId',
      align: 'left',
      sorter: true,
    },
    {
      title: 'City',
      dataIndex: 'city',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Created On',
      dataIndex: 'createdOn',
      align: 'left',
      sorter: true,
    },
    {
      title: 'Modified On',
      dataIndex: 'modifiedOn',
      align: 'left',
      sorter: true,
    },
    // {
    //   title: 'Action',
    //   key: 'operation',
    //   render: (text, record) => {
    //     return (
    //       <Link to={`/lives/${record.id}/detail`} title="View">
    //         <Button shape="circle"  icon={<EyeOutlined  style={{ fontSize: '16px' }}/>}  style={{margin:'5px'}}/>
    //       </Link>
    //     );
    //   },
    // },
  ];

  const handleModeChange = e => {
    const mode = e.target.value;
    setReviewOf(mode);
  };
  useEffect(() => {
    fetchReviewQuestionWithCriteria({ pageSize: PAGE_SIZE, pageNumber: PAGE_NUMBER , type:'DRIVER'});
    return () => {
      cleanReviewProps();
    };
  }, []);

  return (
    <div className="container-fluid no-breadcrumb page-dashboard">
      <QueueAnim type="bottom" className="ui-animate">
        <div className="article__section">
          <article className="article">
            <Row type="flex" justify="space-between">
              <Col xl={12} lg={12} md={24} xs={24} sm={24}>
                <h4 className="article-title">Review Question List</h4>
              </Col>
              <Col>
                <Breadcrumb separator="/">
                  <Breadcrumb.Item>
                    <HomeOutlined/>
                    {'Dashboard '}
                  </Breadcrumb.Item>
                  <Breadcrumb.Item> Review Question List</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>

            <Message error={errors}/>

            <Card>
              <div>
                <Row justify={'space-between'}>
                  <Col {...searchItemLayout} className="mb-2">
                    <Form name={'searchParameter'}>
                      <Search
                        placeholder="Please enter value"
                        enterButton="Search"
                        onSearch={onFinish}
                      />
                    </Form>
                  </Col>
                </Row>

                <div className="box box-default box-ant-table-v1">
                  <div className="table-responsive">
                    <Table
                      columns={columns}
                      rowKey={record => record.id}
                      dataSource={reviews instanceof Array ? reviews : []}
                      pagination={{
                        total: pagination.total,
                        showSizeChanger: true,
                        current: pagination.current,
                      }}
                      loading={loading}
                      onChange={handleTableChange}
                      scroll={{ x: 720 }}
                    />
                  </div>
                </div>
              </div>
            </Card>
          </article>
        </div>
      </QueueAnim>
    </div>
  );
};

export default List;
