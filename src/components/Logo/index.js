import React, { Fragment } from 'react';
import LogoImage from '../../assets/full_logo.png';
import SmallLogo from '../../assets/Guruji.png';
import { connect } from 'react-redux';

const Logo = props => {
  const { collapsedNav } = props;
  return (
    <Fragment>
      {collapsedNav !== true ? (
        <span>
          <img
            src={LogoImage}
            style={{ height: '55px', width: '205px', padding: '2px' }}
            alt="Guruji Logo"
          />
        </span>
      ) : (
        <img src={SmallLogo} style={{ height: '60px', padding: '1px' }} alt="Guruji Logo" />
      )}
    </Fragment>
  );
};

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
  collapsedNav: state.settings.collapsedNav,
});
export default connect(mapStateToProps)(Logo);
