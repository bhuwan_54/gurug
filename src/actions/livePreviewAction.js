// Import actionType constants
import {
  LIVE_PREVIEW_ADD_REQUEST,
  LIVE_PREVIEW_ADD_REQUEST_SUCCESS,
  LIVE_PREVIEW_ADD_REQUEST_FAILURE,
  LIVE_PREVIEW_FETCH_REQUEST,
  LIVE_PREVIEW_FETCH_REQUEST_SUCCESS,
  LIVE_PREVIEW_FETCH_REQUEST_FAILURE,
  LIVE_PREVIEW_UPDATE_REQUEST,
  LIVE_PREVIEW_UPDATE_REQUEST_SUCCESS,
  LIVE_PREVIEW_UPDATE_REQUEST_FAILURE,
  LIVE_PREVIEW_CLEAN_REQUEST,
  TOTAL_TRIP_FETCH_REQUEST,
  TOTAL_TRIP_FETCH_REQUEST_SUCCESS,
  TOTAL_TRIP_FETCH_REQUEST_FAILURE,
  TRIP_DETAIL_FETCH_REQUEST,
  TRIP_DETAIL_FETCH_REQUEST_SUCCESS,
  TRIP_DETAIL_FETCH_REQUEST_FAILURE
} from '../constants/actionTypes';

export function livePreviewAddRequest() {
  return {
    type: LIVE_PREVIEW_ADD_REQUEST,
  };
}
export function livePreviewAddRequestSuccess(data) {
  return {
    type: LIVE_PREVIEW_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function livePreviewAddRequestFailure(error) {
  return {
    type: LIVE_PREVIEW_ADD_REQUEST_FAILURE,
    error,
  };
}

export function livePreviewFetchRequest() {
  return {
    type: LIVE_PREVIEW_FETCH_REQUEST,
  };
}
export function livePreviewFetchRequestSuccess(data) {
  return {
    type: LIVE_PREVIEW_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function livePreviewFetchRequestFailure(error) {
  return {
    type: LIVE_PREVIEW_FETCH_REQUEST_FAILURE,
    error,
  };
}


export function totalTripFetchRequest() {
  return {
    type: TOTAL_TRIP_FETCH_REQUEST,
  };
}
export function totalTripFetchRequestSuccess(data) {
  return {
    type: TOTAL_TRIP_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function totalTripFetchRequestFailure(error) {
  return {
    type: TOTAL_TRIP_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function tripDetailFetchRequest() {
  return {
    type: TRIP_DETAIL_FETCH_REQUEST,
  };
}
export function tripDetailFetchRequestSuccess(data) {
  return {
    type: TRIP_DETAIL_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function tripDetailFetchRequestFailure(error) {
  return {
    type: TRIP_DETAIL_FETCH_REQUEST_FAILURE,
    error,
  };
}
export function livePreviewUpdateRequest() {
  return {
    type: LIVE_PREVIEW_UPDATE_REQUEST,
  };
}
export function livePreviewUpdateRequestSuccess(data) {
  return {
    type: LIVE_PREVIEW_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function livePreviewUpdateRequestFailure(error) {
  return {
    type: LIVE_PREVIEW_UPDATE_REQUEST_FAILURE,
    error,
  };
}

export const livePreviewCleanRequest = () => {
  return {
    type: LIVE_PREVIEW_CLEAN_REQUEST,
  };
};
