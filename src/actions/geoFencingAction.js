// Import actionType constants
import {
  GEOFENCING_ADD_REQUEST,
  GEOFENCING_ADD_REQUEST_SUCCESS,
  GEOFENCING_ADD_REQUEST_FAILURE,
  GEOFENCING_FETCH_REQUEST,
  GEOFENCING_FETCH_REQUEST_SUCCESS,
  GEOFENCING_FETCH_REQUEST_FAILURE,
  GEOFENCING_UPDATE_REQUEST,
  GEOFENCING_UPDATE_REQUEST_SUCCESS,
  GEOFENCING_UPDATE_REQUEST_FAILURE,
  GEOFENCING_CLEAN_REQUEST,
} from '../constants/actionTypes';

export const geoFencingAddRequest = () => {
  return {
    type: GEOFENCING_ADD_REQUEST,
  };
};
export function geoFencingAddRequestSuccess(data) {
  return {
    type: GEOFENCING_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function geoFencingAddRequestFailure(error) {
  return {
    type: GEOFENCING_ADD_REQUEST_FAILURE,
    error,
  };
}

export function geoFencingFetchRequest() {
  return {
    type: GEOFENCING_FETCH_REQUEST,
  };
}
export function geoFencingFetchRequestSuccess(data) {
  return {
    type: GEOFENCING_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function geoFencingFetchRequestFailure(error) {
  return {
    type: GEOFENCING_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function geoFencingUpdateRequest() {
  return {
    type: GEOFENCING_UPDATE_REQUEST,
  };
}
export function geoFencingUpdateRequestSuccess(data) {
  return {
    type: GEOFENCING_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function geoFencingUpdateRequestFailure(error) {
  return {
    type: GEOFENCING_UPDATE_REQUEST_FAILURE,
    error,
  };
}

export const geoFencingCleanRequest = () => {
  return {
    type: GEOFENCING_CLEAN_REQUEST,
  };
};
