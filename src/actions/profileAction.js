// Import actionType constants
import {
  PROFILE_FETCH_REQUEST,
  PROFILE_FETCH_REQUEST_SUCCESS,
  PROFILE_FETCH_REQUEST_FAILURE,
  PROFILE_UPDATE_REQUEST,
  PROFILE_UPDATE_REQUEST_SUCCESS,
  PROFILE_UPDATE_REQUEST_FAILURE
} from '../constants/actionTypes';


export function profileFetchRequest() {
  return {
    type: PROFILE_FETCH_REQUEST,
  };
}

export function profileFetchRequestSuccess(data) {
  return {
    type: PROFILE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function profileFetchRequestFailure(error) {
  return {
    type: PROFILE_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function profileUpdateRequest() {
  return {
    type: PROFILE_UPDATE_REQUEST,
  };
}

export function profileUpdateRequestSuccess(data) {
  return {
    type: PROFILE_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function profileUpdateRequestFailure(error) {
  return {
    type: PROFILE_UPDATE_REQUEST_FAILURE,
    error,
  };
}


