// Import actionType constants
import {
  CUSTOMER_ADD_REQUEST,
  CUSTOMER_ADD_REQUEST_SUCCESS,
  CUSTOMER_ADD_REQUEST_FAILURE,
  CUSTOMER_FETCH_REQUEST,
  CUSTOMER_FETCH_REQUEST_SUCCESS,
  CUSTOMER_FETCH_REQUEST_FAILURE,
  CUSTOMER_UPDATE_REQUEST,
  CUSTOMER_UPDATE_REQUEST_SUCCESS,
  CUSTOMER_UPDATE_REQUEST_FAILURE,
  CUSTOMER_CLEAN_REQUEST,
} from '../constants/actionTypes';

export function customerAddRequest() {
  return {
    type: CUSTOMER_ADD_REQUEST,
  };
}

export function customerAddRequestSuccess(data) {
  return {
    type: CUSTOMER_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function customerAddRequestFailure(error) {
  return {
    type: CUSTOMER_ADD_REQUEST_FAILURE,
    error,
  };
}

export function customerFetchRequest() {
  return {
    type: CUSTOMER_FETCH_REQUEST,
  };
}

export function customerFetchRequestSuccess(data) {
  return {
    type: CUSTOMER_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function customerFetchRequestFailure(error) {
  return {
    type: CUSTOMER_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function customerUpdateRequest() {
  return {
    type: CUSTOMER_UPDATE_REQUEST,
  };
}

export function customerUpdateRequestSuccess(data) {
  return {
    type: CUSTOMER_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function customerUpdateRequestFailure(error) {
  return {
    type: CUSTOMER_UPDATE_REQUEST_FAILURE,
    error,
  };
}

export const customerCleanRequest = () => {
  return {
    type: CUSTOMER_CLEAN_REQUEST,
  };
};
