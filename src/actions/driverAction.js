// Import actionType constants
import {
  DRIVER_ADD_REQUEST,
  DRIVER_ADD_REQUEST_SUCCESS,
  DRIVER_ADD_REQUEST_FAILURE,
  DRIVER_FETCH_REQUEST,
  DRIVER_FETCH_REQUEST_SUCCESS,
  DRIVER_FETCH_REQUEST_FAILURE,
  DRIVER_UPDATE_REQUEST,
  DRIVER_UPDATE_REQUEST_SUCCESS,
  DRIVER_UPDATE_REQUEST_FAILURE,
  DRIVER_CLEAN_REQUEST,
  DRIVER_DELETE_REQUEST_SUCCESS,
  DRIVER_DELETE_REQUEST_FAILURE,
  SECURITY_QUESTION_FETCH_REQUEST,
  SECURITY_QUESTION_FETCH_REQUEST_SUCCESS,
  SECURITY_QUESTION_FETCH_REQUEST_FAILURE
} from '../constants/actionTypes';

export function driverAddRequest() {
  return {
    type: DRIVER_ADD_REQUEST,
  };
}
export function driverAddRequestSuccess(data) {
  return {
    type: DRIVER_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function driverAddRequestFailure(error) {
  return {
    type: DRIVER_ADD_REQUEST_FAILURE,
    error,
  };
}

export function driverFetchRequest() {
  return {
    type: DRIVER_FETCH_REQUEST,
  };
}
export function driverFetchRequestSuccess(data) {
  return {
    type: DRIVER_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function driverFetchRequestFailure(error) {
  return {
    type: DRIVER_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function driverUpdateRequest() {
  return {
    type: DRIVER_UPDATE_REQUEST,
  };
}
export function driverUpdateRequestSuccess(data) {
  return {
    type: DRIVER_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function driverUpdateRequestFailure(error) {
  return {
    type: DRIVER_UPDATE_REQUEST_FAILURE,
    error,
  };
}

export const driverCleanRequest = () => {
  return {
    type: DRIVER_CLEAN_REQUEST,
  };
};



export function securityFetchRequest() {
  return {
    type: SECURITY_QUESTION_FETCH_REQUEST,
  };
}
export function securityFetchRequestSuccess(data) {
  return {
    type: SECURITY_QUESTION_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function securityFetchRequestFailure(error) {
  return {
    type: SECURITY_QUESTION_FETCH_REQUEST_FAILURE,
    error,
  };
}
