// Import actionType constants
import {
  REVIEW_FETCH_REQUEST,
  REVIEW_FETCH_REQUEST_SUCCESS,
  REVIEW_FETCH_REQUEST_FAILURE,
  REVIEW_CLEAN_REQUEST,
} from '../constants/actionTypes';

export function reviewFetchRequest() {
  return {
    type: REVIEW_FETCH_REQUEST,
  };
}
export function reviewFetchRequestSuccess(data) {
  return {
    type: REVIEW_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function reviewFetchRequestFailure(error) {
  return {
    type: REVIEW_FETCH_REQUEST_FAILURE,
    error,
  };
}

export const reviewCleanRequest = () => {
  return {
    type: REVIEW_CLEAN_REQUEST,
  };
};
