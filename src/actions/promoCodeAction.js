// Import actionType constants
import {
  PROMO_CODE_ADD_REQUEST,
  PROMO_CODE_ADD_REQUEST_SUCCESS,
  PROMO_CODE_ADD_REQUEST_FAILURE,
  PROMO_CODE_FETCH_REQUEST,
  PROMO_CODE_FETCH_REQUEST_SUCCESS,
  PROMO_CODE_FETCH_REQUEST_FAILURE,
  PROMO_CODE_UPDATE_REQUEST,
  PROMO_CODE_UPDATE_REQUEST_SUCCESS,
  PROMO_CODE_UPDATE_REQUEST_FAILURE,
  PROMO_CODE_CLEAN_REQUEST,
  PROMO_CODE_DISCOUNT_FETCH_REQUEST,
  PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS,
  PROMO_CODE_DISCOUNT_FETCH_REQUEST_FAILURE,
  PROMO_CODE
} from '../constants/actionTypes';

export function promoCodeAddRequest() {
  return {
    type: PROMO_CODE_ADD_REQUEST,
  };
}
export function promoCodeAddRequestSuccess(data) {
  return {
    type: PROMO_CODE_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function promoCodeAddRequestFailure(error) {
  return {
    type: PROMO_CODE_ADD_REQUEST_FAILURE,
    error,
  };
}

export function promoCodeFetchRequest() {
  return {
    type: PROMO_CODE_FETCH_REQUEST,
  };
}
export function promoCodeFetchRequestSuccess(data) {
  return {
    type: PROMO_CODE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function promoCodeFetchRequestFailure(error) {
  return {
    type: PROMO_CODE_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function promoCodeDiscountFetchRequest() {
  return {
    type: PROMO_CODE_DISCOUNT_FETCH_REQUEST,
  };
}
export function promoCodeDiscountFetchRequestSuccess(data) {
  return {
    type: PROMO_CODE_DISCOUNT_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function promoCodeDiscountFetchRequestFailure(error) {
  return {
    type: PROMO_CODE_DISCOUNT_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function promoCodeUpdateRequest() {
  return {
    type: PROMO_CODE_UPDATE_REQUEST,
  };
}
export function promoCodeUpdateRequestSuccess(data) {
  return {
    type: PROMO_CODE_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function promoCodeUpdateRequestFailure(error) {
  return {
    type: PROMO_CODE_UPDATE_REQUEST_FAILURE,
    error,
  };
}

export const promoCodeCleanRequest = () => {
  return {
    type: PROMO_CODE_CLEAN_REQUEST,
  };
};


export const promoCode = (data) => {
  return {
    type: PROMO_CODE,
    data: data
  };
};
