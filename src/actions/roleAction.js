// Import actionType constants
import {
  ROLE_ADD_REQUEST,
  ROLE_ADD_REQUEST_SUCCESS,
  ROLE_ADD_REQUEST_FAILURE,
  ROLE_FETCH_REQUEST,
  ROLE_FETCH_REQUEST_SUCCESS,
  ROLE_FETCH_REQUEST_FAILURE,
  ROLE_USER_TYPE_FETCH_REQUEST,
  ROLE_USER_TYPE_FETCH_REQUEST_SUCCESS,
  ROLE_USER_TYPE_FETCH_REQUEST_FAILURE,
  ROLE_UPDATE_REQUEST,
  ROLE_UPDATE_REQUEST_SUCCESS,
  ROLE_UPDATE_REQUEST_FAILURE,
  PERMISSION_FETCH_REQUEST,
  PERMISSION_FETCH_REQUEST_SUCCESS,
  PERMISSION_FETCH_REQUEST_FAILURE,
  ROLE_CLEAN_REQUEST,
} from '../constants/actionTypes';

export function roleAddRequest() {
  return {
    type: ROLE_ADD_REQUEST,
  };
}
export function roleAddRequestSuccess(data) {
  return {
    type: ROLE_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function roleAddRequestFailure(error) {
  return {
    type: ROLE_ADD_REQUEST_FAILURE,
    error,
  };
}

export function roleFetchRequest() {
  return {
    type: ROLE_FETCH_REQUEST,
  };
}
export function roleFetchRequestSuccess(data) {
  return {
    type: ROLE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function roleFetchRequestFailure(error) {
  return {
    type: ROLE_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function roleUserTypeFetchRequest() {
  return {
    type: ROLE_USER_TYPE_FETCH_REQUEST,
  };
}
export function roleUserTypeFetchRequestSuccess(data) {
  return {
    type: ROLE_USER_TYPE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function roleUserTypeFetchRequestFailure(error) {
  return {
    type: ROLE_USER_TYPE_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function roleUpdateRequest() {
  return {
    type: ROLE_UPDATE_REQUEST,
  };
}
export function roleUpdateRequestSuccess(data) {
  return {
    type: ROLE_UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function roleUpdateRequestFailure(error) {
  return {
    type: ROLE_UPDATE_REQUEST_FAILURE,
    error,
  };
}

export function permissionFetchRequest() {
  return {
    type: PERMISSION_FETCH_REQUEST,
  };
}
export function permissionFetchRequestSuccess(data) {
  return {
    type: PERMISSION_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function permissionFetchRequestFailure(error) {
  return {
    type: PERMISSION_FETCH_REQUEST_FAILURE,
    error,
  };
}

export const roleCleanRequest = () => {
  return {
    type: ROLE_CLEAN_REQUEST,
  };
};
