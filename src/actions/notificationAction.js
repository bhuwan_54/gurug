// Import actionType constants
import {
  NOTIFICATION_ADD_REQUEST,
  NOTIFICATION_ADD_REQUEST_SUCCESS,
  NOTIFICATION_ADD_REQUEST_FAILURE,
  NOTIFICATION_FETCH_REQUEST,
  NOTIFICATION_FETCH_REQUEST_SUCCESS,
  NOTIFICATION_FETCH_REQUEST_FAILURE,
  NOTIFICATION_CLEAN_REQUEST,
} from '../constants/actionTypes';

export function notificationAddRequest() {
  return {
    type: NOTIFICATION_ADD_REQUEST,
  };
}

export function notificationAddRequestSuccess(data) {
  return {
    type: NOTIFICATION_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function notificationAddRequestFailure(error) {
  return {
    type: NOTIFICATION_ADD_REQUEST_FAILURE,
    error,
  };
}

export function notificationFetchRequest() {
  return {
    type: NOTIFICATION_FETCH_REQUEST,
  };
}

export function notificationFetchRequestSuccess(data) {
  return {
    type: NOTIFICATION_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function notificationFetchRequestFailure(error) {
  return {
    type: NOTIFICATION_FETCH_REQUEST_FAILURE,
    error,
  };
}

export const notificationCleanRequest = () => {
  return {
    type: NOTIFICATION_CLEAN_REQUEST,
  };
};
