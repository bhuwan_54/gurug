// Import actionType constants
import {
  TRIP_FEE_ADD_REQUEST,
  TRIP_FEE_ADD_REQUEST_SUCCESS,
  TRIP_FEE_ADD_REQUEST_FAILURE,
  TRIP_FEE_FETCH_REQUEST,
  TRIP_FEE_FETCH_REQUEST_SUCCESS,
  TRIP_FEE_FETCH_REQUEST_FAILURE,
  TRIP_FEE_CLEAN_REQUEST,
} from '../constants/actionTypes';

export function tripFeeAddRequest() {
  return {
    type: TRIP_FEE_ADD_REQUEST,
  };
}

export function tripFeeAddRequestSuccess(data) {
  return {
    type: TRIP_FEE_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function tripFeeAddRequestFailure(error) {
  return {
    type: TRIP_FEE_ADD_REQUEST_FAILURE,
    error,
  };
}

export function tripFeeFetchRequest() {
  return {
    type: TRIP_FEE_FETCH_REQUEST,
  };
}

export function tripFeeFetchRequestSuccess(data) {
  return {
    type: TRIP_FEE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function tripFeeFetchRequestFailure(error) {
  return {
    type: TRIP_FEE_FETCH_REQUEST_FAILURE,
    error,
  };
}

export const tripFeeCleanRequest = () => {
  return {
    type: TRIP_FEE_CLEAN_REQUEST,
  };
};
