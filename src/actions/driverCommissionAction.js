// Import actionType constants
import {
  DRIVER_COMMISSION_ADD_REQUEST,
  DRIVER_COMMISSION_ADD_REQUEST_SUCCESS,
  DRIVER_COMMISSION_ADD_REQUEST_FAILURE,
  DRIVER_COMMISSION_FETCH_REQUEST,
  DRIVER_COMMISSION_FETCH_REQUEST_SUCCESS,
  DRIVER_COMMISSION_FETCH_REQUEST_FAILURE,
  DRIVER_COMMISSION_CLEAN_REQUEST,
  DRIVER_COMMISSION_TYPE_REQUEST,
  DRIVER_COMMISSION_TYPE_REQUEST_SUCCESS,
  DRIVER_COMMISSION_TYPE_REQUEST_FAILURE,
  COMMISSION_PROFILE_FETCH_REQUEST,
  COMMISSION_PROFILE_FETCH_REQUEST_SUCCESS,
  COMMISSION_PROFILE_FETCH_REQUEST_FAILURE,
} from '../constants/actionTypes';

export function driverCommissionAddRequest() {
  return {
    type: DRIVER_COMMISSION_ADD_REQUEST,
  };
}

export function driverCommissionAddRequestSuccess(data) {
  return {
    type: DRIVER_COMMISSION_ADD_REQUEST_SUCCESS,
    data,
  };
}

export function driverCommissionAddRequestFailure(error) {
  return {
    type: DRIVER_COMMISSION_ADD_REQUEST_FAILURE,
    error,
  };
}

export function driverCommissionFetchRequest() {
  return {
    type: DRIVER_COMMISSION_FETCH_REQUEST,
  };
}

export function driverCommissionFetchRequestSuccess(data) {
  return {
    type: DRIVER_COMMISSION_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function driverCommissionFetchRequestFailure(error) {
  return {
    type: DRIVER_COMMISSION_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function commissionTypeFetchRequest() {
  return {
    type: DRIVER_COMMISSION_TYPE_REQUEST,
  };
}

export function commissionTypeFetchRequestSuccess(data) {
  return {
    type: DRIVER_COMMISSION_TYPE_REQUEST_SUCCESS,
    data,
  };
}

export function commissionTypeFetchRequestFailure(error) {
  return {
    type: DRIVER_COMMISSION_TYPE_REQUEST_FAILURE,
    error,
  };
}

export const driverCommissionCleanRequest = () => {
  return {
    type: DRIVER_COMMISSION_CLEAN_REQUEST,
  };
};

export function commissionProfileFetchRequest() {
  return {
    type: COMMISSION_PROFILE_FETCH_REQUEST,
  };
}

export function commissionProfileFetchRequestSuccess(data) {
  return {
    type: COMMISSION_PROFILE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function commissionProfileFetchRequestFailure(error) {
  return {
    type: COMMISSION_PROFILE_FETCH_REQUEST_FAILURE,
    error,
  };
}
