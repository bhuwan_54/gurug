// Import actionType constants
import {
  SUPPORT_TICKET_ASSIGN_REQUEST,
  SUPPORT_TICKET_ASSIGN_REQUEST_SUCCESS,
  SUPPORT_TICKET_ASSIGN_REQUEST_FAILURE,
  SUPPORT_TICKET_FETCH_REQUEST,
  SUPPORT_TICKET_FETCH_REQUEST_SUCCESS,
  SUPPORT_TICKET_FETCH_REQUEST_FAILURE,
  EMPLOYEE_FETCH_REQUEST,
  EMPLOYEE_FETCH_REQUEST_SUCCESS,
  EMPLOYEE_FETCH_REQUEST_FAILURE,
  SUPPORT_TICKET_CLEAN_REQUEST
} from '../constants/actionTypes';

export function supportAssignRequest() {
  return {
    type: SUPPORT_TICKET_ASSIGN_REQUEST,
  };
}
export function supportAssignRequestSuccess(data) {
  return {
    type: SUPPORT_TICKET_ASSIGN_REQUEST_SUCCESS,
    data,
  };
}

export function supportTicketAssignRequestFailure(error) {
  return {
    type: SUPPORT_TICKET_ASSIGN_REQUEST_FAILURE,
    error,
  };
}

export function supportTicketFetchRequest() {
  return {
    type: SUPPORT_TICKET_FETCH_REQUEST,
  };
}
export function supportTicketFetchRequestSuccess(data) {
  return {
    type: SUPPORT_TICKET_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function supportTicketRequestFailure(error) {
  return {
    type: SUPPORT_TICKET_FETCH_REQUEST_FAILURE,
    error,
  };
}

export function employeeFetchRequest() {
  return {
    type: EMPLOYEE_FETCH_REQUEST,
  };
}
export function employeeFetchRequestSuccess(data) {
  return {
    type: EMPLOYEE_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function employeeFetchRequestFailure(error) {
  return {
    type: EMPLOYEE_FETCH_REQUEST_FAILURE,
    error,
  };
}

export const supportTicketCleanRequest = () => {
  return {
    type: SUPPORT_TICKET_CLEAN_REQUEST,
  };
};
