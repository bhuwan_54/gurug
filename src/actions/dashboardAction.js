// Import actionType constants
import {
  DASHBOARD_FETCH_REQUEST,
  DASHBOARD_FETCH_REQUEST_SUCCESS,
  DASHBOARD_FETCH_REQUEST_FAILURE,
} from '../constants/actionTypes';

export function dashboardFetchRequest() {
  return {
    type: DASHBOARD_FETCH_REQUEST,
  };
}

export function dashboardFetchRequestSuccess(data) {
  return {
    type: DASHBOARD_FETCH_REQUEST_SUCCESS,
    data,
  };
}

export function dashboardFetchRequestFailure(error) {
  return {
    type: DASHBOARD_FETCH_REQUEST_FAILURE,
    error,
  };
}
